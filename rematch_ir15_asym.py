# %%
import os
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt

import xtrack as xt
import xtrack._temp.lhc_match as lm
from misc import *

from rematch_from_xtrack import *

import rematch_ir15_seperately

# %%
try:
    get_ipython().run_line_magic("matplotlib", "inline")
except:
    pass


# %%
prefix = Path("../").absolute()

optics_file = prefix / "summer_studies/collapse/opt_collapse_1100_1500.madx"
# optics_file = "opt_round_1100_xsuite.madx";
# optics_file = "./opt_round_1100_xsuite_bet0_800.madx"
# optics_file = "optics_xsuite/opt_round_1100_ats_1000.madx"
# optics_file = "optics_xsuite/opt_round_1100_ats_800.madx"
# optics_file = "optics_xsuite/opt_round_1100_ats_700.madx"
# optics_file = "optics_xsuite/opt_round_1100_ats_600.madx"

# optics_file = prefix / "summer_studies/SoL/opt_flathv_300_1200_1500.madx"
# optics_file = "optics_xsuite/opt_flathv_300_720_again.madx"
# optics_file = prefix / "summer_studies/collapse/opt_collapse_flathv_700_2800.madx"

# optics_file = "optics_xsuite/opt_flathv_700_2800_asymetric_test5.madx"

# optics_file = "optics_xsuite/opt_flathv_700_2800_asym_700_1200.madx"

# optics_file = "optics_xsuite/opt_flathv_700_2800_asym_700_2000.madx"

collider_name = "collider_hl16.json"


collider = xt.Multiline.from_json(collider_name)
collider.build_trackers()
collider.vars.load_madx_optics_file(optics_file)

collider.lhcb1.twiss_default["only_markers"] = True
collider.lhcb2.twiss_default["only_markers"] = True

collider.lhcb1.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["reverse"] = True

# %%
staged_match = True
first_pass = False
lm.set_var_limits_and_steps(collider)
collider.vars.vary_default.update(quads_ir15_defaults)

# %%
betx0_ip1 = 1.1
bety0_ip1 = 1.1
betx0_ip5 = 1.1
bety0_ip5 = 1.1

betx_ip1 = 1.1
bety_ip1 = 1.1
betx_ip5 = 1.1
bety_ip5 = 1.1
# %%

scxir1 = betx_ip1 / betx0_ip1
scyir1 = bety_ip1 / bety0_ip1
scxir5 = betx_ip5 / betx0_ip5
scyir5 = bety_ip5 / bety0_ip5
# %%
tw0b1 = collider["lhcb1"].cycle("ip3").twiss(method="4d")
tw0b2 = collider["lhcb2"].cycle("ip3").twiss(method="4d")


# %%
print(tw0b1[["betx", "bety"], "ip.*"])
print(tw0b2[["betx", "bety"], "ip.*"])
# %%
muxs = {}
muys = {}

saved_vars = {}
saved_vars.update(
    {
        "b1": {"ip2": {}, "ip3": {}, "ip4": {}, "ip6": {}, "ip7": {}, "ip8": {}},
        "b2": {"ip2": {}, "ip3": {}, "ip4": {}, "ip6": {}, "ip7": {}, "ip8": {}},
    }
)

for bim in ["b1", "b2"]:
    for arc_name in lm.ARC_NAMES:
        muxs[f"mux{arc_name}{bim}"] = collider.varval[f"mux{arc_name}{bim}"]
        muys[f"muy{arc_name}{bim}"] = collider.varval[f"muy{arc_name}{bim}"]
    for ip_name in [2, 4, 6, 8]:
        muxs[f"muxip{ip_name}{bim}"] = collider.varval[f"muxip{ip_name}{bim}"]
        muys[f"muyip{ip_name}{bim}"] = collider.varval[f"muyip{ip_name}{bim}"]
    for ip_name in [1, 5]:
        muxs[f"muxip{ip_name}{bim}_l"] = collider.varval[f"muxip{ip_name}{bim}_l"]
        muys[f"muyip{ip_name}{bim}_l"] = collider.varval[f"muyip{ip_name}{bim}_l"]
        muxs[f"muxip{ip_name}{bim}_r"] = collider.varval[f"muxip{ip_name}{bim}_r"]
        muys[f"muyip{ip_name}{bim}_r"] = collider.varval[f"muyip{ip_name}{bim}_r"]

    saved_vars[bim]["ip2"]["betx"] = collider.varval[f"betxip2{bim}"]
    saved_vars[bim]["ip2"]["bety"] = collider.varval[f"betyip2{bim}"]

    for ip_name in [3, 7]:
        for param in ["betx", "bety", "alfx", "alfy", "dx", "dpx", "mux", "muy"]:
            saved_vars[bim][f"ip{ip_name}"][param] = collider.varval[
                f"{param}ip{ip_name}{bim}"
            ]

    for ip_name in [4, 6, 8]:
        for param in ["betx", "bety", "alfx", "alfy", "dx", "dpx"]:
            saved_vars[bim][f"ip{ip_name}"][param] = collider.varval[
                f"{param}ip{ip_name}{bim}"
            ]


# %%
tw45_56_xt = lm.get_arc_periodic_solution(collider, arc_name=["45", "56"])
tw81_12_xt = lm.get_arc_periodic_solution(collider, arc_name=["81", "12"])

# %%
beta0 = tw45_56_xt["lhcb1"]["45"].get_twiss_init("s.ds.l5.b1")
beta0.mux = 0
beta0.muy = 0
tw_ip5_pre_0 = collider.lhcb1.twiss(
    ele_start="s.ds.l5.b1", ele_stop="e.ds.r5.b1", twiss_init=beta0
)
beta0 = tw81_12_xt["lhcb1"]["81"].get_twiss_init("s.ds.l1.b1")
beta0.mux = 0
beta0.muy = 0
tw_ip1_pre_0 = collider.lhcb1.cycle("ip3").twiss(
    ele_start="s.ds.l1.b1", ele_stop="e.ds.r1.b1", twiss_init=beta0, method="4d"
)
# %%
optimizers = {}
# %%
collider.lhcb1.cycle("ip3", inplace=True)
collider.lhcb2.cycle("ip3", inplace=True)
collider.lhcb1.twiss_default["only_markers"] = True
collider.lhcb2.twiss_default["only_markers"] = True

collider.lhcb1.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["reverse"] = True

# %%

get_ipython().run_line_magic("load_ext", "autoreload")
get_ipython().run_line_magic("autoreload", "2")
import rematch_ir15_seperately

# %%
collider.vars.vary_default.update(
    {
        "kqx1.l1": {"step": 1e-6, "limits": (-qtlim1, 0)},
        "kqx2a.l1": {"step": 1e-6, "limits": (-qtlim1, 0)},
        "kqx3.l1": {"step": 1e-6, "limits": (-qtlim1, 0)},
    }
)
# %%
optimizers["ir1"] = rematch_ir15_seperately.rematch_ir1(
    collider,
    betx0_ip1,
    bety0_ip1,
    tw_sq_a81_ip1_a12=tw81_12_xt,
    restore=False,
    solve=True,
    match_on_triplet=0,
    cycle=False,
)
# %%
print(optimizers["ir1"].target_status())
print(optimizers["ir1"].log())

# %%
optimizers["ir1_run2"] = rematch_ir15_seperately.rematch_ir1(
    collider,
    betx0_ip1,
    bety0_ip1,
    tw_sq_a81_ip1_a12=tw81_12_xt,
    restore=False,
    solve=True,
    match_on_triplet=0,
    cycle=False,
)

# %%
print(optimizers["ir1_run2"].target_status())
print(optimizers["ir1_run2"].log())
# print(optimizers["ir1_run2"].log().hit_limits)
# %%
optimizers["ir5"] = rematch_ir15_seperately.rematch_ir5(
    collider,
    betx0_ip5,
    bety0_ip5,
    tw_sq_a45_ip5_a56=tw45_56_xt,
    restore=False,
    solve=True,
    match_on_triplet=5,
)
# %%
print(optimizers["ir5"].target_status())
print(optimizers["ir5"].log())

# %%
optimizers["ir5_run2"] = rematch_ir15_seperately.rematch_ir5(
    collider,
    betx0_ip5,
    bety0_ip5,
    tw_sq_a45_ip5_a56=tw45_56_xt,
    restore=False,
    solve=True,
    match_on_triplet=0,
)
# %%
print(optimizers["ir5_run2"].target_status())
print(optimizers["ir5_run2"].log())

# %%
collider.lhcb1.cycle("lhcb1$start", inplace=True)
collider.lhcb2.cycle("lhcb2$start", inplace=True)
collider.lhcb1.twiss_default["only_markers"] = True
collider.lhcb2.twiss_default["only_markers"] = True

collider.lhcb1.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["reverse"] = True


# %%
tw81_45_xt = lm.get_arc_periodic_solution(collider, arc_name=["81", "45"])

beta0 = tw81_45_xt["lhcb1"]["45"].get_twiss_init("s.ds.l5.b1")
beta0.mux = 0
beta0.muy = 0
tw_ip5_pre = collider.lhcb1.twiss(
    ele_start="s.ds.l5.b1", ele_stop="e.ds.r5.b1", twiss_init=beta0
)


beta0 = tw81_45_xt["lhcb1"]["81"].get_twiss_init("s.ds.l1.b1")
beta0.mux = 0
beta0.muy = 0
tw_ip1_pre = collider.lhcb1.cycle("ip3").twiss(
    ele_start="s.ds.l1.b1", ele_stop="e.ds.r1.b1", twiss_init=beta0
)

print(tw_ip5_pre[["betx", "bety"], "ip5"])
print(tw_ip1_pre[["betx", "bety"], "ip1"])

# %%
print(
    "Old", tw_ip1_pre_0.rows[["mqxfb.a2r1_exit", "mqxfa.b3r1_exit"]].cols["betx bety"]
)
print("New", tw_ip1_pre.rows[["mqxfb.a2r1_exit", "mqxfa.b3r1_exit"]].cols["betx bety"])
print(
    "Old",
    tw_ip1_pre_0.rows["mqxfb.a2r1_exit"].bety
    / tw_ip1_pre_0.rows["mqxfa.b3r1_exit"].betx,
)
print(
    "New",
    tw_ip1_pre.rows["mqxfb.a2r1_exit"].bety / tw_ip1_pre.rows["mqxfa.b3r1_exit"].betx,
)
print(tw_ip1_pre.rows["acfca.4ar1.b1_exit"].bety / tw_ip1_pre.rows["acfca.4ar1.b1_exit"].betx)


print(tw_ip5_pre.rows[["mqxfb.a2r5_exit", "mqxfa.b3r5_exit"]].cols["betx bety"])

print(tw_ip5_pre.rows["mqxfb.a2r5_exit"].bety / tw_ip5_pre.rows["mqxfa.b3r5_exit"].betx)
print(
    tw_ip5_pre_0.rows["mqxfb.a2r5_exit"].bety
    / tw_ip5_pre_0.rows["mqxfa.b3r5_exit"].betx
)

# %%

fig, axs = plt.subplots(2, 1, figsize=(21, 11))
axs[0].set_title("IP5")
axs[0].plot(tw_ip5_pre.s, tw_ip5_pre.betx, color="black", label=r"New $\beta_x$")
axs[0].plot(tw_ip5_pre.s, tw_ip5_pre.bety, color="red", label=r"New $\beta_y$")

axs[0].plot(
    tw_ip5_pre_0.s, tw_ip5_pre_0.betx, ls="--", color="blue", label=r"Old $\beta_x$"
)
axs[0].plot(
    tw_ip5_pre_0.s, tw_ip5_pre_0.bety, ls="--", color="green", label=r"Old $\beta_y$"
)

axs[0].set_xlabel("s [m]")
axs[0].set_ylabel(r"$\beta_{x,y} [m]$")
axs[0].legend()

axs[1].set_title("IP1")
axs[1].plot(tw_ip1_pre.s, tw_ip1_pre.betx, color="black", label=r"New $\beta_x$")
axs[1].plot(tw_ip1_pre.s, tw_ip1_pre.bety, color="red", label=r"New $\beta_y$")

axs[1].plot(
    tw_ip1_pre_0.s, tw_ip1_pre_0.betx, ls="--", color="blue", label=r"Old $\beta_x$"
)
axs[1].plot(
    tw_ip1_pre_0.s, tw_ip1_pre_0.bety, ls="--", color="green", label=r"Old $\beta_y$"
)
# axs[1].axvline(x=tw_ip1_pre.rows["mqxfb.a2r1_exit"].s[0])
# axs[1].axvline(x=tw_ip1_pre.rows["mqxfb.b2r1_entry"].s[0])
[axs[1].axvline(x=s) for s in tw_ip1_pre.rows["acfca.*_exit"].s]
axs[1].set_xlabel("s [m]")
axs[1].set_ylabel(r"$\beta_{x,y} [m]$")
axs[1].legend()


plt.show()


# %%
def print_triplet_strengths(collider):
    # print("Triplet IR1")
    for quad in rematch_ir15_seperately.triplet_ir1.split():
        print(f"{quad} = {collider.varval[quad]};")

    # print("Quads IR1")
    for quad in rematch_ir15_seperately.quads_ir1_b1.split():
        print(f"{quad} = {collider.varval[quad]};")
    for quad in rematch_ir15_seperately.quads_ir1_b2.split():
        print(f"{quad} = {collider.varval[quad]};")

    # print("Triplet IR5")
    for quad in rematch_ir15_seperately.triplet_ir5.split():
        print(f"{quad} = {collider.varval[quad]};")

    # print("Quads IR5")
    for quad in rematch_ir15_seperately.quads_ir5_b1.split():
        print(f"{quad} = {collider.varval[quad]};")
    for quad in rematch_ir15_seperately.quads_ir5_b2.split():
        print(f"{quad} = {collider.varval[quad]};")


print_triplet_strengths(collider)
# %%
tw_non_ats_arcs = lm.get_arc_periodic_solution(
    collider, arc_name=["23", "34", "67", "78"]
)

# %%
for bim in ["b1", "b2"]:
    optimizers[f"ir3{bim}"] = lm.rematch_ir3(
        collider=collider,
        line_name=f"lhc{bim}",
        boundary_conditions_left=tw_non_ats_arcs[f"lhc{bim}"]["23"],
        boundary_conditions_right=tw_non_ats_arcs[f"lhc{bim}"]["34"],
        mux_ir3=saved_vars[bim]["ip3"]["mux"],
        muy_ir3=saved_vars[bim]["ip3"]["muy"],
        alfx_ip3=saved_vars[bim]["ip3"]["alfx"],
        alfy_ip3=saved_vars[bim]["ip3"]["alfy"],
        betx_ip3=saved_vars[bim]["ip3"]["betx"],
        bety_ip3=saved_vars[bim]["ip3"]["bety"],
        dx_ip3=saved_vars[bim]["ip3"]["dx"],
        dpx_ip3=saved_vars[bim]["ip3"]["dpx"],
        solve=True,
        staged_match=True,
        default_tol=default_tol,
    )

# %%
optimizers["ir3b1"].target_status()
optimizers["ir3b2"].target_status()
# %%
print(optimizers["ir3b1"].log())
print(optimizers["ir3b2"].log())
# %%
for bim in ["b1", "b2"]:
    optimizers[f"ir7{bim}"] = lm.rematch_ir7(
        collider=collider,
        line_name=f"lhc{bim}",
        boundary_conditions_left=tw_non_ats_arcs[f"lhc{bim}"]["67"],
        boundary_conditions_right=tw_non_ats_arcs[f"lhc{bim}"]["78"],
        mux_ir7=saved_vars[bim]["ip7"]["mux"],
        muy_ir7=saved_vars[bim]["ip7"]["muy"],
        alfx_ip7=saved_vars[bim]["ip7"]["alfx"],
        alfy_ip7=saved_vars[bim]["ip7"]["alfy"],
        betx_ip7=saved_vars[bim]["ip7"]["betx"],
        bety_ip7=saved_vars[bim]["ip7"]["bety"],
        dx_ip7=saved_vars[bim]["ip7"]["dx"],
        dpx_ip7=saved_vars[bim]["ip7"]["dpx"],
        solve=True,
        staged_match=True,
        default_tol=default_tol,
    )

# %%
optimizers["ir7b1"].target_status()
optimizers["ir7b2"].target_status()
# %%
print(optimizers["ir7b1"].log())
print(optimizers["ir7b2"].log())


# %%
tw_sq_a81_ip1_a12_b1 = lm.propagate_optics_from_beta_star(
    collider,
    ip_name="ip1",
    line_name="lhcb1",
    ele_start="s.ds.r8.b1",
    ele_stop="e.ds.l2.b1",
    beta_star_x=betx_ip1,
    beta_star_y=bety_ip1,
)

tw_sq_a45_ip5_a56_b1 = lm.propagate_optics_from_beta_star(
    collider,
    ip_name="ip5",
    line_name="lhcb1",
    ele_start="s.ds.r4.b1",
    ele_stop="e.ds.l6.b1",
    beta_star_x=betx_ip5,
    beta_star_y=bety_ip5,
)

tw_sq_a81_ip1_a12_b2 = lm.propagate_optics_from_beta_star(
    collider,
    ip_name="ip1",
    line_name="lhcb2",
    ele_start="s.ds.r8.b2",
    ele_stop="e.ds.l2.b2",
    beta_star_x=betx_ip1,
    beta_star_y=bety_ip1,
)

tw_sq_a45_ip5_a56_b2 = lm.propagate_optics_from_beta_star(
    collider,
    ip_name="ip5",
    line_name="lhcb2",
    ele_start="s.ds.r4.b2",
    ele_stop="e.ds.l6.b2",
    beta_star_x=betx_ip5,
    beta_star_y=bety_ip5,
)

# %%
(
    mux_ir2_target_b1,
    muy_ir2_target_b1,
    mux_ir4_target_b1,
    muy_ir4_target_b1,
    mux_ir6_target_b1,
    muy_ir6_target_b1,
    mux_ir8_target_b1,
    muy_ir8_target_b1,
) = lm.compute_ats_phase_advances_for_auxiliary_irs(
    "lhcb1",
    tw_sq_a81_ip1_a12_b1,
    tw_sq_a45_ip5_a56_b1,
    muxs["muxip1b1_l"],
    muys["muyip1b1_l"],
    muxs["muxip1b1_r"],
    muys["muyip1b1_r"],
    muxs["muxip5b1_l"],
    muys["muyip5b1_l"],
    muxs["muxip5b1_r"],
    muys["muyip5b1_r"],
    muxs["muxip2b1"],
    muys["muyip2b1"],
    muxs["muxip4b1"],
    muys["muyip4b1"],
    muxs["muxip6b1"],
    muys["muyip6b1"],
    muxs["muxip8b1"],
    muys["muyip8b1"],
    muxs["mux12b1"],
    muys["muy12b1"],
    muxs["mux45b1"],
    muys["muy45b1"],
    muxs["mux56b1"],
    muys["muy56b1"],
    muxs["mux81b1"],
    muys["muy81b1"],
)

(
    mux_ir2_target_b2,
    muy_ir2_target_b2,
    mux_ir4_target_b2,
    muy_ir4_target_b2,
    mux_ir6_target_b2,
    muy_ir6_target_b2,
    mux_ir8_target_b2,
    muy_ir8_target_b2,
) = lm.compute_ats_phase_advances_for_auxiliary_irs(
    "lhcb2",
    tw_sq_a81_ip1_a12_b2,
    tw_sq_a45_ip5_a56_b2,
    muxs["muxip1b2_l"],
    muys["muyip1b2_l"],
    muxs["muxip1b2_r"],
    muys["muyip1b2_r"],
    muxs["muxip5b2_l"],
    muys["muyip5b2_l"],
    muxs["muxip5b2_r"],
    muys["muyip5b2_r"],
    muxs["muxip2b2"],
    muys["muyip2b2"],
    muxs["muxip4b2"],
    muys["muyip4b2"],
    muxs["muxip6b2"],
    muys["muyip6b2"],
    muxs["muxip8b2"],
    muys["muyip8b2"],
    muxs["mux12b2"],
    muys["muy12b2"],
    muxs["mux45b2"],
    muys["muy45b2"],
    muxs["mux56b2"],
    muys["muy56b2"],
    muxs["mux81b2"],
    muys["muy81b2"],
)
# %%

optimizers["ir2b1"] = lm.rematch_ir2(
    collider,
    line_name="lhcb1",
    boundary_conditions_left=tw_sq_a81_ip1_a12_b1,
    boundary_conditions_right=tw_non_ats_arcs["lhcb1"]["23"],
    mux_ir2=mux_ir2_target_b1,
    muy_ir2=muy_ir2_target_b1,
    betx_ip2=saved_vars["b1"]["ip2"]["betx"],
    bety_ip2=saved_vars["b1"]["ip2"]["bety"],
    solve=True,
    staged_match=staged_match,
    default_tol=default_tol,
)


optimizers["ir2b2"] = rematch_ir2(
    collider,
    line_name="lhcb2",
    boundary_conditions_left=tw_sq_a81_ip1_a12_b2,
    boundary_conditions_right=tw_non_ats_arcs["lhcb2"]["23"],
    mux_ir2=mux_ir2_target_b2,
    muy_ir2=muy_ir2_target_b2,
    betx_ip2=saved_vars["b2"]["ip2"]["betx"],
    bety_ip2=saved_vars["b2"]["ip2"]["bety"],
    solve=True,
    staged_match=staged_match,
    default_tol=default_tol,
)


# %%
optimizers["ir2b1"].target_status()
optimizers["ir2b2"].target_status()
# %%
print(optimizers["ir2b1"].log())
print(optimizers["ir2b2"].log())
# %%

optimizers["ir8b1"] = lm.rematch_ir8(
    collider,
    line_name="lhcb1",
    boundary_conditions_left=tw_non_ats_arcs["lhcb1"]["78"],
    boundary_conditions_right=tw_sq_a81_ip1_a12_b1,
    solve=True,
    mux_ir8=mux_ir8_target_b1,
    muy_ir8=muy_ir8_target_b1,
    alfx_ip8=saved_vars["b1"]["ip8"]["alfx"],
    alfy_ip8=saved_vars["b1"]["ip8"]["alfy"],
    betx_ip8=saved_vars["b1"]["ip8"]["betx"],
    bety_ip8=saved_vars["b1"]["ip8"]["bety"],
    dx_ip8=saved_vars["b1"]["ip8"]["dx"],
    dpx_ip8=saved_vars["b1"]["ip8"]["dpx"],
    staged_match=staged_match,
    default_tol=default_tol,
)

optimizers["ir8b2"] = lm.rematch_ir8(
    collider,
    line_name="lhcb2",
    boundary_conditions_left=tw_non_ats_arcs["lhcb2"]["78"],
    boundary_conditions_right=tw_sq_a81_ip1_a12_b2,
    solve=True,
    mux_ir8=mux_ir8_target_b2,
    muy_ir8=muy_ir8_target_b2,
    alfx_ip8=saved_vars["b2"]["ip8"]["alfx"],
    alfy_ip8=saved_vars["b2"]["ip8"]["alfy"],
    betx_ip8=saved_vars["b2"]["ip8"]["betx"],
    bety_ip8=saved_vars["b2"]["ip8"]["bety"],
    dx_ip8=saved_vars["b2"]["ip8"]["dx"],
    dpx_ip8=saved_vars["b2"]["ip8"]["dpx"],
    staged_match=staged_match,
    default_tol=default_tol,
)


# %%
optimizers["ir8b1"].target_status()
optimizers["ir8b2"].target_status()
# %%
print(optimizers["ir8b1"].log())
print(optimizers["ir8b2"].log())

# %%
optimizers["ir4b1"] = rematch_ir4(
    collider=collider,
    line_name="lhcb1",
    boundary_conditions_left=tw_non_ats_arcs["lhcb1"]["34"],
    boundary_conditions_right=tw_sq_a45_ip5_a56_b1,
    mux_ir4=mux_ir4_target_b1,
    muy_ir4=muy_ir4_target_b1,
    alfx_ip4=saved_vars["b1"]["ip4"]["alfx"],
    alfy_ip4=saved_vars["b1"]["ip4"]["alfy"],
    betx_ip4=saved_vars["b1"]["ip4"]["betx"],
    bety_ip4=saved_vars["b1"]["ip4"]["bety"],
    dx_ip4=saved_vars["b1"]["ip4"]["dx"],
    dpx_ip4=saved_vars["b1"]["ip4"]["dpx"],
    solve=True,
    staged_match=staged_match,
    default_tol=default_tol,
)

optimizers["ir4b2"] = rematch_ir4(
    collider=collider,
    line_name="lhcb2",
    boundary_conditions_left=tw_non_ats_arcs["lhcb2"]["34"],
    boundary_conditions_right=tw_sq_a45_ip5_a56_b2,
    mux_ir4=mux_ir4_target_b2,
    muy_ir4=muy_ir4_target_b2,
    alfx_ip4=saved_vars["b2"]["ip4"]["alfx"],
    alfy_ip4=saved_vars["b2"]["ip4"]["alfy"],
    betx_ip4=saved_vars["b2"]["ip4"]["betx"],
    bety_ip4=saved_vars["b2"]["ip4"]["bety"],
    dx_ip4=saved_vars["b2"]["ip4"]["dx"],
    dpx_ip4=saved_vars["b2"]["ip4"]["dpx"],
    solve=True,
    staged_match=staged_match,
    default_tol=default_tol,
)
# %%
optimizers["ir4b1"].target_status()
optimizers["ir4b2"].target_status()
# %%
print(optimizers["ir4b1"].log())
print(optimizers["ir4b2"].log())
# %%
optimizers["ir6b1"] = rematch_ir6(
    collider=collider,
    line_name="lhcb1",
    boundary_conditions_left=tw_sq_a45_ip5_a56_b1,
    boundary_conditions_right=tw_non_ats_arcs["lhcb1"]["67"],
    mux_ir6=mux_ir6_target_b1,
    muy_ir6=muy_ir6_target_b1,
    alfx_ip6=saved_vars["b1"]["ip6"]["alfx"],
    alfy_ip6=saved_vars["b1"]["ip6"]["alfy"],
    betx_ip6=saved_vars["b1"]["ip6"]["betx"],
    bety_ip6=saved_vars["b1"]["ip6"]["bety"],
    dx_ip6=saved_vars["b1"]["ip6"]["dx"],
    dpx_ip6=saved_vars["b1"]["ip6"]["dpx"],
    solve=True,
    staged_match=staged_match,
    default_tol=default_tol,
)
# %%
optimizers["ir6b2"] = rematch_ir6(
    collider=collider,
    line_name="lhcb2",
    boundary_conditions_left=tw_sq_a45_ip5_a56_b2,
    boundary_conditions_right=tw_non_ats_arcs["lhcb2"]["67"],
    mux_ir6=mux_ir6_target_b2,
    muy_ir6=muy_ir6_target_b2,
    alfx_ip6=saved_vars["b2"]["ip6"]["alfx"],
    alfy_ip6=saved_vars["b2"]["ip6"]["alfy"],
    betx_ip6=saved_vars["b2"]["ip6"]["betx"],
    bety_ip6=saved_vars["b2"]["ip6"]["bety"],
    dx_ip6=saved_vars["b2"]["ip6"]["dx"],
    dpx_ip6=saved_vars["b2"]["ip6"]["dpx"],
    solve=True,
    staged_match=staged_match,
    default_tol=default_tol,
)
# %%
optimizers["ir6b1"].target_status()
optimizers["ir6b2"].target_status()
# %%
print(optimizers["ir6b1"].log())
print(optimizers["ir6b2"].log())


# %%
# optimizers["orbit_knobs"] = lm.match_orbit_knobs_ip2_ip8(collider)
optimizers["orbit_knobs"] = match_orbit_knobs_ip2_ip8(collider, first_pass=first_pass)

# %%
for key, val in optimizers["orbit_knobs"].items():
    print(20 * "#", key, 20 * "#")
    print(val.log())

# %%
optimizers["tune"] = rematch_tune(collider, 62.31, 60.32, 62.31, 60.32, solve=True)
optimizers["chroma"] = rematch_chroma(collider, 2, 2, 2, 2, solve=True)

# %%
optimizers["tune"]["b1"].target_status()
optimizers["tune"]["b2"].target_status()
optimizers["chroma"]["b1"].target_status()
optimizers["chroma"]["b2"].target_status()

# %%
print(optimizers["tune"]["b1"].log())
print(optimizers["tune"]["b2"].log())
print(optimizers["chroma"]["b1"].log())
print(optimizers["chroma"]["b2"].log())


# %%
def disable_crossing_all(collider):
    collider.varval["on_x1"] = 0
    collider.varval["on_x5"] = 0
    collider.varval["on_x2"] = 0
    collider.varval["on_sep2"] = 0
    collider.varval["on_x8v"] = 0
    collider.varval["on_sep8h"] = 0
    collider.varval["on_sep8v"] = 0


def enable_crossing_all(collider):
    collider.varval["on_x1"] = 250
    collider.varval["on_x5"] = 250
    collider.varval["on_x2"] = -170
    collider.varval["on_sep2"] = 0.138
    collider.varval["on_x8v"] = 170
    collider.varval["on_sep8h"] = -0.01
    collider.varval["on_sep8v"] = 0.01


# %%

disable_crossing_all(collider)
# enable_crossing_all(collider)

twb1 = collider["lhcb1"].cycle("ip3").twiss(method="4d")
twb2 = collider["lhcb2"].cycle("ip3").twiss(method="4d")

# %%
print(twb1[["betx", "bety", "x", "y", "px", "py"], "ip.*"])
print(twb2[["betx", "bety", "x", "y", "px", "py"], "ip.*"])
# %%
rra = "s.ds.l5.b1"
rrb = "e.ds.r5.b1"

# rra = "s.ds.l1.b1"
# rrb = "e.ds.r1.b1"

# rra = None
# rrb = None

fig, axs = plt.subplots(figsize=(21, 11))
axs.plot(
    twb1["s", rra:rrb],
    twb1["betx", rra:rrb],
    label=r"$\beta_x$, $\beta_x^*$ new",
    color="black",
)
axs.plot(
    twb1["s", rra:rrb],
    twb1["bety", rra:rrb],
    label=r"$\beta_y$, $\beta_y^*$ new",
    color="red",
)
axs.plot(
    tw0b1["s", rra:rrb],
    tw0b1["betx", rra:rrb],
    label=r"$\beta_x$, $\beta_x^*$ old",
    ls="--",
    color="grey",
)
axs.plot(
    tw0b1["s", rra:rrb],
    tw0b1["bety", rra:rrb],
    label=r"$\beta_y$, $\beta_y^*$ old",
    ls="--",
    color="blue",
)
axs_t = axs.twiny()
axs_t.set_xticks(
    tw0b1[["s"], "ip.*"],
    tw0b1[["name"], "ip.*"],
)
axs_t.set_xlim(axs.get_xlim())
[axs_t.axvline(xpos, linestyle="--") for xpos in tw0b1[["s"], "ip.*"]]
axs.set_xlabel("s [m]")
axs.set_ylabel(r"$\beta_{x,y}$ [m]")
axs.legend()
plt.show()

# %%
rra = "s.ds.l5.b1"
rrb = "e.ds.r5.b1"
rra = None
rrb = None

fig, axs = plt.subplots(figsize=(11, 11))
axs.plot(
    twb1["s", rra:rrb],
    twb1["x", rra:rrb],
    label=r"$x$, $\beta_x^*$ new",
)
axs.plot(
    twb1["s", rra:rrb],
    twb1["y", rra:rrb],
    label=r"$y$, $\beta_y^*$ new",
)
axs.plot(
    tw0b1["s", rra:rrb],
    tw0b1["x", rra:rrb],
    label=r"$x$, $\beta_x^*$ old",
    ls="--",
)
axs.plot(
    tw0b1["s", rra:rrb],
    tw0b1["y", rra:rrb],
    label=r"$y$, $\beta_y^*$ old",
    ls="--",
)
axs_t = axs.twiny()
axs_t.set_xticks(
    tw0b1[["s"], "ip.*"],
    tw0b1[["name"], "ip.*"],
)
axs_t.set_xlim(axs.get_xlim())
[axs_t.axvline(xpos, linestyle="--") for xpos in tw0b1[["s"], "ip.*"]]
axs.set_xlabel("s [m]")
axs.set_ylabel(r"$CO [m]$")
axs.legend()
plt.show()
# %%
plot_w(collider, "lhcb1")

# %%
os.makedirs("optics_xsuite", exist_ok=True)
save_optics_hl(
    collider,
    f"optics_xsuite/opt_flathv_{int(bety_ip1*1000)}_{int(betx_ip1*1000)}_asym_700_2200_noir6b1.madx",
)

# %%
