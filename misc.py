import xtrack as xt
import xpart as xp
import xtrack._temp.lhc_match as lm
import numpy as np
from cpymad.madx import Madx
import matplotlib.pyplot as plt
import matplotlib.patches as patches

default_tol = {
    None: 1e-8,
    "betx": 1e-6,
    "bety": 1e-6,
}  # to have no rematching w.r.t. madx

cols = ["betx", "alfx", "mux", "bety", "alfy", "muy", "dx", "dpx"]


def disable_crossing_all(collider):
    collider.varval["on_x1"] = 0
    collider.varval["on_x5"] = 0
    collider.varval["on_x2"] = 0
    collider.varval["on_sep2"] = 0
    collider.varval["on_x8v"] = 0
    collider.varval["on_sep8h"] = 0
    collider.varval["on_sep8v"] = 0


def enable_crossing_all(collider):
    collider.varval["on_x1"] = 250
    collider.varval["on_x5"] = 250
    collider.varval["on_x2"] = -170
    collider.varval["on_sep2"] = 0.138
    collider.varval["on_x8v"] = 170
    collider.varval["on_sep8h"] = -0.01
    collider.varval["on_sep8v"] = 0.01


def plot_orbit(tw, line_name, title=None):
    fig, axs = plt.subplots(figsize=(21, 11))
    if title is None:
        axs.set_title(line_name, fontsize=34)
    else:
        axs.set_title(f"{line_name}, {title}")
    axs.plot(tw[line_name].s, tw[line_name].x, label="x", color="black")
    axs.plot(tw[line_name].s, tw[line_name].y, label="y", color="red")
    axs_t = axs.twiny()
    axs_t.set_xticks(
        tw[line_name][["s"], "ip.*"],
        tw[line_name][["name"], "ip.*"],
        rotation="horizontal",
    )
    axs_t.set_xlim(axs.get_xlim()[0], axs.get_xlim()[1])
    axs.set_ylabel(r"$co [m]$", fontsize=30)
    axs.set_xlabel(r"$s [m]$", fontsize=30)
    axs.legend(fontsize=30)
    axs.grid()
    plt.show()


def plot_w(collider, line_name, rl=None, rh=None):
    tw = collider[line_name].cycle("ip3").twiss(method="4d")
    fig, axs = plt.subplots(figsize=(11, 11))
    axs.plot(
        tw["s", rl:rh],
        tw["wx_chrom", rl:rh],
        label=r"$W_x$",
        color="black",
    )
    axs.plot(
        tw["s", rl:rh],
        tw["wy_chrom", rl:rh],
        label=r"$W_y$",
        color="red",
    )

    axs_t = axs.twiny()
    axs_t.set_xticks(
        tw[["s"], "ip.*"],
        tw[["name"], "ip.*"],
    )
    axs_t.set_xlim(axs.get_xlim())
    [axs_t.axvline(xpos, linestyle="--") for xpos in tw[["s"], "ip.*"]]
    axs.set_xlabel("s [m]")
    axs.set_ylabel(r"$W_{x,y}$")
    axs.legend()
    plt.show()


quads_ir15_knob_names = (
    "kqx1.l5 kqx2a.l5 kqx2b.l5 kqx3.l5 "
    "kq4.l5b1 kq4.r5b1 kq5.l5b1 kq5.r5b1 kq6.l5b1 kq6.r5b1 "
    "kq7.l5b1 kq7.r5b1 kq8.l5b1 kq8.r5b1 kq9.l5b1 kq9.r5b1 "
    "kq10.l5b1 kq10.r5b1 kqtl11.l5b1 kqtl11.r5b1 "
    "kqt12.l5b1 kqt12.r5b1 kqt13.l5b1 kqt13.r5b1"
)

quads_ir15_names = [
    "mqxfa.a1l5",
    "mqxfa.b1l5",
    "mqxfb.a2l5",
    "mqxfb.b2l5",
    "mqxfa.a3l5",
    "mqxfa.b3l5",
    "mqy.4l5.b1",
    "mqml.5l5.b1",
    "mqml.6l5.b1",
    "mqm.b7l5.b1",
    "mqml.8l5.b1",
    "mqm.9l5.b1",
    "mqml.10l5.b1",
    "mqtli.11l5.b1",
    "mqt.12l5.b1",
    "mqt.13l5.b1",
]


def plot_ir15_quads(collider, line_name, ir="5"):
    lenghts = []
    strengths = []
    positions = []

    elem_table = collider[line_name].get_table()

    for qq in quads_ir15_names:
        if ir == "1":
            qq = qq.replace("l5", "l1")
        lenghts.append(collider[line_name].element_dict[qq].length)
        strengths.append(collider[line_name].element_dict[qq].k1)
        positions.append(elem_table.rows[qq].s)
        if ir == "1":
            qq = qq.replace("l1", "r1")
        elif ir == "5":
            qq = qq.replace("l5", "r5")

        lenghts.append(collider[line_name].element_dict[qq].length)
        strengths.append(collider[line_name].element_dict[qq].k1)
        positions.append(elem_table.rows[qq].s)

    return positions, lenghts, strengths


def plotLatticeSeries(ax, s, l, height=1.0, v_offset=0.0, color="r", alpha=0.5, lw=3):
    # aux=series
    ax.add_patch(
        patches.Rectangle(
            (s - l, v_offset - height / 2.0),  # (x,y)
            l,  # width
            height,  # height
            color=color,
            alpha=alpha,
            lw=lw,
        )
    )
    return


def build_collider(optics_name, cycling=False):
    mad1 = Madx()
    mad1.call("../acc-models-lhc/lhc.seq")
    mad1.call("../acc-models-lhc/hllhc_sequence.madx")
    mad1.input("beam, sequence=lhcb1, particle=proton, energy=7000;")
    mad1.use("lhcb1")
    mad1.call(optics_name)

    mad4 = Madx()
    mad4.input("mylhcbeam=4")
    mad4.call("../acc-models-lhc/lhcb4.seq")
    mad4.call("../acc-models-lhc/hllhc_sequence.madx")
    mad4.input("beam, sequence=lhcb2, particle=proton, energy=7000;")
    mad4.use("lhcb2")
    mad4.call(optics_name)

    line1 = xt.Line.from_madx_sequence(
        mad1.sequence.lhcb1,
        allow_thick=True,
        deferred_expressions=True,
        replace_in_expr={"bv_aux": "bvaux_b1"},
    )

    line4 = xt.Line.from_madx_sequence(
        mad4.sequence.lhcb2,
        allow_thick=True,
        deferred_expressions=True,
        replace_in_expr={"bv_aux": "bvaux_b2"},
    )

    if cycling:
        line1.cycle("ip3", inplace=True)
        line4.cycle("ip3", inplace=True)

    # Remove solenoids (cannot backtwiss for now)
    for ll in [line1, line4]:
        tt = ll.get_table()
        for nn in tt.rows[tt.element_type == "Solenoid"].name:
            ee_elen = ll[nn].length
            ll.element_dict[nn] = xt.Drift(length=ee_elen)

    collider = xt.Multiline(lines={"lhcb1": line1, "lhcb2": line4})
    collider.lhcb1.particle_ref = xp.Particles(mass0=xp.PROTON_MASS_EV, p0c=7000e9)
    collider.lhcb2.particle_ref = xp.Particles(mass0=xp.PROTON_MASS_EV, p0c=7000e9)

    collider.lhcb1.twiss_default["method"] = "4d"
    collider.lhcb2.twiss_default["method"] = "4d"
    collider.lhcb2.twiss_default["reverse"] = True

    collider.build_trackers()

    return collider


strengths_to_check = [
    "kq4.l5b1",
    "kq4.r5b1",
    "kq5.l5b1",
    "kq5.r5b1",
    "kq6.l5b1",
    "kq6.r5b1",
    "kq7.l5b1",
    "kq7.r5b1",
    "kq8.l5b1",
    "kq9.l5b1",
    "kq10.l5b1",
    "kq8.r5b1",
    "kq9.r5b1",
    "kq10.r5b1",
    "kqtl11.l5b1",
    "kqtl11.r5b1",
    "kqt12.l5b1",
    "kqt12.r5b1",
    "kqt13.l5b1",
    "kqt13.r5b1",
]


def connect_lr_qx(collider, nqx=0, ir=5):
    """Connect specified right kqx in IP5 of IP1 with the left kqx"""
    assert ir == 1 or ir == 5
    collider.vars[f"kqx{nqx}.r{ir}"] = -collider.vars[f"kqx{nqx}.l{ir}"]


def connect_lr_qs(collider, nq=0, ir=5, bim=1):
    """Connect specified kq in ir for either beam with the left kq"""
    collider.vars[f"kq{nq}.r{ir}b{bim}"] = -collider.vars[f"kq{nq}.l{ir}b{bim}"]


def connect_q2s(collider, ir=5):
    """Connect Q2 in IP5, or IP1 to be used as one for matching"""
    assert ir == 1 or ir == 5
    collider.vars[f"kqx2b.l{ir}"] = +collider.vars[f"kqx2a.l{ir}"]
    collider.vars[f"kqx2a.r{ir}"] = -collider.vars[f"kqx2a.l{ir}"]
    collider.vars[f"kqx2b.r{ir}"] = -collider.vars[f"kqx2a.l{ir}"]


def rematch_tune(collider, targets, solve=False):
    optimizers = {}

    for line_name in collider.line_names:
        bim = line_name[-2:]
        opt = collider[line_name].match(
            solve=False,
            assert_within_tol=False,
            restore_if_fail=True,
            verbose=False,
            n_steps_max=5,
            targets=[
                xt.TargetSet(qx=targets[bim]["qx"], qy=targets[bim]["qy"], tol=1e-6)
            ],
            vary=xt.VaryList([f"kqtf.{bim}", f"kqtd.{bim}"], step=1e-8),
        )

        if solve:
            opt.solve()
        optimizers[bim] = opt
    return optimizers


def rematch_chroma(collider, targets, solve=False):
    optimizers = {}
    for line_name in collider.line_names:
        bim = line_name[-2:]
        opt = collider[line_name].match(
            solve=False,
            assert_within_tol=False,
            restore_if_fail=True,
            verbose=False,
            n_steps_max=10,
            targets=[
                xt.TargetSet(qx=targets[bim]["dqx"], qy=targets[bim]["dqy"], tol=1e-4)
            ],
            vary=xt.VaryList([f"ksf.{bim}", f"ksd.{bim}"], step=1e-8),
        )

        if solve:
            opt.solve()
        optimizers[bim] = opt
    return optimizers


def set_sext_all(collider, ksf_b1, ksd_b1, ksf_b2, ksd_b2):
    collider.varval["ksd1.a12b1"] = ksd_b1
    collider.varval["ksd1.a56b1"] = ksd_b1
    collider.varval["ksd2.a12b1"] = ksd_b1
    collider.varval["ksd2.a56b1"] = ksd_b1
    collider.varval["ksf1.a12b1"] = ksf_b1
    collider.varval["ksf1.a56b1"] = ksf_b1
    collider.varval["ksf2.a12b1"] = ksf_b1
    collider.varval["ksf2.a56b1"] = ksf_b1
    collider.varval["ksd1.a12b2"] = ksd_b2
    collider.varval["ksd1.a56b2"] = ksd_b2
    collider.varval["ksd2.a12b2"] = ksd_b2
    collider.varval["ksd2.a56b2"] = ksd_b2
    collider.varval["ksf1.a12b2"] = ksf_b2
    collider.varval["ksf1.a56b2"] = ksf_b2
    collider.varval["ksf2.a12b2"] = ksf_b2
    collider.varval["ksf2.a56b2"] = ksf_b2

    collider.vars["ksd1.a23b1"]._set_value(ksd_b1)
    collider.vars["ksd1.a67b1"]._set_value(ksd_b1)
    collider.vars["ksd2.a23b1"]._set_value(ksd_b1)
    collider.vars["ksd2.a67b1"]._set_value(ksd_b1)
    collider.vars["ksf1.a23b1"]._set_value(ksf_b1)
    collider.vars["ksf1.a67b1"]._set_value(ksf_b1)
    collider.vars["ksf2.a23b1"]._set_value(ksf_b1)
    collider.vars["ksf2.a67b1"]._set_value(ksf_b1)
    collider.vars["ksd1.a23b2"]._set_value(ksd_b2)
    collider.vars["ksd1.a67b2"]._set_value(ksd_b2)
    collider.vars["ksd2.a23b2"]._set_value(ksd_b2)
    collider.vars["ksd2.a67b2"]._set_value(ksd_b2)
    collider.vars["ksf1.a23b2"]._set_value(ksf_b2)
    collider.vars["ksf1.a67b2"]._set_value(ksf_b2)
    collider.vars["ksf2.a23b2"]._set_value(ksf_b2)
    collider.vars["ksf2.a67b2"]._set_value(ksf_b2)

    collider.vars["ksd1.a34b1"]._set_value(ksd_b1)
    collider.vars["ksd1.a78b1"]._set_value(ksd_b1)
    collider.vars["ksd2.a34b1"]._set_value(ksd_b1)
    collider.vars["ksd2.a78b1"]._set_value(ksd_b1)
    collider.vars["ksf1.a34b1"]._set_value(ksf_b1)
    collider.vars["ksf1.a78b1"]._set_value(ksf_b1)
    collider.vars["ksf2.a34b1"]._set_value(ksf_b1)
    collider.vars["ksf2.a78b1"]._set_value(ksf_b1)
    collider.vars["ksd1.a34b2"]._set_value(ksd_b2)
    collider.vars["ksd1.a78b2"]._set_value(ksd_b2)
    collider.vars["ksd2.a34b2"]._set_value(ksd_b2)
    collider.vars["ksd2.a78b2"]._set_value(ksd_b2)
    collider.vars["ksf1.a34b2"]._set_value(ksf_b2)
    collider.vars["ksf1.a78b2"]._set_value(ksf_b2)
    collider.vars["ksf2.a34b2"]._set_value(ksf_b2)
    collider.vars["ksf2.a78b2"]._set_value(ksf_b2)

    collider.varval["ksd1.a45b1"] = ksd_b1
    collider.varval["ksd1.a81b1"] = ksd_b1
    collider.varval["ksd2.a45b1"] = ksd_b1
    collider.varval["ksd2.a81b1"] = ksd_b1
    collider.varval["ksf1.a45b1"] = ksf_b1
    collider.varval["ksf1.a81b1"] = ksf_b1
    collider.varval["ksf2.a45b1"] = ksf_b1
    collider.varval["ksf2.a81b1"] = ksf_b1
    collider.varval["ksd1.a45b2"] = ksd_b2
    collider.varval["ksd1.a81b2"] = ksd_b2
    collider.varval["ksd2.a45b2"] = ksd_b2
    collider.varval["ksd2.a81b2"] = ksd_b2
    collider.varval["ksf1.a45b2"] = ksf_b2
    collider.varval["ksf1.a81b2"] = ksf_b2
    collider.varval["ksf2.a45b2"] = ksf_b2
    collider.varval["ksf2.a81b2"] = ksf_b2


# Adapted from xtrack, with the addition of the first_pass flag
# to not reset the orbit knobs when coming from a madx optics file
# generated with xtrack
def match_orbit_knobs_ip2_ip8(collider, first_pass=False):
    if first_pass:
        all_knobs_ip2ip8 = [
            "acbxh3.r2",
            "acbchs5.r2b1",
            "pxip2b1",
            "acbxh2.l8",
            "acbyhs4.r8b2",
            "pyip2b1",
            "acbxv1.l8",
            "acbyvs4.l2b1",
            "acbxh1.l8",
            "acbxv2.r8",
            "pxip8b2",
            "yip8b1",
            "pxip2b2",
            "acbcvs5.r2b1",
            "acbyhs4.l8b1",
            "acbyvs4.l8b1",
            "acbxh2.l2",
            "acbxh3.l2",
            "acbxv1.r8",
            "acbxv1.r2",
            "acbyvs4.r2b2",
            "acbyvs4.l2b2",
            "yip8b2",
            "xip2b2",
            "acbxh2.r2",
            "acbyhs4.l2b2",
            "acbxv2.r2",
            "acbyhs5.r8b1",
            "acbxh2.r8",
            "acbxv3.r8",
            "acbyvs5.r8b2",
            "acbyvs5.l2b2",
            "yip2b1",
            "acbxv2.l2",
            "acbyhs4.r2b2",
            "acbyhs4.r2b1",
            "xip8b2",
            "acbyvs5.l2b1",
            "acbyvs4.r8b1",
            "acbyvs4.r8b2",
            "acbyvs5.r8b1",
            "acbxh1.r8",
            "acbyvs4.l8b2",
            "acbyhs5.l2b1",
            "acbyvs4.r2b1",
            "acbcvs5.r2b2",
            "acbcvs5.l8b2",
            "acbyhs4.r8b1",
            "pxip8b1",
            "acbxv1.l2",
            "yip2b2",
            "acbyhs4.l8b2",
            "acbxv3.r2",
            "xip8b1",
            "acbchs5.r2b2",
            "acbxh3.l8",
            "acbxh3.r8",
            "acbyhs5.r8b2",
            "acbxv2.l8",
            "acbxh1.l2",
            "pyip8b1",
            "pyip8b2",
            "acbxv3.l8",
            "xip2b1",
            "acbyhs5.l2b2",
            "acbchs5.l8b2",
            "acbcvs5.l8b1",
            "pyip2b2",
            "acbxv3.l2",
            "acbchs5.l8b1",
            "acbyhs4.l2b1",
            "acbxh1.r2",
        ]

        # kill all existing knobs
        for kk in all_knobs_ip2ip8:
            collider.vars[kk] = 0

    twinit_zero_orbit = [xt.TwissInit(), xt.TwissInit()]

    targets_close_bump = [
        xt.TargetSet(line="lhcb1", at=xt.END, x=0, px=0, y=0, py=0),
        xt.TargetSet(line="lhcb2", at=xt.END, x=0, px=0, y=0, py=0),
    ]

    bump_range_ip2 = {
        "ele_start": ["s.ds.l2.b1", "s.ds.l2.b2"],
        "ele_stop": ["e.ds.r2.b1", "e.ds.r2.b2"],
    }
    bump_range_ip8 = {
        "ele_start": ["s.ds.l8.b1", "s.ds.l8.b2"],
        "ele_stop": ["e.ds.r8.b1", "e.ds.r8.b2"],
    }

    correctors_ir2_single_beam_h = [
        "acbyhs4.l2b1",
        "acbyhs4.r2b2",
        "acbyhs4.l2b2",
        "acbyhs4.r2b1",
        "acbyhs5.l2b2",
        "acbyhs5.l2b1",
        "acbchs5.r2b1",
        "acbchs5.r2b2",
    ]

    correctors_ir2_single_beam_v = [
        "acbyvs4.l2b1",
        "acbyvs4.r2b2",
        "acbyvs4.l2b2",
        "acbyvs4.r2b1",
        "acbyvs5.l2b2",
        "acbyvs5.l2b1",
        "acbcvs5.r2b1",
        "acbcvs5.r2b2",
    ]

    correctors_ir8_single_beam_h = [
        "acbyhs4.l8b1",
        "acbyhs4.r8b2",
        "acbyhs4.l8b2",
        "acbyhs4.r8b1",
        "acbchs5.l8b2",
        "acbchs5.l8b1",
        "acbyhs5.r8b1",
        "acbyhs5.r8b2",
    ]

    correctors_ir8_single_beam_v = [
        "acbyvs4.l8b1",
        "acbyvs4.r8b2",
        "acbyvs4.l8b2",
        "acbyvs4.r8b1",
        "acbcvs5.l8b2",
        "acbcvs5.l8b1",
        "acbyvs5.r8b1",
        "acbyvs5.r8b2",
    ]

    correctors_ir2_common_h = [
        "acbxh1.l2",
        "acbxh2.l2",
        "acbxh3.l2",
        "acbxh1.r2",
        "acbxh2.r2",
        "acbxh3.r2",
    ]

    correctors_ir2_common_v = [
        "acbxv1.l2",
        "acbxv2.l2",
        "acbxv3.l2",
        "acbxv1.r2",
        "acbxv2.r2",
        "acbxv3.r2",
    ]

    correctors_ir8_common_h = [
        "acbxh1.l8",
        "acbxh2.l8",
        "acbxh3.l8",
        "acbxh1.r8",
        "acbxh2.r8",
        "acbxh3.r8",
    ]

    correctors_ir8_common_v = [
        "acbxv1.l8",
        "acbxv2.l8",
        "acbxv3.l8",
        "acbxv1.r8",
        "acbxv2.r8",
        "acbxv3.r8",
    ]

    #########################
    # Match IP offset knobs #
    #########################

    offset_match = 0.5e-3

    # ---------- on_o2v ----------

    opt_o2v = collider.match_knob(
        knob_name="on_o2v",
        knob_value_end=(offset_match * 1e3),
        targets=(
            targets_close_bump
            + [
                xt.TargetSet(line="lhcb1", at="ip2", y=offset_match, py=0),
                xt.TargetSet(line="lhcb2", at="ip2", y=offset_match, py=0),
            ]
        ),
        vary=xt.VaryList(correctors_ir2_single_beam_v),
        run=False,
        twiss_init=twinit_zero_orbit,
        **bump_range_ip2,
    )
    opt_o2v.solve()
    opt_o2v.generate_knob()

    # ---------- on_o2h ----------

    opt_o2h = collider.match_knob(
        knob_name="on_o2h",
        knob_value_end=(offset_match * 1e3),
        targets=(
            targets_close_bump
            + [
                xt.TargetSet(line="lhcb1", at="ip2", x=offset_match, px=0),
                xt.TargetSet(line="lhcb2", at="ip2", x=offset_match, px=0),
            ]
        ),
        vary=xt.VaryList(correctors_ir2_single_beam_h),
        run=False,
        twiss_init=twinit_zero_orbit,
        **bump_range_ip2,
    )
    opt_o2h.solve()
    opt_o2h.generate_knob()

    # ---------- on_o8v ----------

    opt_o8v = collider.match_knob(
        knob_name="on_o8v",
        knob_value_end=(offset_match * 1e3),
        targets=(
            targets_close_bump
            + [
                xt.TargetSet(line="lhcb1", at="ip8", y=offset_match, py=0),
                xt.TargetSet(line="lhcb2", at="ip8", y=offset_match, py=0),
            ]
        ),
        vary=xt.VaryList(correctors_ir8_single_beam_v),
        run=False,
        twiss_init=twinit_zero_orbit,
        **bump_range_ip8,
    )
    opt_o8v.solve()
    opt_o8v.generate_knob()

    # ---------- on_o8h ----------

    opt_o8h = collider.match_knob(
        knob_name="on_o8h",
        knob_value_end=(offset_match * 1e3),
        targets=(
            targets_close_bump
            + [
                xt.TargetSet(line="lhcb1", at="ip8", x=offset_match, px=0),
                xt.TargetSet(line="lhcb2", at="ip8", x=offset_match, px=0),
            ]
        ),
        vary=xt.VaryList(correctors_ir8_single_beam_h),
        run=False,
        twiss_init=twinit_zero_orbit,
        **bump_range_ip8,
    )
    opt_o8h.solve()
    opt_o8h.generate_knob()

    ##############################
    # Match angular offset knobs #
    ##############################

    ang_offset_match = 30e-6

    # ---------- on_a2h ----------

    opt_a2h = collider.match_knob(
        knob_name="on_a2h",
        knob_value_end=(ang_offset_match * 1e6),
        targets=(
            targets_close_bump
            + [
                xt.TargetSet(line="lhcb1", at="ip2", x=0, px=ang_offset_match),
                xt.TargetSet(line="lhcb2", at="ip2", x=0, px=ang_offset_match),
            ]
        ),
        vary=xt.VaryList(correctors_ir2_single_beam_h),
        run=False,
        twiss_init=twinit_zero_orbit,
        **bump_range_ip2,
    )

    opt_a2h.solve()
    opt_a2h.generate_knob()

    # ---------- on_a2v ----------

    opt_a2v = collider.match_knob(
        knob_name="on_a2v",
        knob_value_end=(ang_offset_match * 1e6),
        targets=(
            targets_close_bump
            + [
                xt.TargetSet(line="lhcb1", at="ip2", y=0, py=ang_offset_match),
                xt.TargetSet(line="lhcb2", at="ip2", y=0, py=ang_offset_match),
            ]
        ),
        vary=xt.VaryList(correctors_ir2_single_beam_v),
        run=False,
        twiss_init=twinit_zero_orbit,
        **bump_range_ip2,
    )

    opt_a2v.solve()
    opt_a2v.generate_knob()

    # ---------- on_a8h ----------

    opt_a8h = collider.match_knob(
        knob_name="on_a8h",
        knob_value_end=(ang_offset_match * 1e6),
        targets=(
            targets_close_bump
            + [
                xt.TargetSet(line="lhcb1", at="ip8", x=0, px=ang_offset_match),
                xt.TargetSet(line="lhcb2", at="ip8", x=0, px=ang_offset_match),
            ]
        ),
        vary=xt.VaryList(correctors_ir8_single_beam_h),
        run=False,
        twiss_init=twinit_zero_orbit,
        **bump_range_ip8,
    )

    opt_a8h.solve()
    opt_a8h.generate_knob()

    # ---------- on_a8v ----------

    opt_a8v = collider.match_knob(
        knob_name="on_a8v",
        knob_value_end=(ang_offset_match * 1e6),
        targets=(
            targets_close_bump
            + [
                xt.TargetSet(line="lhcb1", at="ip8", y=0, py=ang_offset_match),
                xt.TargetSet(line="lhcb2", at="ip8", y=0, py=ang_offset_match),
            ]
        ),
        vary=xt.VaryList(correctors_ir8_single_beam_v),
        run=False,
        twiss_init=twinit_zero_orbit,
        **bump_range_ip8,
    )

    opt_a8v.solve()
    opt_a8v.generate_knob()

    ##############################
    # Match crossing angle knobs #
    ##############################

    angle_match_ip2 = 170e-6
    angle_match_ip8 = 170e-6

    # ---------- on_x2h ----------

    opt_x2h = collider.match_knob(
        knob_name="on_x2h",
        knob_value_end=(angle_match_ip2 * 1e6),
        targets=(
            targets_close_bump
            + [
                xt.TargetSet(line="lhcb1", at="ip2", x=0, px=angle_match_ip2),
                xt.TargetSet(line="lhcb2", at="ip2", x=0, px=-angle_match_ip2),
            ]
        ),
        vary=[
            xt.VaryList(correctors_ir2_single_beam_h),
            xt.VaryList(correctors_ir2_common_h, tag="mcbx"),
        ],
        run=False,
        twiss_init=twinit_zero_orbit,
        **bump_range_ip2,
    )
    # Set mcbx by hand
    testkqx2 = abs(collider.varval["kqx.l2"]) * 7000.0 / 0.3
    acbx_xing_ir2 = (
        1.0e-6 if testkqx2 > 210.0 else 11.0e-6
    )  # Value for 170 urad crossing
    for icorr in [1, 2, 3]:
        collider.vars[f"acbxh{icorr}.l2_from_on_x2h"] = acbx_xing_ir2
        collider.vars[f"acbxh{icorr}.r2_from_on_x2h"] = -acbx_xing_ir2
    # Match other correctors with fixed mcbx and generate knob
    opt_x2h.disable_vary(tag="mcbx")
    opt_x2h.solve()
    opt_x2h.generate_knob()

    # ---------- on_x2v ----------

    opt_x2v = collider.match_knob(
        knob_name="on_x2v",
        knob_value_end=(angle_match_ip2 * 1e6),
        targets=(
            targets_close_bump
            + [
                xt.TargetSet(line="lhcb1", at="ip2", y=0, py=angle_match_ip2),
                xt.TargetSet(line="lhcb2", at="ip2", y=0, py=-angle_match_ip2),
            ]
        ),
        vary=[
            xt.VaryList(correctors_ir2_single_beam_v),
            xt.VaryList(correctors_ir2_common_v, tag="mcbx"),
        ],
        run=False,
        twiss_init=twinit_zero_orbit,
        **bump_range_ip2,
    )
    # Set mcbx by hand
    testkqx2 = abs(collider.varval["kqx.l2"]) * 7000.0 / 0.3
    acbx_xing_ir2 = 1.0e-6 if testkqx2 > 210.0 else 11.0e-6
    for icorr in [1, 2, 3]:
        collider.vars[f"acbxv{icorr}.l2_from_on_x2v"] = acbx_xing_ir2
        collider.vars[f"acbxv{icorr}.r2_from_on_x2v"] = -acbx_xing_ir2
    # Match other correctors with fixed mcbx and generate knob
    opt_x2v.disable_vary(tag="mcbx")
    opt_x2v.solve()
    opt_x2v.generate_knob()

    # ---------- on_x8h ----------

    opt_x8h = collider.match_knob(
        knob_name="on_x8h",
        knob_value_end=(angle_match_ip8 * 1e6),
        targets=(
            targets_close_bump
            + [
                xt.TargetSet(line="lhcb1", at="ip8", x=0, px=angle_match_ip8),
                xt.TargetSet(line="lhcb2", at="ip8", x=0, px=-angle_match_ip8),
            ]
        ),
        vary=[
            xt.VaryList(correctors_ir8_single_beam_h),
            xt.VaryList(correctors_ir8_common_h, tag="mcbx"),
        ],
        run=False,
        twiss_init=twinit_zero_orbit,
        **bump_range_ip8,
    )

    # Set mcbx by hand (reduce value by 10, to test matching algorithm)
    testkqx8 = abs(collider.varval["kqx.l8"]) * 7000.0 / 0.3
    acbx_xing_ir8 = (
        1.0e-6 if testkqx8 > 210.0 else 11.0e-6
    )  # Value for 170 urad crossing

    # Set mcbx by hand
    for icorr in [1, 2, 3]:
        collider.vars[f"acbxh{icorr}.l8_from_on_x8h"] = (
            acbx_xing_ir8 * angle_match_ip8 / 170e-6
        )
        collider.vars[f"acbxh{icorr}.r8_from_on_x8h"] = (
            -acbx_xing_ir8 * angle_match_ip8 / 170e-6
        )

    #   (reduce value by 10, to test matching algorithm)
    #   collider.vars[f'acbxh{icorr}.l8_from_on_x8h'] = acbx_xing_ir8 * angle_match_ip8 / 170e-6 * 0.1
    #   collider.vars[f'acbxh{icorr}.r8_from_on_x8h'] = -acbx_xing_ir8 * angle_match_ip8 / 170e-6 * 0.1

    # First round of optimization without changing mcbx
    opt_x8h.disable_vary(tag="mcbx")
    opt_x8h.step(3)  # perform 3 steps without checking for convergence

    # Link all mcbx strengths to the first one
    collider.vars["acbxh2.l8_from_on_x8h"] = collider.vars["acbxh1.l8_from_on_x8h"]
    collider.vars["acbxh3.l8_from_on_x8h"] = collider.vars["acbxh1.l8_from_on_x8h"]
    collider.vars["acbxh2.r8_from_on_x8h"] = -collider.vars["acbxh1.l8_from_on_x8h"]
    collider.vars["acbxh3.r8_from_on_x8h"] = -collider.vars["acbxh1.l8_from_on_x8h"]
    collider.vars["acbxh1.r8_from_on_x8h"] = -collider.vars["acbxh1.l8_from_on_x8h"]

    # Enable first mcbx knob (which controls the others)
    assert opt_x8h.vary[8].name == "acbxh1.l8_from_on_x8h"
    opt_x8h.vary[8].active = True

    # Solve and generate knob
    opt_x8h.solve()
    opt_x8h.generate_knob()

    # ---------- on_x8v ----------

    opt_x8v = collider.match_knob(
        knob_name="on_x8v",
        knob_value_end=(angle_match_ip8 * 1e6),
        targets=(
            targets_close_bump
            + [
                xt.TargetSet(line="lhcb1", at="ip8", y=0, py=angle_match_ip8),
                xt.TargetSet(line="lhcb2", at="ip8", y=0, py=-angle_match_ip8),
            ]
        ),
        vary=[
            xt.VaryList(correctors_ir8_single_beam_v),
            xt.VaryList(correctors_ir8_common_v, tag="mcbx"),
        ],
        run=False,
        twiss_init=twinit_zero_orbit,
        **bump_range_ip8,
    )

    # Set mcbx by hand
    testkqx8 = abs(collider.varval["kqx.l8"]) * 7000.0 / 0.3
    acbx_xing_ir8 = (
        1.0e-6 if testkqx8 > 210.0 else 11.0e-6
    )  # Value for 170 urad crossing
    # Set MCBX by hand
    for icorr in [1, 2, 3]:
        collider.vars[f"acbxv{icorr}.l8_from_on_x8v"] = (
            acbx_xing_ir8 * angle_match_ip8 / 170e-6
        )
        collider.vars[f"acbxv{icorr}.r8_from_on_x8v"] = (
            -acbx_xing_ir8 * angle_match_ip8 / 170e-6
        )

    # First round of optimization without changing mcbx
    opt_x8v.disable_vary(tag="mcbx")
    opt_x8v.step(3)  # perform 3 steps without checking for convergence

    # Solve with all vary active and generate knob
    opt_x8v.enable_vary(tag="mcbx")
    opt_x8v.solve()
    opt_x8v.generate_knob()

    ##########################
    # Match separation knobs #
    ##########################

    sep_match = 2e-3

    # ---------- on_sep2h ----------

    opt_sep2h = collider.match_knob(
        knob_name="on_sep2h",
        knob_value_end=(sep_match * 1e3),
        targets=(
            targets_close_bump
            + [
                xt.TargetSet(line="lhcb1", at="ip2", x=sep_match, px=0),
                xt.TargetSet(line="lhcb2", at="ip2", x=-sep_match, px=0),
            ]
        ),
        vary=[
            xt.VaryList(correctors_ir2_single_beam_h),
            xt.VaryList(correctors_ir2_common_h, tag="mcbx"),
        ],
        run=False,
        twiss_init=twinit_zero_orbit,
        **bump_range_ip2,
    )

    # Set mcbx by hand
    testkqx2 = abs(collider.varval["kqx.l2"]) * 7000.0 / 0.3
    acbx_sep_ir2 = 18e-6 if testkqx2 > 210.0 else 16e-6

    for icorr in [1, 2, 3]:
        collider.vars[f"acbxh{icorr}.l2_from_on_sep2h"] = acbx_sep_ir2
        collider.vars[f"acbxh{icorr}.r2_from_on_sep2h"] = acbx_sep_ir2

    # Match other correctors with fixed mcbx and generate knob
    opt_sep2h.disable_vary(tag="mcbx")
    opt_sep2h.solve()
    opt_sep2h.generate_knob()

    # ---------- on_sep2v ----------

    opt_sep2v = collider.match_knob(
        knob_name="on_sep2v",
        knob_value_end=(sep_match * 1e3),
        targets=(
            targets_close_bump
            + [
                xt.TargetSet(line="lhcb1", at="ip2", y=sep_match, py=0),
                xt.TargetSet(line="lhcb2", at="ip2", y=-sep_match, py=0),
            ]
        ),
        vary=[
            xt.VaryList(correctors_ir2_single_beam_v),
            xt.VaryList(correctors_ir2_common_v, tag="mcbx"),
        ],
        run=False,
        twiss_init=twinit_zero_orbit,
        **bump_range_ip2,
    )

    # Set mcbx by hand
    testkqx2 = abs(collider.varval["kqx.l2"]) * 7000.0 / 0.3
    acbx_sep_ir2 = 18e-6 if testkqx2 > 210.0 else 16e-6

    for icorr in [1, 2, 3]:
        collider.vars[f"acbxv{icorr}.l2_from_on_sep2v"] = acbx_sep_ir2
        collider.vars[f"acbxv{icorr}.r2_from_on_sep2v"] = acbx_sep_ir2

    # Match other correctors with fixed mcbx and generate knob
    opt_sep2v.disable_vary(tag="mcbx")
    opt_sep2v.solve()
    opt_sep2v.generate_knob()

    # ---------- on_sep8h ----------

    opt_sep8h = collider.match_knob(
        knob_name="on_sep8h",
        knob_value_end=(sep_match * 1e3),
        targets=(
            targets_close_bump
            + [
                xt.TargetSet(line="lhcb1", at="ip8", x=sep_match, px=0),
                xt.TargetSet(line="lhcb2", at="ip8", x=-sep_match, px=0),
            ]
        ),
        vary=[
            xt.VaryList(correctors_ir8_single_beam_h),
            xt.VaryList(correctors_ir8_common_h, tag="mcbx"),
        ],
        run=False,
        twiss_init=twinit_zero_orbit,
        **bump_range_ip8,
    )

    # Set mcbx by hand
    testkqx8 = abs(collider.varval["kqx.l8"]) * 7000.0 / 0.3
    acbx_sep_ir8 = 18e-6 if testkqx8 > 210.0 else 16e-6

    for icorr in [1, 2, 3]:
        collider.vars[f"acbxh{icorr}.l8_from_on_sep8h"] = (
            acbx_sep_ir8 * sep_match / 2e-3
        )
        collider.vars[f"acbxh{icorr}.r8_from_on_sep8h"] = (
            acbx_sep_ir8 * sep_match / 2e-3
        )

    # Match other correctors with fixed mcbx and generate knob
    opt_sep8h.disable_vary(tag="mcbx")
    opt_sep8h.solve()
    opt_sep8h.generate_knob()

    # ---------- on_sep8v ----------

    opt_sep8v = collider.match_knob(
        knob_name="on_sep8v",
        knob_value_end=(sep_match * 1e3),
        targets=(
            targets_close_bump
            + [
                xt.TargetSet(line="lhcb1", at="ip8", y=sep_match, py=0),
                xt.TargetSet(line="lhcb2", at="ip8", y=-sep_match, py=0),
            ]
        ),
        vary=[
            xt.VaryList(correctors_ir8_single_beam_v),
            xt.VaryList(correctors_ir8_common_v, tag="mcbx"),
        ],
        run=False,
        twiss_init=twinit_zero_orbit,
        **bump_range_ip8,
    )

    # Set mcbx by hand
    testkqx8 = abs(collider.varval["kqx.l8"]) * 7000.0 / 0.3
    acbx_sep_ir8 = 18e-6 if testkqx8 > 210.0 else 16e-6

    for icorr in [1, 2, 3]:
        collider.vars[f"acbxv{icorr}.l8_from_on_sep8v"] = (
            acbx_sep_ir8 * sep_match / 2e-3
        )
        collider.vars[f"acbxv{icorr}.r8_from_on_sep8v"] = (
            acbx_sep_ir8 * sep_match / 2e-3
        )

    # Match other correctors with fixed mcbx and generate knob
    opt_sep8v.disable_vary(tag="mcbx")
    opt_sep8v.solve()
    opt_sep8v.generate_knob()

    v = collider.vars
    f = collider.functions
    v["phi_ir2"] = 90.0
    v["phi_ir8"] = 0.0
    for irn in [2, 8]:
        v[f"cphi_ir{irn}"] = f.cos(v[f"phi_ir{irn}"] * np.pi / 180.0)
        v[f"sphi_ir{irn}"] = f.sin(v[f"phi_ir{irn}"] * np.pi / 180.0)
        v[f"on_x{irn}h"] = v[f"on_x{irn}"] * v[f"cphi_ir{irn}"]
        v[f"on_x{irn}v"] = v[f"on_x{irn}"] * v[f"sphi_ir{irn}"]
        v[f"on_sep{irn}h"] = -v[f"on_sep{irn}"] * v[f"sphi_ir{irn}"]
        v[f"on_sep{irn}v"] = v[f"on_sep{irn}"] * v[f"cphi_ir{irn}"]
        v[f"on_o{irn}h"] = v[f"on_o{irn}"] * v[f"cphi_ir{irn}"]
        v[f"on_o{irn}v"] = v[f"on_o{irn}"] * v[f"sphi_ir{irn}"]
        v[f"on_a{irn}h"] = -v[f"on_a{irn}"] * v[f"sphi_ir{irn}"]
        v[f"on_a{irn}v"] = v[f"on_a{irn}"] * v[f"cphi_ir{irn}"]

    opt = {
        "on_o2h": opt_o2h,
        "on_o2v": opt_o2v,
        "on_o8h": opt_o8h,
        "on_o8v": opt_o8v,
        "on_a2h": opt_a2h,
        "on_a2v": opt_a2v,
        "on_a8h": opt_a8h,
        "on_a8v": opt_a8v,
        "on_x2h": opt_x2h,
        "on_x2v": opt_x2v,
        "on_x8h": opt_x8h,
        "on_x8v": opt_x8v,
        "on_sep2h": opt_sep2h,
        "on_sep2v": opt_sep2v,
        "on_sep8h": opt_sep8h,
        "on_sep8v": opt_sep8v,
    }

    return opt


def get_cellphases(collider):
    tw_arcs = lm.get_arc_periodic_solution(collider, arc_name=lm.ARC_NAMES)

    cell_phases = {}
    cell_phases.update(
        {"12": {}, "23": {}, "34": {}, "45": {}, "56": {}, "67": {}, "78": {}, "81": {}}
    )

    for arc_name in lm.ARC_NAMES:
        for bim in ["b1", "b2"]:
            for plane in ["x", "y"]:
                cell_phases[arc_name][f"mu{plane}cell{arc_name}{bim}"] = (
                    tw_arcs[f"lhc{bim}"][arc_name][
                        f"mu{plane}", f"e.cell.{arc_name}.{bim}"
                    ]
                    - tw_arcs[f"lhc{bim}"][arc_name][
                        f"mu{plane}", f"s.cell.{arc_name}.{bim}"
                    ]
                )

    return cell_phases


def get_phasearcs(collider):
    tw_arcs = lm.get_arc_periodic_solution(collider, arc_name=lm.ARC_NAMES)

    arc_phases = {}
    arc_phases.update(
        {"12": {}, "23": {}, "34": {}, "45": {}, "56": {}, "67": {}, "78": {}, "81": {}}
    )

    for arc_name in lm.ARC_NAMES:
        for bim in ["b1", "b2"]:
            for plane in ["x", "y"]:
                arc_phases[arc_name][f"mu{plane}{arc_name}{bim}"] = (
                    tw_arcs[f"lhc{bim}"][arc_name][
                        f"mu{plane}", f"s.ds.l{arc_name[1]}.{bim}"
                    ]
                    - tw_arcs[f"lhc{bim}"][arc_name][
                        f"mu{plane}", f"e.ds.r{arc_name[0]}.{bim}"
                    ]
                )

    return arc_phases


def get_ip_params_to_save_(collider, tw_arcs_):
    ip_params = {}
    ip_params.update(
        {"1": {}, "2": {}, "3": {}, "4": {}, "5": {}, "6": {}, "7": {}, "8": {}}
    )
    for arc in lm.ARC_NAMES:
        for bim in ["b1", "b2"]:
            tw_arcs = lm.get_arc_periodic_solution(collider, f"lhc{bim}", arc)
            # beta0 = tw_arcs[f"lhc{bim}"][arc].get_twiss_init(f"s.ds.l{arc[1]}.{bim}")
            beta0 = tw_arcs.get_twiss_init(f"s.ds.l{arc[1]}.{bim}")
            beta0.mux = 0
            beta0.muy = 0

            tw_pre = None
            if arc == "81":
                tw_pre = (
                    collider[f"lhc{bim}"]
                    .cycle("ip3")
                    .twiss(
                        ele_start=f"s.ds.l{arc[1]}.{bim}",
                        ele_stop=f"e.ds.r{arc[1]}.{bim}",
                        twiss_init=beta0,
                        method="4d",
                        reverse={"b1": False, "b2": True}[bim],
                    )
                )
            else:
                tw_pre = collider[f"lhc{bim}"].twiss(
                    ele_start=f"s.ds.l{arc[1]}.{bim}",
                    ele_stop=f"e.ds.r{arc[1]}.{bim}",
                    twiss_init=beta0,
                )
            betx_ip = tw_pre["betx", f"ip{arc[1]}"]
            bety_ip = tw_pre["bety", f"ip{arc[1]}"]
            alfx_ip = tw_pre["alfx", f"ip{arc[1]}"]
            alfy_ip = tw_pre["alfy", f"ip{arc[1]}"]
            dx_ip = tw_pre["dx", f"ip{arc[1]}"]
            dpx_ip = tw_pre["dpx", f"ip{arc[1]}"]

            mux = (
                tw_pre["mux", f"e.ds.r{arc[1]}.{bim}"]
                - tw_pre["mux", f"s.ds.l{arc[1]}.{bim}"]
            )
            muy = (
                tw_pre["muy", f"e.ds.r{arc[1]}.{bim}"]
                - tw_pre["muy", f"s.ds.l{arc[1]}.{bim}"]
            )

            muxip_l = (
                tw_pre["mux", f"ip{arc[1]}"] - tw_pre["mux", f"s.ds.l{arc[1]}.{bim}"]
            )
            muyip_l = (
                tw_pre["muy", f"ip{arc[1]}"] - tw_pre["muy", f"s.ds.l{arc[1]}.{bim}"]
            )
            muxip_r = (
                tw_pre["mux", f"e.ds.r{arc[1]}.{bim}"] - tw_pre["mux", f"ip{arc[1]}"]
            )
            muyip_r = (
                tw_pre["muy", f"e.ds.r{arc[1]}.{bim}"] - tw_pre["muy", f"ip{arc[1]}"]
            )

            ip_params[f"{arc[1]}"][f"betxip{arc[1]}{bim}"] = betx_ip
            ip_params[f"{arc[1]}"][f"betyip{arc[1]}{bim}"] = bety_ip
            ip_params[f"{arc[1]}"][f"alfxip{arc[1]}{bim}"] = alfx_ip
            ip_params[f"{arc[1]}"][f"alfyip{arc[1]}{bim}"] = alfy_ip
            ip_params[f"{arc[1]}"][f"dxip{arc[1]}{bim}"] = dx_ip
            ip_params[f"{arc[1]}"][f"dpxip{arc[1]}{bim}"] = dpx_ip
            ip_params[f"{arc[1]}"][f"muxip{arc[1]}{bim}"] = mux
            ip_params[f"{arc[1]}"][f"muyip{arc[1]}{bim}"] = muy
            ip_params[f"{arc[1]}"][f"muxip{arc[1]}{bim}_l"] = muxip_l
            ip_params[f"{arc[1]}"][f"muyip{arc[1]}{bim}_l"] = muyip_l
            ip_params[f"{arc[1]}"][f"muxip{arc[1]}{bim}_r"] = muxip_r
            ip_params[f"{arc[1]}"][f"muyip{arc[1]}{bim}_r"] = muyip_r

    return ip_params


def get_ip_params_to_save(collider):
    collider.lhcb1.cycle("s.ds.l3.b1", inplace=True)
    collider.lhcb2.cycle("s.ds.l3.b2", inplace=True)
    collider.lhcb1.twiss_default["only_markers"] = True
    collider.lhcb2.twiss_default["only_markers"] = True

    collider.lhcb1.twiss_default["method"] = "4d"
    collider.lhcb2.twiss_default["method"] = "4d"
    collider.lhcb2.twiss_default["reverse"] = True

    ip_params = {}
    ip_params.update(
        {"1": {}, "2": {}, "3": {}, "4": {}, "5": {}, "6": {}, "7": {}, "8": {}}
    )

    # IP1, IP5
    tw_arcs = lm.get_arc_periodic_solution(collider, arc_name=["81", "45"])
    for ip, arc in zip(["1", "5"], ["81", "45"]):
        for bim in ["b1", "b2"]:
            beta0 = tw_arcs[f"lhc{bim}"][arc].get_twiss_init(f"s.ds.l{ip}.{bim}")
            beta0.mux = 0
            beta0.muy = 0
            tw_ip = collider[f"lhc{bim}"].twiss(
                ele_start=f"s.ds.l{ip}.{bim}",
                ele_stop=f"e.ds.r{ip}.{bim}",
                twiss_init=beta0,
            )

            ip_params[ip][f"betx0_ip{ip}"] = tw_ip["betx", f"ip{ip}"]
            ip_params[ip][f"bety0_ip{ip}"] = tw_ip["bety", f"ip{ip}"]
            ip_params[ip][f"muxip{ip}{bim}"] = (
                tw_ip["mux", f"e.ds.r{ip}.{bim}"] - tw_ip["mux", f"s.ds.l{ip}.{bim}"]
            )

            ip_params[ip][f"muyip{ip}{bim}"] = (
                tw_ip["muy", f"e.ds.r{ip}.{bim}"] - tw_ip["muy", f"s.ds.l{ip}.{bim}"]
            )
            ip_params[ip][f"muxip{ip}{bim}_l"] = (
                tw_ip["mux", f"ip{ip}"] - tw_ip["mux", f"s.ds.l{ip}.{bim}"]
            )
            ip_params[ip][f"muyip{ip}{bim}_l"] = (
                tw_ip["muy", f"ip{ip}"] - tw_ip["muy", f"s.ds.l{ip}.{bim}"]
            )
            ip_params[ip][f"muxip{ip}{bim}_r"] = (
                tw_ip["mux", f"e.ds.r{ip}.{bim}"] - tw_ip["mux", f"ip{ip}"]
            )
            ip_params[ip][f"muyip{ip}{bim}_r"] = (
                tw_ip["muy", f"e.ds.r{ip}.{bim}"] - tw_ip["muy", f"ip{ip}"]
            )

    collider.lhcb1.cycle("lhcb1$start", inplace=True)
    collider.lhcb2.cycle("lhcb2$start", inplace=True)
    collider.lhcb1.twiss_default["only_markers"] = True
    collider.lhcb2.twiss_default["only_markers"] = True

    collider.lhcb1.twiss_default["method"] = "4d"
    collider.lhcb2.twiss_default["method"] = "4d"
    collider.lhcb2.twiss_default["reverse"] = True

    tw = collider.twiss()

    for ip in [1, 2, 3, 4, 5, 6, 7, 8]:
        for bim in ["b1", "b2"]:
            ip_params[f"{ip}"][f"dxip{ip}{bim}"] = tw[f"lhc{bim}"]["dx", f"ip{ip}"]
            ip_params[f"{ip}"][f"dpxip{ip}{bim}"] = tw[f"lhc{bim}"]["dpx", f"ip{ip}"]

            for xy in ["x", "y"]:
                ip_params[f"{ip}"][f"bet{xy}ip{ip}{bim}"] = tw[f"lhc{bim}"][
                    f"bet{xy}", f"ip{ip}"
                ]
                ip_params[f"{ip}"][f"alf{xy}ip{ip}{bim}"] = tw[f"lhc{bim}"][
                    f"alf{xy}", f"ip{ip}"
                ]

    mu_arcs = get_phasearcs(collider)

    for bim in ["b1", "b2"]:
        for xy in ["x", "y"]:
            ip_params["2"][f"mu{xy}ip2{bim}"] = (
                tw[f"lhc{bim}"][f"mu{xy}", f"e.ds.r2.{bim}"]
                - tw[f"lhc{bim}"][f"mu{xy}", "ip1"]
                - ip_params["1"][f"mu{xy}ip1{bim}_r"]
                - mu_arcs["12"][f"mu{xy}12{bim}"]
            )
            ip_params["6"][f"mu{xy}ip6{bim}"] = (
                tw[f"lhc{bim}"][f"mu{xy}", f"e.ds.r6.{bim}"]
                - tw[f"lhc{bim}"][f"mu{xy}", "ip5"]
                - ip_params["5"][f"mu{xy}ip5{bim}_r"]
                - mu_arcs["56"][f"mu{xy}56{bim}"]
            )
            ip_params["8"][f"mu{xy}ip8{bim}"] = (
                tw[f"lhc{bim}"][f"mu{xy}", "ip1.l1"]
                - tw[f"lhc{bim}"][f"mu{xy}", f"s.ds.l8.{bim}"]
                - ip_params["1"][f"mu{xy}ip1{bim}_l"]
                - mu_arcs["81"][f"mu{xy}81{bim}"]
            )
            ip_params["4"][f"mu{xy}ip4{bim}"] = (
                tw[f"lhc{bim}"][f"mu{xy}", "ip5"]
                - tw[f"lhc{bim}"][f"mu{xy}", f"s.ds.l4.{bim}"]
                - ip_params["5"][f"mu{xy}ip5{bim}_l"]
                - mu_arcs["45"][f"mu{xy}45{bim}"]
            )
            ip_params["3"][f"mu{xy}ip3{bim}"] = (
                tw[f"lhc{bim}"][f"mu{xy}", f"e.ds.r3.{bim}"]
                - tw[f"lhc{bim}"][f"mu{xy}", f"s.ds.l3.{bim}"]
            )
            ip_params["7"][f"mu{xy}ip7{bim}"] = (
                tw[f"lhc{bim}"][f"mu{xy}", f"e.ds.r7.{bim}"]
                - tw[f"lhc{bim}"][f"mu{xy}", f"s.ds.l7.{bim}"]
            )

            ip_params["2"][f"mu{xy}ip2{bim}_l"] = (
                tw[f"lhc{bim}"][f"mu{xy}", "ip2"]
                - tw[f"lhc{bim}"][f"mu{xy}", "ip1"]
                - ip_params["1"][f"mu{xy}ip1{bim}_r"]
                - mu_arcs["12"][f"mu{xy}12{bim}"]
            )
            ip_params["6"][f"mu{xy}ip6{bim}_l"] = (
                tw[f"lhc{bim}"][f"mu{xy}", "ip6"]
                - tw[f"lhc{bim}"][f"mu{xy}", "ip5"]
                - ip_params["5"][f"mu{xy}ip5{bim}_r"]
                - mu_arcs["56"][f"mu{xy}56{bim}"]
            )
            ip_params["8"][f"mu{xy}ip8{bim}_r"] = (
                tw[f"lhc{bim}"][f"mu{xy}", "ip1.l1"]
                - tw[f"lhc{bim}"][f"mu{xy}", f"ip8"]
                - ip_params["1"][f"mu{xy}ip1{bim}_l"]
                - mu_arcs["81"][f"mu{xy}81{bim}"]
            )
            ip_params["4"][f"mu{xy}ip4{bim}_r"] = (
                tw[f"lhc{bim}"][f"mu{xy}", "ip5"]
                - tw[f"lhc{bim}"][f"mu{xy}", "ip4"]
                - ip_params["5"][f"mu{xy}ip5{bim}_l"]
                - mu_arcs["45"][f"mu{xy}45{bim}"]
            )

            ip_params["2"][f"mu{xy}ip2{bim}_r"] = (
                ip_params["2"][f"mu{xy}ip2{bim}"] - ip_params["2"][f"mu{xy}ip2{bim}_l"]
            )
            ip_params["6"][f"mu{xy}ip6{bim}_r"] = (
                ip_params["6"][f"mu{xy}ip6{bim}"] - ip_params["6"][f"mu{xy}ip6{bim}_l"]
            )
            ip_params["8"][f"mu{xy}ip8{bim}_l"] = (
                ip_params["8"][f"mu{xy}ip8{bim}"] - ip_params["8"][f"mu{xy}ip8{bim}_r"]
            )
            ip_params["4"][f"mu{xy}ip4{bim}_l"] = (
                ip_params["4"][f"mu{xy}ip4{bim}"] - ip_params["4"][f"mu{xy}ip4{bim}_r"]
            )

            ip_params["3"][f"mu{xy}ip3{bim}_r"] = (
                tw[f"lhc{bim}"][f"mu{xy}", f"e.ds.r3.{bim}"]
                - tw[f"lhc{bim}"][f"mu{xy}", "ip3"]
            )
            ip_params["7"][f"mu{xy}ip7{bim}_r"] = (
                tw[f"lhc{bim}"][f"mu{xy}", f"e.ds.r7.{bim}"]
                - tw[f"lhc{bim}"][f"mu{xy}", "ip7"]
            )
            ip_params["3"][f"mu{xy}ip3{bim}_l"] = (
                ip_params["3"][f"mu{xy}ip3{bim}"] - ip_params["3"][f"mu{xy}ip3{bim}_r"]
            )
            ip_params["7"][f"mu{xy}ip7{bim}_l"] = (
                ip_params["7"][f"mu{xy}ip7{bim}"] - ip_params["7"][f"mu{xy}ip7{bim}_r"]
            )
    return ip_params


# Based on the gen_madx_optics_file_auto from xtrack
def save_optics_hl(collider, fname):
    tw = collider.twiss()

    dct_expr = {}
    dct_val = {}

    vtable = collider.vars.get_table()
    vsave = vtable.rows[
        vtable.mask[
            vtable.mask["acb.*"]
            | vtable.mask["kd.*"]
            | vtable.mask["kq.*"]
            | vtable.mask["ks.*"]
        ]
    ]
    for nn in vsave.name:
        vv = collider.vars[nn]
        lm.gen_madx_optics_file.extract_val_or_madexpr(vv, dct_expr, dct_val)

    out_lines = []

    for nn in sorted(dct_val.keys()):
        out_lines.append(nn + " = " + str(dct_val[nn]) + ";")

    out_lines.append("")

    for nn in sorted(dct_expr.keys()):
        out_lines.append(nn + " := " + dct_expr[nn] + ";")

    out_lines.append("")
    for bim in ["b1", "b2"]:
        out_lines.append(f"qx{bim} = {tw[f'lhc{bim}'].qx} ;")
        out_lines.append(f"qy{bim} = {tw[f'lhc{bim}'].qy} ;")

    out_lines.append("")
    for bim in ["b1", "b2"]:
        out_lines.append(f"qpx{bim} = {tw[f'lhc{bim}'].dqx} ;")
        out_lines.append(f"qpy{bim} = {tw[f'lhc{bim}'].dqy} ;")

    out_lines.append("")
    for ip in [1, 5, 2, 8]:
        out_lines.append(f"betx_ip{ip} = {tw.lhcb1['betx', f'ip{ip}']};")
        out_lines.append(f"bety_ip{ip} = {tw.lhcb1['bety', f'ip{ip}']};")

    mu_cells = get_cellphases(collider)
    mu_arcs = get_phasearcs(collider)
    ip_params = get_ip_params_to_save(collider)

    out_lines.append("")
    for arc_name in lm.ARC_NAMES:
        for bim in ["b1", "b2"]:
            for plane in ["x", "y"]:
                out_lines.append(
                    f"mu{plane}cell{arc_name}{bim} = {mu_cells[arc_name][f'mu{plane}cell{arc_name}{bim}']} ;"
                )

    out_lines.append("")
    for arc_name in lm.ARC_NAMES:
        for bim in ["b1", "b2"]:
            for plane in ["x", "y"]:
                out_lines.append(
                    f"mu{plane}{arc_name}{bim} = {mu_arcs[arc_name][f'mu{plane}{arc_name}{bim}']} ;"
                )

    out_lines.append("")
    for ip in [1, 2, 3, 4, 5, 6, 7, 8]:
        for name, val in ip_params[f"{ip}"].items():
            out_lines.append(f"{name} = {val} ;")

    print(f"Saving optics to {fname}")
    with open(fname, "w") as fid:
        fid.write("\n".join(out_lines))


def rematch_ir15(
    collider,
    betxip5,
    betyip5,
    tw_sq_a45_ip5_a56=None,
    match_on_triplet=0,
    match_inj_tunes=0,
    no_match_beta=False,
    ir5q4sym=0,
    ir5q5sym=0,
    ir5q6sym=0,
    restore=True,
    solve=False,
    ratio_at_CC=1.0,
    beta_peak_ratio=0.995,
    n_steps=25,
):
    """Match IP5 for the beta_x, beta_y, and copy strenghts to IP1

    Parameters
    ----------
    collider : xtrack.Multiline
        HL-LHC collider with b1 and b2
    betxip5 : float
        betax for ip1 and ip5
    betyip5 : float
        betay for ip1 and ip5
    tw_sq_a45_ip5_a56 : dict
        Dictionary for beam1, beam2 with twiss tables for arcs 45, 56
    match_on_triplet: int
        Flag to select how to match the triplet.
        0 : Don't touch the triplet
        1 : Q1, Q2, Q3 free
        2 : Q2 free
        3 :
        4 : Link Q1 with Q3
    match_inj_tunes: bool
    no_match_beta: bool
        If False, match beta at the ip
    ir5q4sym: int
        Flag to connect Q4
        0 : Connect Q4 left for both beams and Q4 right for both beams
        1 : Connect Q4 left and right
    ir5q5sym: int
        Flag to connect Q5
        0 : Connect Q5 left for both beams and Q5 right for both beams
        1 : Connect Q5 left and right
    ir5q6sym: int
        Flag to connect Q6
        0 : Connect Q6 left for both beams and Q6 right for both beams
        1 : Connect Q6 left and right
    restore: bool
        Flag to restore the variables if the match fails
    solve: bool
        Flag to solve or not

    """
    imb = 1.50

    if tw_sq_a45_ip5_a56 is None:
        tw_sq_a45_ip5_a56 = lm.get_arc_periodic_solution(
            collider, arc_name=["45", "56"]
        )

    targets = []

    if no_match_beta == False:
        targets.append(
            xt.TargetSet(
                line="lhcb1",
                at="ip5",
                betx=betxip5,
                bety=betyip5,
                alfx=0,
                alfy=0,
                dx=0,
                dpx=0,
            )
        )
        targets.append(
            xt.TargetSet(
                line="lhcb2",
                at="ip5",
                betx=betxip5,
                bety=betyip5,
                alfx=0,
                alfy=0,
                dx=0,
                dpx=0,
            )
        )
    else:
        targets.append(
            xt.TargetSet(line="lhcb1", at="ip5", alfx=0, alfy=0, dx=0, dpx=0)
        )
        targets.append(
            xt.TargetSet(line="lhcb2", at="ip5", alfx=0, alfy=0, dx=0, dpx=0)
        )

    targets.append(
        xt.TargetSet(
            line="lhcb1",
            at="e.ds.r5.b1",
            betx=tw_sq_a45_ip5_a56["lhcb1"]["56"]["betx", "e.ds.r5.b1"],
            bety=tw_sq_a45_ip5_a56["lhcb1"]["56"]["bety", "e.ds.r5.b1"],
            alfx=tw_sq_a45_ip5_a56["lhcb1"]["56"]["alfx", "e.ds.r5.b1"],
            alfy=tw_sq_a45_ip5_a56["lhcb1"]["56"]["alfy", "e.ds.r5.b1"],
            dx=tw_sq_a45_ip5_a56["lhcb1"]["56"]["dx", "e.ds.r5.b1"],
            dpx=tw_sq_a45_ip5_a56["lhcb1"]["56"]["dpx", "e.ds.r5.b1"],
        )
    )
    targets.append(
        xt.TargetSet(
            line="lhcb2",
            at="e.ds.r5.b2",
            betx=tw_sq_a45_ip5_a56["lhcb2"]["56"]["betx", "e.ds.r5.b2"],
            bety=tw_sq_a45_ip5_a56["lhcb2"]["56"]["bety", "e.ds.r5.b2"],
            alfx=tw_sq_a45_ip5_a56["lhcb2"]["56"]["alfx", "e.ds.r5.b2"],
            alfy=tw_sq_a45_ip5_a56["lhcb2"]["56"]["alfy", "e.ds.r5.b2"],
            dx=tw_sq_a45_ip5_a56["lhcb2"]["56"]["dx", "e.ds.r5.b2"],
            dpx=tw_sq_a45_ip5_a56["lhcb2"]["56"]["dpx", "e.ds.r5.b2"],
        )
    )

    muxIP5b1_l = collider["lhcb1"].varval["muxip5b1_l"]
    muxIP5b1_r = collider["lhcb1"].varval["muxip5b1_r"]
    muyIP5b1_l = collider["lhcb1"].varval["muyip5b1_l"]
    muyIP5b1_r = collider["lhcb1"].varval["muyip5b1_r"]
    muxIP5b1 = muxIP5b1_l + muxIP5b1_r
    muyIP5b1 = muyIP5b1_l + muyIP5b1_r

    muxIP5b2_l = collider["lhcb2"].varval["muxip5b2_l"]
    muxIP5b2_r = collider["lhcb2"].varval["muxip5b2_r"]
    muyIP5b2_l = collider["lhcb2"].varval["muyip5b2_l"]
    muyIP5b2_r = collider["lhcb2"].varval["muyip5b2_r"]
    muxIP5b2 = muxIP5b2_l + muxIP5b2_r
    muyIP5b2 = muyIP5b2_l + muyIP5b2_r

    if match_inj_tunes == 0:
        targets.append(
            xt.TargetSet(line="lhcb1", at="ip5", mux=muxIP5b1_l, muy=muyIP5b1_l)
        )
        targets.append(
            xt.TargetSet(line="lhcb1", at="e.ds.r5.b1", mux=muxIP5b1, muy=muyIP5b1)
        )
        targets.append(
            xt.TargetSet(line="lhcb2", at="ip5", mux=muxIP5b2_l, muy=muyIP5b2_l)
        )
        targets.append(
            xt.TargetSet(line="lhcb2", at="e.ds.r5.b2", mux=muxIP5b2, muy=muyIP5b2)
        )

    else:
        targets.append(
            xt.TargetSet(
                line="lhcb1",
                at="e.ds.r5.b1",
                mux=muxIP5b1,
                muy=muyIP5b1,
            )
        )
        targets.append(
            xt.TargetSet(
                line="lhcb2",
                at="e.ds.r5.b2",
                mux=muxIP5b2,
                muy=muyIP5b2,
            )
        )

    vary_list = []

    if ir5q4sym == 0:
        collider.vars["imq4l"] = (
            -collider.vars["kq4.l5b2"] / collider.vars["kq4.l5b1"] / 100
        )
        collider.vars["imq4r"] = (
            -collider.vars["kq4.r5b2"] / collider.vars["kq4.r5b1"] / 100
        )

        collider.vars["kq4.l5b2"] = -(
            collider.vars["kq4.l5b1"] * collider.vars["imq4l"] * 100
        )
        collider.vars["kq4.r5b2"] = -(
            collider.vars["kq4.r5b1"] * collider.vars["imq4r"] * 100
        )

        if (
            collider.varval["imq4l"] < 1 / imb / 100
            or collider.varval["imq4l"] > imb / 100
        ):
            collider.vars["imq4l"]._set_value(1 / imb / 100)
        if (
            collider.varval["imq4r"] < 1 / imb / 100
            or collider.varval["imq4r"] > imb / 100
        ):
            collider.vars["imq4r"]._set_value(1 / imb / 100)

        vary_list.append(xt.Vary("imq4l", limits=(1 / imb / 100, imb / 100), step=1e-6))
        vary_list.append(xt.Vary("imq4r", limits=(1 / imb / 100, imb / 100), step=1e-6))
        vary_list.append(xt.Vary("kq4.l5b1"))
        vary_list.append(xt.Vary("kq4.r5b1"))
    elif ir5q4sym == 1:
        connect_lr_qs(collider, nq=4, ir=5, bim=1)
        connect_lr_qs(collider, nq=4, ir=5, bim=2)
        vary_list.append(xt.Vary("kq4.l5b1"))
        vary_list.append(xt.Vary("kq4.l5b2"))

    if ir5q5sym == 0:
        collider.vars["imq5l"] = (
            -collider.vars["kq5.l5b2"] / collider.vars["kq5.l5b1"] / 100
        )
        collider.vars["imq5r"] = (
            -collider.vars["kq5.r5b2"] / collider.vars["kq5.r5b1"] / 100
        )

        collider.vars["kq5.l5b2"] = -(
            collider.vars["kq5.l5b1"] * collider.vars["imq5l"] * 100
        )
        collider.vars["kq5.r5b2"] = -(
            collider.vars["kq5.r5b1"] * collider.vars["imq5r"] * 100
        )

        if (
            collider.varval["imq5l"] < 1 / imb / 100
            or collider.varval["imq5l"] > imb / 100
        ):
            collider.vars["imq5l"]._set_value(1 / imb / 100)
        if (
            collider.varval["imq5r"] < 1 / imb / 100
            or collider.varval["imq5r"] > imb / 100
        ):
            collider.vars["imq5r"]._set_value(1 / imb / 100)

        vary_list.append(xt.Vary("imq5l", limits=(1 / imb / 100, imb / 100), step=1e-6))
        vary_list.append(xt.Vary("imq5r", limits=(1 / imb / 100, imb / 100), step=1e-6))
        vary_list.append(xt.Vary("kq5.l5b1"))
        vary_list.append(xt.Vary("kq5.r5b1"))
    elif ir5q5sym == 1:
        connect_lr_qs(collider, nq=5, ir=5, bim=1)
        connect_lr_qs(collider, nq=5, ir=5, bim=2)
        vary_list.append(xt.Vary("kq5.l5b1"))
        vary_list.append(xt.Vary("kq5.l5b2"))

    if ir5q6sym == 0:
        collider.vars["imq6l"] = (
            -collider.vars["kq6.l5b2"] / collider.vars["kq6.l5b1"] / 100
        )
        collider.vars["imq6r"] = (
            -collider.vars["kq6.r5b2"] / collider.vars["kq6.r5b1"] / 100
        )

        collider.vars["kq6.l5b2"] = -(
            collider.vars["kq6.l5b1"] * collider.vars["imq6l"] * 100
        )
        collider.vars["kq6.r5b2"] = -(
            collider.vars["kq6.r5b1"] * collider.vars["imq6r"] * 100
        )

        if (
            collider.varval["imq6l"] < 1 / imb / 100
            or collider.varval["imq6l"] > imb / 100
        ):
            collider.vars["imq6l"]._set_value(1 / imb / 100)
        if (
            collider.varval["imq6r"] < 1 / imb / 100
            or collider.varval["imq6r"] > imb / 100
        ):
            collider.vars["imq6r"]._set_value(1 / imb / 100)

        vary_list.append(xt.Vary("imq6l", limits=(1 / imb / 100, imb / 100), step=1e-6))
        vary_list.append(xt.Vary("imq6r", limits=(1 / imb / 100, imb / 100), step=1e-6))
        vary_list.append(xt.Vary("kq6.l5b1"))
        vary_list.append(xt.Vary("kq6.r5b1"))
    elif ir5q6sym == 1:
        connect_lr_qs(collider, nq=6, ir=5, bim=1)
        connect_lr_qs(collider, nq=6, ir=5, bim=2)
        vary_list.append(xt.Vary("kq6.l5b1"))
        vary_list.append(xt.Vary("kq6.l5b2"))

    quads_ir5_b1 = (
        "kq4.l5b1 kq4.r5b1 kq5.l5b1 kq5.r5b1 kq6.l5b1 kq6.r5b1 "
        "kq7.l5b1 kq7.r5b1 kq8.l5b1 kq8.r5b1 kq9.l5b1 kq9.r5b1 "
        "kq10.l5b1 kq10.r5b1 kqtl11.l5b1 kqtl11.r5b1 "
        "kqt12.l5b1 kqt12.r5b1 kqt13.l5b1 kqt13.r5b1"
    )

    quads_ir5_b2 = (
        "kq4.l5b2 kq4.r5b2 kq5.l5b2 kq5.r5b2 kq6.l5b2 kq6.r5b2 "
        "kq7.l5b2 kq7.r5b2 kq8.l5b2 kq8.r5b2 kq9.l5b2 kq9.r5b2 "
        "kq10.l5b2 kq10.r5b2 kqtl11.l5b2 kqtl11.r5b2 kqt12.l5b2 "
        "kqt12.r5b2 kqt13.l5b2 kqt13.r5b2"
    )
    # Beam 1
    for quad in quads_ir5_b1.split()[6:]:
        vary_list.append(xt.Vary(quad))

    # Beam 2
    for quad in quads_ir5_b2.split()[6:]:
        vary_list.append(xt.Vary(quad))

    if match_on_triplet == 0:  # q1 q2 q3 untouched
        pass
    elif match_on_triplet == 1:  # Q1 Q2 Q3 free
        connect_q2s(collider)
        connect_lr_qx(collider, 1)
        connect_lr_qx(collider, 3)
        vary_list.append(xt.Vary("kqx1.l5"))
        vary_list.append(xt.Vary("kqx2a.l5"))
        vary_list.append(xt.Vary("kqx3.l5"))
    elif match_on_triplet == 2:
        # connect kqx2a and kqx2b left and right
        connect_q2s(collider)
        vary_list.append(xt.Vary("kqx2a.l5"))
    elif match_on_triplet == 4:
        # link q3 and q1

        connect_lr_qx(collider, 1)
        collider.vars["kqx3.l5"] = +collider.vars["kqx1.l5"]
        collider.vars["kqx3.r5"] = -collider.vars["kqx1.l5"]

        connect_q2s(collider)
        vary_list.append(xt.Vary("kqx1.l5"))
        vary_list.append(xt.Vary("kqx2a.l5"))
    elif match_on_triplet == 5:
        connect_q2s(collider)
        connect_lr_qx(collider, 1)
        connect_lr_qx(collider, 3)
        vary_list.append(xt.Vary("kqx1.l5"))
        vary_list.append(xt.Vary("kqx2a.l5"))
        targets.append(
            xt.Target(
                lambda tt: tt.lhcb1["bety", "mqxfb.a2r5_exit"]
                / tt.lhcb1["betx", "mqxfa.b3r5_exit"],
                value=beta_peak_ratio,
                tol=1e-2,
            )
        )
    elif match_on_triplet == 6:
        # connect_q2s(collider, ir=5)
        # connect_lr_qx(collider, nqx=1, ir=5)
        # connect_lr_qx(collider, nqx=3, ir=5)
        # vary_list.append(xt.Vary(tag="triplet", name="kqx1.l5"))
        # vary_list.append(xt.Vary(tag="triplet", name="kqx2a.l5"))
        # vary_list.append(xt.Vary(tag="triplet", name="kqx3.l5"))
        targets.append(
            xt.Target(
                lambda tt: tt.lhcb1["bety", "acfca.4ar5.b1_exit"]
                / tt.lhcb1["betx", "acfca.4ar5.b1_exit"],
                value=ratio_at_CC,  # 0.98
                tol=1e-3,
            )
        )
        targets.append(
            xt.Target(
                lambda tt: tt.lhcb1["bety", "acfca.4ar5.b1_exit"],
                value=1150.0,  # 570.0,
                tol=1e-2,
            )
        )
        targets.append(
            xt.Target(
                lambda tt: tt.lhcb1["bety", "mqxfb.a2r5_exit"]
                / tt.lhcb1["betx", "mqxfa.b3r5_exit"],
                value=beta_peak_ratio,
                tol=1e-2,
            )
        )
        # # reduce bet bump at Q5-Q6
        # targets.append(
        #     xt.Target(
        #         lambda tt: tt.lhcb1["betx", "mqml.5l5.b1_entry"]
        #         - tt.lhcb1["betx", "mqml.6l5.b1_entry"],
        #         value=xt.GreaterThan(0),
        #         tol=1e0,
        #     )
        # )

    elif match_on_triplet == 7:
        targets.append(
            xt.Target(
                lambda tt: tt.lhcb1["bety", "mqxfb.a2r5_exit"]
                / tt.lhcb1["betx", "mqxfa.b3r5_exit"],
                value=beta_peak_ratio,
                tol=1e-2,
            )
        )

    twiss_init_b1 = tw_sq_a45_ip5_a56["lhcb1"]["45"].get_twiss_init("s.ds.l5.b1")
    twiss_init_b1.mux = 0
    twiss_init_b1.muy = 0

    twiss_init_b2 = tw_sq_a45_ip5_a56["lhcb2"]["45"].get_twiss_init("s.ds.l5.b2")
    twiss_init_b2.mux = 0
    twiss_init_b2.muy = 0

    opt = collider.match(
        solve=False,
        only_markers=False,
        method="4d",
        solver_options={"n_bisections": 3, "min_step": 1e-7, "n_steps_max": n_steps},
        default_tol=default_tol,
        ele_start=["s.ds.l5.b1", "s.ds.l5.b2"],
        ele_stop=["e.ds.r5.b1", "e.ds.r5.b2"],
        restore_if_fail=restore,
        assert_within_tol=False,
        twiss_init=[twiss_init_b1, twiss_init_b2],
        targets=targets,
        vary=vary_list,
        verbose=False,
    )

    if solve:
        opt.solve()

    collider.vars["kqx1.r5"] = -collider.varval["kqx1.l5"]
    collider.vars["kqx2.l5"] = collider.varval["kqx2a.l5"]
    collider.vars["kqx2a.l5"] = collider.varval["kqx2.l5"]
    collider.vars["kqx2b.l5"] = collider.varval["kqx2.l5"]
    collider.vars["kqx2a.r5"] = -collider.varval["kqx2.l5"]
    collider.vars["kqx2b.r5"] = -collider.varval["kqx2.l5"]
    collider.vars["kqx3.r5"] = -collider.varval["kqx3.l5"]

    collider.vars["kqx1.l1"] = collider.varval["kqx1.l5"]
    collider.vars["kqx2a.l1"] = collider.varval["kqx2a.l5"]
    collider.vars["kqx2b.l1"] = collider.varval["kqx2b.l5"]
    collider.vars["kqx3.l1"] = collider.varval["kqx3.l5"]
    collider.vars["kqx1.r1"] = collider.varval["kqx1.r5"]
    collider.vars["kqx2a.r1"] = collider.varval["kqx2a.r5"]
    collider.vars["kqx2b.r1"] = collider.varval["kqx2b.r5"]
    collider.vars["kqx3.r1"] = collider.varval["kqx3.r5"]

    # remove the imqXlr knobs if any
    quads_imqlr = "kq4.l5b2 kq4.r5b2 kq5.l5b2 kq5.r5b2 kq6.l5b2 kq6.r5b2"
    for quad in quads_imqlr.split():
        collider.vars[quad] = collider.varval[quad]

    for quad in quads_ir5_b1.split():
        collider.vars[quad.replace("5b", "1b")] = collider.varval[quad]

    for quad in quads_ir5_b2.split():
        collider.vars[quad.replace("5b", "1b")] = collider.varval[quad]

    return opt


### CRABS ###


def match_cc(collider, line_name, ip, hv, z_crab=1e-3, solve=True):
    collider.varval["z_crab"] = z_crab

    irn = ip[2]
    bim = line_name[-2:]
    signb = 1
    if line_name == "lhcb2":
        signb = -1

    targets = None
    if hv == "h":
        targets = [xt.TargetSet(at=f"{ip}", px=0, x=1e-6 * z_crab * signb, tol=1e-20)]
    elif hv == "v":
        targets = [xt.TargetSet(at=f"{ip}", py=0, y=1e-6 * z_crab * signb, tol=1e-20)]

    vary = xt.VaryList([f"a{hv}crab_l{irn}{bim}", f"a{hv}crab_r{irn}{bim}"], step=1e-10)

    collider.varval[f"on_crab{irn}"] = 1

    opt = collider[line_name].match(
        solve=False,
        only_markers=False,
        restore_if_fail=False,
        assert_within_tol=False,
        verbose=False,
        solver_options={"n_steps_max": 15},
        targets=targets,
        vary=vary,
    )

    if solve:
        opt.solve()

    collider.varval[f"on_crab{irn}"] = 0

    return opt


def match_all_cc(collider, z_crab=1e-3, nrj=7000, reset_values=True, solve=True):
    collider.varval["nrj"] = nrj
    collider.varval["z_crab"] = z_crab
    phi_ir1 = collider.varval["phi_ir1"]
    phi_ir5 = collider.varval["phi_ir5"]
    # reset everything
    if reset_values:
        for ip in [1, 5]:
            for bim in ["b1", "b2"]:
                collider.varval[f"ahcrab_l{ip}{bim}"] = 0.01 / nrj
                collider.varval[f"ahcrab_r{ip}{bim}"] = 0.01 / nrj
                collider.varval[f"avcrab_l{ip}{bim}"] = 0.01 / nrj
                collider.varval[f"avcrab_r{ip}{bim}"] = 0.01 / nrj

    collider.varval["phi_ir1"] = 0
    collider.varval["phi_ir5"] = 0

    opt_cc_h1_b1 = match_cc(collider, "lhcb1", "ip1", "h", solve=True)
    opt_cc_h1_b2 = match_cc(collider, "lhcb2", "ip1", "h", solve=True)
    opt_cc_h5_b1 = match_cc(collider, "lhcb1", "ip5", "h", solve=True)
    opt_cc_h5_b2 = match_cc(collider, "lhcb2", "ip5", "h", solve=True)

    collider.varval["phi_ir1"] = 90
    collider.varval["phi_ir5"] = 90
    opt_cc_v1_b1 = match_cc(collider, "lhcb1", "ip1", "v", solve=True)
    opt_cc_v1_b2 = match_cc(collider, "lhcb2", "ip1", "v", solve=True)
    opt_cc_v5_b1 = match_cc(collider, "lhcb1", "ip5", "v", solve=True)
    opt_cc_v5_b2 = match_cc(collider, "lhcb2", "ip5", "v", solve=True)

    opts = {
        "cc_ip1_b1_h": opt_cc_h1_b1,
        "cc_ip1_b2_h": opt_cc_h1_b2,
        "cc_ip5_b1_h": opt_cc_h5_b1,
        "cc_ip5_b2_h": opt_cc_h5_b2,
        "cc_ip1_b1_v": opt_cc_v1_b1,
        "cc_ip1_b2_v": opt_cc_v1_b2,
        "cc_ip5_b1_v": opt_cc_v5_b1,
        "cc_ip5_b2_v": opt_cc_v5_b2,
    }

    # reset variables
    collider.varval["phi_ir1"] = phi_ir1
    collider.varval["phi_ir5"] = phi_ir5

    v_crabh_l1b1 = collider.varval["ahcrab_l1b1"] * nrj * 1e9 * 1e-6
    v_crabh_r1b1 = collider.varval["ahcrab_r1b1"] * nrj * 1e9 * 1e-6
    v_crabh_l1b2 = collider.varval["ahcrab_l1b2"] * nrj * 1e9 * 1e-6
    v_crabh_r1b2 = collider.varval["ahcrab_r1b2"] * nrj * 1e9 * 1e-6
    v_crabh_l5b1 = collider.varval["ahcrab_l5b1"] * nrj * 1e9 * 1e-6
    v_crabh_r5b1 = collider.varval["ahcrab_r5b1"] * nrj * 1e9 * 1e-6
    v_crabh_l5b2 = collider.varval["ahcrab_l5b2"] * nrj * 1e9 * 1e-6
    v_crabh_r5b2 = collider.varval["ahcrab_r5b2"] * nrj * 1e9 * 1e-6

    v_crabv_l1b1 = collider.varval["avcrab_l1b1"] * nrj * 1e9 * 1e-6
    v_crabv_r1b1 = collider.varval["avcrab_r1b1"] * nrj * 1e9 * 1e-6
    v_crabv_l1b2 = collider.varval["avcrab_l1b2"] * nrj * 1e9 * 1e-6
    v_crabv_r1b2 = collider.varval["avcrab_r1b2"] * nrj * 1e9 * 1e-6
    v_crabv_l5b1 = collider.varval["avcrab_l5b1"] * nrj * 1e9 * 1e-6
    v_crabv_r5b1 = collider.varval["avcrab_r5b1"] * nrj * 1e9 * 1e-6
    v_crabv_l5b2 = collider.varval["avcrab_l5b2"] * nrj * 1e9 * 1e-6
    v_crabv_r5b2 = collider.varval["avcrab_r5b2"] * nrj * 1e9 * 1e-6

    v_crab_h = [
        v_crabh_l1b1,
        v_crabh_r1b1,
        v_crabh_l1b2,
        v_crabh_r1b2,
        v_crabh_l5b1,
        v_crabh_r5b1,
        v_crabh_l5b2,
        v_crabh_r5b2,
    ]
    v_crab_v = [
        v_crabv_l1b1,
        v_crabv_r1b1,
        v_crabv_l1b2,
        v_crabv_r1b2,
        v_crabv_l5b1,
        v_crabv_r5b1,
        v_crabv_l5b2,
        v_crabv_r5b2,
    ]

    vcrabh_max = np.max(np.abs(v_crab_h))

    vcrabv_max = np.max(np.abs(v_crab_v))

    for vh, vv in zip(v_crab_h, v_crab_v):
        print(3.4 * 4 / vh, 3.4 * 4 / vv)

    crabh_angle_max = 3.4 * 4 / vcrabh_max
    crabv_angle_max = 3.4 * 4 / vcrabv_max

    print(f"{crabh_angle_max=}, {crabv_angle_max=}")
    return (
        opts,
        crabh_angle_max,
        crabv_angle_max,
    )
