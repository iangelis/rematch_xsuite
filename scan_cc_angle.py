# %%
import os
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
from pprint import pprint
import xtrack as xt
import xtrack._temp.lhc_match as lm
from misc import *

# from rematch_from_xtrack import *


# %%
try:
    get_ipython().run_line_magic("matplotlib", "inline")
    # get_ipython().run_line_magic("matplotlib", "qt6")
except:
    pass


# %%
def get_beta_ratio_at_cc(collider):
    tw = collider.twiss()
    ratiob1 = (
        tw.lhcb1["bety", "acfca.4ar5.b1_exit"] / tw.lhcb1["betx", "acfca.4ar5.b1_exit"]
    )
    ratiob2 = (
        tw.lhcb2["betx", "acfca.4al5.b2_exit"] / tw.lhcb2["bety", "acfca.4al5.b2_exit"]
    )
    print(f"acfca.4ar5.b1= {tw.lhcb1[['betx','bety'],'acfca.4ar5.b1_exit']}")

    return ratiob1, ratiob2


def get_beta_peak_ratio(collider):
    tw = collider.twiss()
    ratio = tw.lhcb1["bety", "mqxfb.a2r5_exit"] / tw.lhcb1["betx", "mqxfa.b3r5_exit"]
    return ratio


# %%
prefix = Path("../").absolute()

optics_file = prefix / "summer_studies/collapse/opt_collapse_1100_1500.madx"
optics_file = prefix / "madx_vs_xsuite_rematch/madx/opt_round_1100_1500_again3.madx"

# optics_file = "cc_ratio_study/opt_cc1.9.madx"
# optics_file = "cc_ratio_study/opt_cc1.85.madx"
# optics_file = (
#     prefix / "acc-models-lhc/strengths/round/start_collapse/opt_collapse_980_1500.madx"
# )

collider_name = "collider_hl16.json"

collider = xt.Multiline.from_json(collider_name)
collider.build_trackers()
collider.vars.load_madx_optics_file(optics_file)

collider.lhcb1.twiss_default["only_markers"] = True
collider.lhcb2.twiss_default["only_markers"] = True

collider.lhcb1.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["reverse"] = True
# %%
staged_match = True
first_pass = False
lm.set_var_limits_and_steps(collider)
collider.vars.vary_default.update(quads_ir15_defaults)

# %%
betx0 = 1.1
bety0 = 1.1

betx = 1.1
bety = 1.1

# %%
tw45_56_xt = lm.get_arc_periodic_solution(collider, arc_name=["45", "56"])
tw81_12_xt = lm.get_arc_periodic_solution(collider, arc_name=["81", "12"])

# %%
beta0 = tw45_56_xt["lhcb1"]["45"].get_twiss_init("s.ds.l5.b1")
beta0.mux = 0
beta0.muy = 0
tw_ip5_pre_0 = collider.lhcb1.twiss(
    ele_start="s.ds.l5.b1", ele_stop="e.ds.r5.b1", twiss_init=beta0
)
beta0 = tw81_12_xt["lhcb1"]["81"].get_twiss_init("s.ds.l1.b1")
beta0.mux = 0
beta0.muy = 0
tw_ip1_pre_0 = collider.lhcb1.cycle("ip3").twiss(
    ele_start="s.ds.l1.b1", ele_stop="e.ds.r1.b1", twiss_init=beta0, method="4d"
)

# %%
# collider.lhcb1.cycle("ip3", inplace=True)
# collider.lhcb2.cycle("ip3", inplace=True)
collider.lhcb1.twiss_default["only_markers"] = True
collider.lhcb2.twiss_default["only_markers"] = True

collider.lhcb1.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["reverse"] = True


# %%
tw0 = collider.twiss()
# %%

get_ipython().run_line_magic("load_ext", "autoreload")
get_ipython().run_line_magic("autoreload", "2")
from misc import rematch_ir15, match_all_cc


# %%
collider.vars.vary_default.update(
    {
        "kqx1.l5": {"step": 1e-6, "limits": (-qtlim1, 0)},
        "kqx2a.l5": {"step": 1e-6, "limits": (-qtlim1, 0)},
        "kqx3.l5": {"step": 1e-6, "limits": (-qtlim1, 0)},
    }
)

print(tw0.lhcb1["bety", "acfca.4ar5.b1_exit"] / tw0.lhcb1["betx", "acfca.4ar5.b1_exit"])
print(tw0.lhcb1[["betx", "bety"], "acfca.4ar5.b1_exit"])
# %%
print(get_beta_peak_ratio(collider))
print(get_beta_ratio_at_cc(collider))

# %%
ratio_at_CC = 0.97
# %%
optimizers = {}
# %%
optimizers["ir15"] = rematch_ir15(
    collider,
    betx0,
    bety0,
    tw_sq_a45_ip5_a56=tw45_56_xt,
    restore=False,
    solve=True,
    ir5q4sym=0,
    match_on_triplet=6,
    ratio_at_CC=ratio_at_CC,
    beta_peak_ratio=0.98,
    # n_steps=15,
)
# %%
# print(optimizers["ir15"].vary_status())
# print(optimizers["ir15"].target_status())
print(optimizers["ir15"].log())

# %%
tw = collider.twiss()
print(tw["lhcb1"][["betx", "bety"], "ip.*"])
print(tw["lhcb2"][["betx", "bety"], "ip.*"])
# %%
r1, r2 = get_beta_ratio_at_cc(collider)
print(r1, r2)
r1 = get_beta_peak_ratio(collider)
print(r1)
# %%

nrj = 7000
z_crab = 1e-3
opts, crabh_angle_max, crabv_angle_max = match_all_cc(collider, reset_values=False)
# %%
print(f"{crabh_angle_max=}, {crabv_angle_max=}")

# %%
# save_optics_hl(collider, f"cc_ratio_study/opt_cc{ratio_at_CC}.madx")
# save_optics_hl(collider, f"cc_ratio_study/opt_1100_cc0.97_peak0.98_part4.madx")
# %%
tw45_xt = lm.get_arc_periodic_solution(collider, arc_name=["45"])
beta0 = tw45_xt["lhcb1"]["45"].get_twiss_init("s.ds.l5.b1")
beta0.mux = 0
beta0.muy = 0
tw_ip5_pre_b1 = collider.lhcb1.twiss(
    ele_start="s.ds.l5.b1", ele_stop="e.ds.r5.b1", twiss_init=beta0
)

print(tw_ip5_pre_b1[["betx", "bety"], "ip5"])

# %%
fig, axs = plt.subplots(figsize=(21, 11))
axs.set_title("IP5")
axs.plot(
    tw_ip5_pre_b1.s,
    tw_ip5_pre_b1.betx,
    color="black",
    label=rf"$\beta_x^0$ {tw_ip5_pre_b1['betx', 'ip5']:2.3f}",
)
axs.plot(
    tw_ip5_pre_b1.s,
    tw_ip5_pre_b1.bety,
    color="red",
    label=rf"$\beta_y^0$ {tw_ip5_pre_b1['bety', 'ip5']:2.3f}",
)
axs.set_xlabel("s [m]")
axs.set_ylabel(r"$\beta_{x,y} [m]$")
axs.legend()
plt.show()

# %%

results = {}
# %%

results[ratio_at_CC] = [crabh_angle_max, crabv_angle_max]

# %%
print(results)
# %%
with open("cc_test.csv", "wa") as fp:
    fp.write("CC Ratio, crabh_angle_max, crabv_angle_max\n")
    for cc_r, cc_amax in results.items():
        fp.write(f"{cc_r}, {cc_amax[0]},{cc_amax[1]}\n")

# %%
