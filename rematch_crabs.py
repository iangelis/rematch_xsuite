# %%
from pathlib import Path
import xtrack as xt
from misc import *

# %%
prefix = Path("../").absolute()
# optics_file = prefix / 'acc-models-lhc/strengths/newir3ir7/opt_collapse_980_1500_newir37.madx'
# optics_file = prefix / 'acc-models-lhc/strengths/newir3ir7/opt_ramp_500_1500_newir37.madx'
# optics_file = prefix / "summer_studies/collapse/opt_collapse_1100_1500.madx"

collider_name = "collider_hl16.json"

collider = xt.Multiline.from_json(collider_name)
collider.build_trackers()
collider.vars.load_madx_optics_file(optics_file)

collider.lhcb1.twiss_default["only_markers"] = True
collider.lhcb2.twiss_default["only_markers"] = True

collider.lhcb1.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["reverse"] = True
# %%
nrj = 7000
z_crab = 1e-3
# %%
opts, crabh_angle_max, crabv_angle_max = match_all_cc(collider)
# %%

print(f"{crabh_angle_max=}, {crabv_angle_max=}")
# %%
collider.lhcb1.cycle("ip3", inplace=True)
collider.lhcb2.cycle("ip3", inplace=True)
# %%
collider.varval["z_crab"] = 1e-3
collider.varval["on_crab1"] = 1
collider.varval["on_crab5"] = 1
collider.varval["on_x1"] = 250
collider.varval["on_x5"] = 250
collider.varval["cd2q4"] = 1
# %%
tw = collider.twiss()
# %%
plot_orbit(tw, "lhcb1")

# %%
collider.varval["z_crab"] = 0
collider.varval["on_crab1"] = 0
collider.varval["on_crab5"] = 0
collider.varval["on_x1"] = 0
collider.varval["on_x5"] = 0

collider.lhcb1.cycle("lhcb1$start", inplace=True)
collider.lhcb2.cycle("lhcb2$start", inplace=True)

# %%
