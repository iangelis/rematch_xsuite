# %%
import os
import numpy as np

import matplotlib.pyplot as plt

import xtrack as xt


import xtrack._temp.lhc_match as lm

from misc import *

# try:
#     get_ipython().run_line_magic("matplotlib", "inline")
# except:
#     pass


# %%
default_tol = {
    None: 1e-8,
    "betx": 1e-6,
    "bety": 1e-6,
}  # to have no rematching w.r.t. madx

# %%
hpre = os.getenv("HOME")
prefix = f"{hpre}/Projects/hllhc_optics/"

# optics_name = "../madx_vs_xsuite_rematch/madx/opt_round_1100_1500_again3.madx"
optics_name = "../madx_vs_xsuite_rematch/madx/opt_round_1100_cc_angle_optim_again.madx"


# %%
collider = None

collider_name = "collider_hl16.json"
if os.path.exists(collider_name):
    collider = xt.Multiline.from_json(collider_name)
else:
    collider = build_collider(optics_name, cycling=True)
    collider.to_json(collider_name)

# %%
collider.vars.load_madx_optics_file(optics_name)
# %%
tw0 = collider.twiss()
print(tw0["lhcb1"][["x", "y", "betx", "bety", "px", "py"], "ip.*"])
print(tw0["lhcb2"][["x", "y", "betx", "bety", "px", "py"], "ip.*"])

# %%
# %%
collider.lhcb1.cycle("ip3", inplace=True)
collider.lhcb2.cycle("ip3", inplace=True)
collider.lhcb1.twiss_default["only_markers"] = True
collider.lhcb2.twiss_default["only_markers"] = True

collider.lhcb1.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["reverse"] = True

# %%
twinit_zero_orbit = [xt.TwissInit(), xt.TwissInit()]

targets_close_bump = [
    xt.TargetSet(line="lhcb1", at=xt.END, x=0, px=0),
    xt.TargetSet(line="lhcb2", at=xt.END, x=0, px=0),
]

bump_range_ip1 = {
    "ele_start": ["s.ds.l1.b1", "s.ds.l1.b2"],
    "ele_stop": ["e.ds.r1.b1", "e.ds.r1.b2"],
}

bump_range_ip5 = {
    "ele_start": ["s.ds.l5.b1", "s.ds.l5.b2"],
    "ele_stop": ["e.ds.r5.b1", "e.ds.r5.b2"],
}


angle_match_ip1 = 295e-6
sep_match = 2e-3

scale = 23348.89927 / 2
# ---------- on_x1hs ----------
CLIMmcbxfb = 2.5 / scale
CLIMmcbxfa = 4.5 / scale
CLIMmcbrd = 5.0 / scale
CLIMmcby4 = 2.25 / scale
CLIMmcbc = 2.1 / scale

# %%
correctors_psep = [
    "acbxhv1.lirn",
    "acbxhv1.rirn",
    "acbxhv2.lirn",
    "acbxhv2.rirn",
    "acbxhv3.lirn",
    "acbxhv3.rirn",
    "acbrdhv4.lirnb1",
    "acbrdhv4.lirnb2",
    "acbrdhv4.rirnb1",
    "acbrdhv4.rirnb2",
    "acbyhvs4.lirnb1",
    "acbyhvs4.lirnb2",
    "acbyhvs4.rirnb1",
    "acbyhvs4.rirnb2",
    "acbyhv4.lirnb1",
    "acbyhv4.lirnb2",
    "acbyhv4.rirnb1",
    "acbyhv4.rirnb2",
    "xyipirnb1",
    "xyipirnb2",
]

for irn in ["1", "5"]:
    for hv in ["h", "v"]:
        for xy in ["x", "y"]:
            for cor in correctors_psep:
                cor_name = cor.replace("hv", hv).replace("xy", xy).replace("irn", irn)
                collider.vars[cor_name] = 0
            #     print(cor_name)
            # print("#########")

# %%
for irn in ["1", "5"]:
    for hv in ["h", "v"]:
        for bim in ["b1", "b2"]:
            connect_target = (
                "acbyhvs4.lirnb1".replace("irn", irn)
                .replace("hv", hv)
                .replace("b1", bim)
            )
            connect_from = (
                "acbrdhv4.lirnb1".replace("irn", irn)
                .replace("hv", hv)
                .replace("b1", bim)
            )
            collider.vars[connect_target] = collider.vars[connect_from]
            collider.vars[connect_target.replace("l", "r")] = collider.vars[
                connect_from.replace("l", "r")
            ]

            connect_target = (
                "acbyhv4.lirnb1".replace("irn", irn)
                .replace("hv", hv)
                .replace("b1", bim)
            )
            collider.vars[connect_target] = collider.vars[connect_from]
            collider.vars[connect_target.replace("l", "r")] = collider.vars[
                connect_from.replace("l", "r")
            ]

collider.vars["acbxh2.l1"] = collider.vars["acbxh1.l1"]
collider.vars["acbxh2.r1"] = collider.vars["acbxh1.r1"]
collider.vars["acbxv2.l1"] = collider.vars["acbxv1.l1"]
collider.vars["acbxv2.r1"] = collider.vars["acbxv1.r1"]


collider.vars["acbxh2.l5"] = collider.vars["acbxh1.l5"]
collider.vars["acbxh2.r5"] = collider.vars["acbxh1.r5"]
collider.vars["acbxv2.l5"] = collider.vars["acbxv1.l5"]
collider.vars["acbxv2.r5"] = collider.vars["acbxv1.r5"]

# %%
collider.vars["cd2q4"] = 1
# %%
opt_sep1h = collider.match_knob(
    knob_name="on_sep1h",
    knob_value_start=0,
    knob_value_end=(sep_match * 1e3),
    targets=(
        targets_close_bump
        + [
            xt.TargetSet(line="lhcb1", at="ip1", x=sep_match, px=0),
            xt.TargetSet(line="lhcb2", at="ip1", x=-sep_match, px=0),
        ]
    ),
    vary=[
        xt.VaryList(
            ["acbrdh4.l1b1", "acbrdh4.l1b2", "acbrdh4.r1b1", "acbrdh4.r1b2"],
            step=1.0e-12,
            limits=(-CLIMmcbrd, CLIMmcbrd),
        ),
        xt.VaryList(
            ["acbxh1.l1", "acbxh1.r1"],
            step=1.0e-12,
            limits=(-CLIMmcbxfb, CLIMmcbxfb),
            tag="mcbx",
        ),
        xt.VaryList(
            ["acbxh3.l1", "acbxh3.r1"],
            step=1.0e-12,
            limits=(-CLIMmcbxfa, CLIMmcbxfa),
            tag="mcbx",
        ),
    ],
    run=False,
    assert_within_tol=False,
    restore_if_fail=False,
    twiss_init=twinit_zero_orbit,
    n_steps_max=25,
    **bump_range_ip1,
)

opt_sep1h.solve()
opt_sep1h.generate_knob()
opt_sep1h.log()

# %%
on_sep1v = collider.match_knob(
    knob_name="on_sep1v",
    knob_value_start=0,
    knob_value_end=(sep_match * 1e3),
    targets=(
        targets_close_bump
        + [
            xt.TargetSet(line="lhcb1", at="ip1", y=sep_match, py=0),
            xt.TargetSet(line="lhcb2", at="ip1", y=-sep_match, py=0),
        ]
    ),
    vary=[
        xt.VaryList(
            ["acbrdv4.l1b1", "acbrdv4.l1b2", "acbrdv4.r1b1", "acbrdv4.r1b2"],
            step=1.0e-12,
            limits=(-CLIMmcbrd, CLIMmcbrd),
        ),
        xt.VaryList(
            ["acbxv1.l1", "acbxv1.r1"],
            step=1.0e-12,
            limits=(-CLIMmcbxfb, CLIMmcbxfb),
            tag="mcbx",
        ),
        xt.VaryList(
            ["acbxv3.l1", "acbxv3.r1"],
            step=1.0e-12,
            limits=(-CLIMmcbxfa, CLIMmcbxfa),
            tag="mcbx",
        ),
    ],
    run=False,
    assert_within_tol=False,
    restore_if_fail=False,
    twiss_init=twinit_zero_orbit,
    n_steps_max=25,
    **bump_range_ip1,
)

on_sep1v.solve()
on_sep1v.generate_knob()
on_sep1v.log()


# %%
opt_sep5h = collider.match_knob(
    knob_name="on_sep5h",
    knob_value_start=0,
    knob_value_end=(sep_match * 1e3),
    targets=(
        targets_close_bump
        + [
            xt.TargetSet(line="lhcb1", at="ip5", x=sep_match, px=0),
            xt.TargetSet(line="lhcb2", at="ip5", x=-sep_match, px=0),
        ]
    ),
    vary=[
        xt.VaryList(
            ["acbrdh4.l5b1", "acbrdh4.l5b2", "acbrdh4.r5b1", "acbrdh4.r5b2"],
            step=1.0e-12,
            limits=(-CLIMmcbrd, CLIMmcbrd),
        ),
        xt.VaryList(
            ["acbxh1.l5", "acbxh1.r5"],
            step=1.0e-12,
            limits=(-CLIMmcbxfb, CLIMmcbxfb),
            tag="mcbx",
        ),
        xt.VaryList(
            ["acbxh3.l5", "acbxh3.r5"],
            step=1.0e-12,
            limits=(-CLIMmcbxfa, CLIMmcbxfa),
            tag="mcbx",
        ),
    ],
    run=False,
    assert_within_tol=False,
    restore_if_fail=False,
    twiss_init=twinit_zero_orbit,
    n_steps_max=25,
    **bump_range_ip5,
)

opt_sep5h.solve()
opt_sep5h.generate_knob()
opt_sep5h.log()
# %%
on_sep5v = collider.match_knob(
    knob_name="on_sep5v",
    knob_value_start=0,
    knob_value_end=(sep_match * 1e3),
    targets=(
        targets_close_bump
        + [
            xt.TargetSet(line="lhcb1", at="ip5", y=sep_match, py=0),
            xt.TargetSet(line="lhcb2", at="ip5", y=-sep_match, py=0),
        ]
    ),
    vary=[
        xt.VaryList(
            ["acbrdv4.l5b1", "acbrdv4.l5b2", "acbrdv4.r5b1", "acbrdv4.r5b2"],
            step=1.0e-12,
            limits=(-CLIMmcbrd, CLIMmcbrd),
        ),
        xt.VaryList(
            ["acbxv1.l5", "acbxv1.r5"],
            step=1.0e-12,
            limits=(-CLIMmcbxfb, CLIMmcbxfb),
            tag="mcbx",
        ),
        xt.VaryList(
            ["acbxv3.l5", "acbxv3.r5"],
            step=1.0e-12,
            limits=(-CLIMmcbxfa, CLIMmcbxfa),
            tag="mcbx",
        ),
    ],
    run=False,
    assert_within_tol=False,
    restore_if_fail=False,
    twiss_init=twinit_zero_orbit,
    n_steps_max=25,
    **bump_range_ip5,
)

on_sep5v.solve()
on_sep5v.generate_knob()
on_sep5v.log()
# %%
v = collider.vars
f = collider.functions
v["phi_ir1"] = 0.0
v["phi_ir5"] = 90.0
for irn in [1, 5]:
    v[f"cphi_ir{irn}"] = f.cos(v[f"phi_ir{irn}"] * np.pi / 180.0)
    v[f"sphi_ir{irn}"] = f.sin(v[f"phi_ir{irn}"] * np.pi / 180.0)
    v[f"on_x{irn}hs"] = (1.0 - v["cd2q4"]) * v[f"on_x{irn}"] * v[f"cphi_ir{irn}"]
    v[f"on_x{irn}vs"] = (1.0 - v["cd2q4"]) * v[f"on_x{irn}"] * v[f"sphi_ir{irn}"]
    v[f"on_x{irn}hl"] = v["cd2q4"] * v[f"on_x{irn}"] * v[f"cphi_ir{irn}"]
    v[f"on_x{irn}vl"] = v["cd2q4"] * v[f"on_x{irn}"] * v[f"sphi_ir{irn}"]
    v[f"on_sep{irn}h"] = -v[f"on_sep{irn}"] * v[f"sphi_ir{irn}"]
    v[f"on_sep{irn}v"] = v[f"on_sep{irn}"] * v[f"cphi_ir{irn}"]

# %%
# TEST on_sep1h
collider.vars["cd2q4"] = 1
collider.vars["on_sep1"] = 2
collider.vars["phi_ir1"] = 90
tw1 = collider.twiss()
# %%
print(tw1["lhcb1"][["x", "y", "px", "py"], "ip.*"])
print(tw1["lhcb2"][["x", "y", "px", "py"], "ip.*"])
# %%
plot_orbit(tw1, "lhcb1")
plot_orbit(tw1, "lhcb2")
# %%
# TEST on_sep1v, has some residuals
collider.vars["cd2q4"] = 1
collider.vars["on_sep1"] = 2
collider.vars["phi_ir1"] = 0.0
tw1 = collider.twiss()
# %%
print(tw1["lhcb1"][["x", "y", "px", "py"], "ip.*"])
print(tw1["lhcb2"][["x", "y", "px", "py"], "ip.*"])
# %%
plot_orbit(tw1, "lhcb1")
plot_orbit(tw1, "lhcb2")

# %%
collider.vars["on_sep1"] = 0
collider.vars["on_sep5"] = 0
collider.vars["phi_ir1"] = 0
collider.vars["phi_ir5"] = 90.0


# %%
# TEST on_sep5v, has some residuals
collider.vars["cd2q4"] = 1
collider.vars["on_sep5"] = 2
collider.vars["phi_ir5"] = 0.0
tw1 = collider.twiss()
# %%
print(tw1["lhcb1"][["x", "y", "px", "py"], "ip.*"])
print(tw1["lhcb2"][["x", "y", "px", "py"], "ip.*"])
# %%
plot_orbit(tw1, "lhcb1")
plot_orbit(tw1, "lhcb2")

# %%
# TEST on_sep1h
collider.vars["cd2q4"] = 1
collider.vars["on_sep5"] = 2
collider.vars["phi_ir5"] = 90.0
tw1 = collider.twiss()
# %%
print(tw1["lhcb1"][["x", "y", "px", "py"], "ip.*"])
print(tw1["lhcb2"][["x", "y", "px", "py"], "ip.*"])
# %%
plot_orbit(tw1, "lhcb1")
plot_orbit(tw1, "lhcb2")

# %%

# knobs to check for on_sep1h
cor_sep1h = [
    "acbxh1.l1",
    "acbxh1.r1",
    "acbxh2.l1",
    "acbxh2.r1",
    "acbxh3.l1",
    "acbxh3.r1",
    "acbrdh4.l1b1",
    "acbrdh4.l1b2",
    "acbrdh4.r1b1",
    "acbrdh4.r1b2",
    "acbyhs4.l1b1",
    "acbyhs4.l1b2",
    "acbyhs4.r1b1",
    "acbyhs4.r1b2",
    "acbyh4.r1b1",
    "acbyh4.l1b2",
]

for cor in cor_sep1h:
    # print(collider.vars[cor]._info())
    print(cor, collider.vars[cor]._get_value())
    print(50 * "#")

# %%
# knobs to check for on_sep1v
cor_sep1v = [
    "acbxv1.l1",
    "acbxv1.r1",
    "acbxv2.l1",
    "acbxv2.r1",
    "acbxv3.l1",
    "acbxv3.r1",
    "acbrdv4.l1b1",
    "acbrdv4.l1b2",
    "acbrdv4.r1b1",
    "acbrdv4.r1b2",
    "acbyvs4.l1b1",
    "acbyvs4.l1b2",
    "acbyvs4.r1b1",
    "acbyvs4.r1b2",
    "acbyv4.l1b1",
    "acbyv4.r1b2",
]

for cor in cor_sep1v:
    print(collider.vars[cor]._info())
    print(50 * "#")
# %%
# knobs to check for on_sep5v
cor_sep5v = [
    "acbxv1.l5",
    "acbxv1.r5",
    "acbxv2.l5",
    "acbxv2.r5",
    "acbxv3.l5",
    "acbxv3.r5",
    "acbrdv4.l5b1",
    "acbrdv4.l5b2",
    "acbrdv4.r5b1",
    "acbrdv4.r5b2",
    "acbyvs4.l5b1",
    "acbyvs4.l5b2",
    "acbyvs4.r5b1",
    "acbyvs4.r5b2",
    "acbyv4.l5b1",
    "acbyv4.r5b2",
]

for cor in cor_sep5v:
    print(collider.vars[cor]._info())
    print(50 * "#")

# %%
cor_sep5h = [
    "acbxh1.l5",
    "acbxh1.r5",
    "acbxh2.l5",
    "acbxh2.r5",
    "acbxh3.l5",
    "acbxh3.r5",
    "acbrdh4.l5b1",
    "acbrdh4.l5b2",
    "acbrdh4.r5b1",
    "acbrdh4.r5b2",
    "acbyhs4.l5b1",
    "acbyhs4.l5b2",
    "acbyhs4.r5b1",
    "acbyhs4.r5b2",
    "acbyh4.r5b1",
    "acbyh4.l5b2",
]

for cor in cor_sep5h:
    print(collider.vars[cor]._info())
    print(50 * "#")

# %%
