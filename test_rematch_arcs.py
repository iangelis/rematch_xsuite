# %%
import os
import numpy as np
from pathlib import Path
import pandas as pd
from cpymad import libmadx
from cpymad.madx import Madx
import matplotlib.pyplot as plt


import xtrack as xt
import xmask as xm
import xpart as xp
import xmask.lhc as xlhc
import xobjects as xo

import xtrack._temp.lhc_match as lm

from misc import *

try:
    get_ipython().run_line_magic("matplotlib", "inline")
except:
    pass


# %%
prefix = Path("../").absolute()

f1 = prefix / "summer_studies/temp_optics/opt_round_1100_1500.madx"

simple_name = "collider_from_madx.json"

collider0 = xt.Multiline.from_json(simple_name)


collider = xt.Multiline.from_json(simple_name)


collider0.build_trackers()
collider0.vars.load_madx_optics_file(f1)

collider0.lhcb1.twiss_default["method"] = "4d"
collider0.lhcb2.twiss_default["method"] = "4d"
collider0.lhcb2.twiss_default["reverse"] = True


collider.build_trackers()
collider.vars.load_madx_optics_file(f1)

collider.lhcb1.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["reverse"] = True

# %%
tw = collider.twiss(method="4d")
tw0 = collider0.twiss(method="4d")


# %%
cols = ["betx", "alfx", "mux", "bety", "alfy", "muy", "dx", "dpx"]

# %%

for arc in lm.ARC_NAMES:
    a = phasearc(collider, "lhcb1", arc[0], arc[1], arc)
    print(a["cell_tw"]["baux"][cols])
    print(a["sir_baux"][cols])

    b = phasearc(collider, "lhcb2", arc[0], arc[1], arc)
    print(b["cell_tw"]["baux"][cols])
    print(b["sir_baux"][cols])

    print(50 * "#")


# %%
tars_mux_b1 = {}
tars_muy_b1 = {}
tars_mux_b2 = {}
tars_muy_b2 = {}

for arc in lm.ARC_NAMES:
    tars_mux_b1[arc] = collider.varval[f"mux{arc}b1"]
    tars_muy_b1[arc] = collider.varval[f"muy{arc}b1"]
    tars_mux_b2[arc] = collider.varval[f"mux{arc}b2"]
    tars_muy_b2[arc] = collider.varval[f"muy{arc}b2"]


# %%

opts_arcs = {}
for arc in lm.ARC_NAMES:
    opt = rematch_arc(
        collider,
        arc,
        tars_mux_b1[arc],
        tars_muy_b1[arc],
        tars_mux_b2[arc],
        tars_muy_b2[arc],
    )
    opts_arcs[arc] = opt

# %%
for arc in lm.ARC_NAMES:
    print(opts_arcs[arc].show())
    print(opts_arcs[arc].log())

    print(f"ARC {arc}")
    print(f"{collider.varval[f'kqf.a{arc}']=},{collider.varval[f'kqd.a{arc}']=}")
    print(f"{collider.varval[f'kqtf.a{arc}b1']=}")
    print(f"{collider.varval[f'kqtd.a{arc}b1']=}")
    print(f"{collider.varval[f'kqtf.a{arc}b2']=}")
    print(f"{collider.varval[f'kqtd.a{arc}b2']=}")
    print(50 * "#")

# %%
tw = collider.twiss(method="4d")
# %%
tw["lhcb1"][cols, ["ip1", "ip5"]].show(digits=5)
tw0["lhcb1"][cols, ["ip1", "ip5"]].show(digits=5)

# %%
collider.varval["kqtf.b1"] = 1e-5
collider.varval["kqtd.b1"] = 1e-5
collider.varval["kqtf.b2"] = 1e-5
collider.varval["kqtd.b2"] = 1e-5
# %%
opts_tune = rematch_tune(collider, 62.31, 60.32, 62.31, 60.32, solve=True)
# %%
for line in ["b1", "b2"]:
    print(
        collider[f"lhc{line}"].varval[f"kqtf.{line}"],
        collider[f"lhc{line}"].varval[f"kqtd.{line}"],
    )
    print(opts_tune[line].show())
    print(opts_tune[line].log())
# %%
tw = collider.twiss(method="4d")
# %%
tw["lhcb1"][cols, ["ip1", "ip5"]].show(digits=5)
print(tw["lhcb1"].qx, tw["lhcb1"].qy)
# %%
