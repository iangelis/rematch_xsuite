# %%
import os
import numpy as np
import pandas as pd
from cpymad import libmadx
from cpymad.madx import Madx
import matplotlib.pyplot as plt


import xtrack as xt
import xmask as xm
import xpart as xp
import xmask.lhc as xlhc
import xobjects as xo

from misc import *

# %%
import xtrack._temp.lhc_match as lm

try:
    get_ipython().run_line_magic("matplotlib", "inline")
except:
    pass

# %%


def build_sequence(
    prefix, mad, mylhcbeam, optics_version="1.6", ignore_cycling=False, ignore_CC=False
):
    # Select beam
    mad.input(f"mylhcbeam = {mylhcbeam};")

    mad.input(
        f"""
    option,-echo,-info;
    system,"mkdir temp";
    call,file="{prefix}/acc-models-lhc/lhc.seq";
    !call,file="{prefix}/acc-models-lhc/hllhc_sequence.madx";
    call,file="{prefix}/acc-models-lhc/toolkit/macro.madx";
    """
    )

    mad.input(
        f"""
      ! Build sequence
      option, -echo,-warn,-info;
      if (mylhcbeam==4){{
        call,file="{prefix}acc-models-lhc/lhcb4.seq";
      }} else {{
        call,file="{prefix}/acc-models-lhc/lhc.seq";
      }};
      option, -echo, warn,-info;
      """
    )

    mad.input(
        """
    l.mbh = 0.001000;
    ACSCA, HARMON := HRF400;
    """
    )

    mad.input(
        f"""
      !Install HL-LHC
      call, file=
        "{prefix}/acc-models-lhc/hllhc_sequence.madx";
      """
        """
      ! Slice nominal sequence
      exec, myslice;
      """
    )

    if not ignore_cycling:
        mad.input(
            """
        !Cycling w.r.t. to IP3 (mandatory to find closed orbit in collision in the presence of errors)
        if (mylhcbeam<3){
        seqedit, sequence=lhcb1; flatten; cycle, start=IP3; flatten; endedit;
        };
        seqedit, sequence=lhcb2; flatten; cycle, start=IP3; flatten; endedit;
        """
        )

    if not ignore_CC:
        mad.input(
            f"""
        ! Install crab cavities (they are off)
        call, file='{prefix}/acc-models-lhc/toolkit/enable_crabcavities.madx';
        on_crab1 = 0;
        on_crab5 = 0;
        """
        )

    mad.input(
        """
        ! Set twiss formats for MAD-X parts (macro from opt. toolkit)
        exec, twiss_opt;
        """
    )


def apply_optics(mad, optics_file):
    mad.call(optics_file)
    # A knob redefinition
    mad.input("on_alice := on_alice_normalized * 7000./nrj;")
    mad.input("on_lhcb := on_lhcb_normalized * 7000./nrj;")


def build_collider_from_mad(prefix, optics_filename):
    # Make mad environment
    links = {"acc-models-lhc": "../"}
    xm.make_mad_environment(links=links)

    # Start mad
    mad_b1b2 = Madx(command_log="mad_collider.log")
    mad_b4 = Madx(command_log="mad_b4.log")

    # Build sequences
    build_sequence(prefix, mad_b1b2, mylhcbeam=1)
    build_sequence(prefix, mad_b4, mylhcbeam=4)

    # Apply optics (only for b1b2, b4 will be generated from b1b2)
    apply_optics(mad_b1b2, optics_file=optics_filename)

    beam_config = {
        "lhcb1": {"beam_energy_tot": 7000},
        "lhcb2": {"beam_energy_tot": 7000},
    }
    # Build xsuite collider
    collider = xlhc.build_xsuite_collider(
        sequence_b1=mad_b1b2.sequence.lhcb1,
        sequence_b2=mad_b1b2.sequence.lhcb2,
        sequence_b4=mad_b4.sequence.lhcb2,
        beam_config=beam_config,
        enable_imperfections=False,
        enable_knob_synthesis=True,
        rename_coupling_knobs=True,
        # pars_for_imperfections=None,
        # ver_lhc_run=None,
        ver_hllhc_optics=1.6,
    )

    # Return collider
    return collider


def load_cpymad(prefix, optics_file):
    mad = Madx(stdout=True)
    mad.chdir(prefix)

    mad.input(
        """
            option,-echo,-info;
            call,file="acc-models-lhc/lhc.seq";
            call,file="acc-models-lhc/hllhc_sequence.madx";
            call,file="acc-models-lhc/toolkit/macro.madx";
            """
    )

    mad.input("exec,mk_beam(7000);")
    mad.input(f"""call,file="{optics_file}";""")
    mad.input(
        """
        !Cycling w.r.t. to IP3 (mandatory to find closed orbit in collision in the presence of errors)
        if (mylhcbeam<3){
        seqedit, sequence=lhcb1; flatten; cycle, start=IP3; flatten; endedit;
        };
        seqedit, sequence=lhcb2; flatten; cycle, start=IP3; flatten; endedit;
        """
    )
    return mad


# %%

f1 = (
    # prefix + "opt_flathv_500_2000_thin_testing.madx"
    prefix
    # + "acc-models-lhc/strengths/round/levelling/opt_levelling_580_1500_thin.madx"
    # + "acc-models-lhc/strengths/flat/opt_flathv_500_2000_thin.madx"
    # + "summer_studies/collapse/opt_collapse_700_1500_thin.madx"
    # + "summer_studies/collapse/opt_collapse_1100_1500_thin.madx"
    # + "summer_studies/collapse/opt_flathv_450_1800_1500_thin.madx"
    # + "start_of_collapse/flat/opt_collapse_flathv_700_2800_test.madx"
    # + "summer_studies/collapse/opt_collapse_700_1500_rematched_again_thin.madx"
    # + "summer_studies/collapse/opt_collapse_flathv_700_2800_thin.madx"
    # + "summer_studies/collapse/opt_collapse_flathv_900_1800_1500_thin.madx"
    # + "summer_studies/collapse/opt_collapse_flathv_900_1800_1500_thin.madx"
    # + "summer_studies/SoL/opt_flathv_300_1200_1500_thin.madx"
    # + "summer_studies/SoL/opt_flathv_300_1200_1500.madx"
    + "summer_studies/temp_optics/opt_round_1100_1500.madx"
)
simple_name = f1.split("/")[-1].split(".")[0] + ".json"


collider = None
if os.path.exists(prefix + simple_name):
    collider = xt.Multiline.from_json(prefix + simple_name)
else:
    collider = build_collider_from_mad(prefix, f1)
    collider.to_json(prefix + simple_name)
# %%
collider.lhcb1.particle_ref = xp.Particles(p0c=7000e9, q0=1, mass0=xp.PROTON_MASS_EV)
collider.lhcb2.particle_ref = xp.Particles(p0c=7000e9, q0=1, mass0=xp.PROTON_MASS_EV)
collider.build_trackers()

# %%
collider.lhcb1.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["reverse"] = True

# %%
for aa in lm.ARC_NAMES:
    print(f"Matching arc {aa}")
    target_mux_b1 = collider.varval[f"mux{aa}b1"]
    target_muy_b1 = collider.varval[f"muy{aa}b1"]
    target_mux_b2 = collider.varval[f"mux{aa}b2"]
    target_muy_b2 = collider.varval[f"muy{aa}b2"]
    print(
        collider.varval[f"mux{aa}b1"],
        collider.varval[f"muy{aa}b1"],
        collider.varval[f"mux{aa}b2"],
        collider.varval[f"muy{aa}b2"],
    )
# %%
# opts = []
# for aa in lm.ARC_NAMES:
#     print(f"Matching arc {aa}")
#     target_mux_b1 = collider.varval[f"mux{aa}b1"]
#     target_muy_b1 = collider.varval[f"muy{aa}b1"]
#     target_mux_b2 = collider.varval[f"mux{aa}b2"]
#     target_muy_b2 = collider.varval[f"muy{aa}b2"]

#     opt = lm.match_arc_phase_advance(
#         collider=collider,
#         arc_name=aa,
#         target_mux_b1=target_mux_b1,
#         target_muy_b1=target_muy_b1,
#         target_mux_b2=target_mux_b2,
#         target_muy_b2=target_muy_b2,
#     )
#     opts.append(opt)

# # %%
# for aa in lm.ARC_NAMES:
#     print(f"Matching arc {aa}")
#     target_mux_b1 = collider.varval[f"mux{aa}b1"]
#     target_muy_b1 = collider.varval[f"muy{aa}b1"]
#     target_mux_b2 = collider.varval[f"mux{aa}b2"]
#     target_muy_b2 = collider.varval[f"muy{aa}b2"]
#     print(
#         collider.varval[f"mux{aa}b1"],
#         collider.varval[f"muy{aa}b1"],
#         collider.varval[f"mux{aa}b2"],
#         collider.varval[f"muy{aa}b2"],
#     )

# %%
# collider.varval["on_disp"] = 1
# collider.varval["on_x1"] = 250
# collider.varval["on_x5"] = 0
collider.vars["on_disp"] = 1
collider.vars["on_x1"] = 250
collider.vars["on_x5"] = 0

lb1_c = collider.lhcb1.cycle("ip3")

lb1_c.twiss_default["method"] = "4d"
tw_init = xt.TwissInit(
    line=lb1_c,
    element_name="ip8",
    particle_ref=lb1_c.particle_ref,
)
tw1 = lb1_c.twiss(ele_start="ip8", ele_stop="ip2", twiss_init=tw_init)
# tw1 = lb1_c.twiss()

# collider.varval["on_disp"] = 0
# collider.varval["on_x1"] = 0
# collider.varval["on_x5"] = 0
fig, axs = plt.subplots(2, 1, figsize=(8, 8))
axs[0].plot(tw1["s"], tw1["x"], label="x")
axs[0].plot(tw1["s"], tw1["y"], label="y")
axs[0].set_xlabel("s [m]")
axs[0].set_ylabel("x,y [m]")
axs[0].legend()

axs[1].plot(tw1["s"], tw1["x"], label="x")
axs[1].plot(tw1["s"], tw1["y"], label="y")
axs[1].set_xlabel("s [m]")
axs[1].set_ylabel("x,y [m]")
axs[1].set_ylim(-0.0001, 0.0001)
axs[1].legend()
plt.show()

# %%
collider.varval["on_disp"] = 0
collider.varval["on_x1"] = 0
collider.varval["on_x5"] = 0
lb1_c = collider.lhcb1.cycle("ip3")

lb1_c.twiss_default["method"] = "4d"
tw_init = xt.TwissInit(
    line=lb1_c,
    element_name="ip8",
    particle_ref=lb1_c.particle_ref,
)
tw1 = lb1_c.twiss(ele_start="ip8", ele_stop="ip2", twiss_init=tw_init)
collider.varval["on_disp"] = 0
collider.varval["on_x1"] = 0
collider.varval["on_x5"] = 0
# %%
fig, axs = plt.subplots(2, 1, figsize=(8, 8))
axs[0].plot(tw1["s"], tw1["x"], label="x")
axs[0].plot(tw1["s"], tw1["y"], label="y")
axs[0].set_xlabel("s [m]")
axs[0].set_ylabel("x,y [m]")
axs[0].legend()

axs[1].plot(tw1["s"], tw1["x"], label="x")
axs[1].plot(tw1["s"], tw1["y"], label="y")
axs[1].set_xlabel("s [m]")
axs[1].set_ylabel("x,y [m]")
axs[1].set_ylim(-0.0001, 0.0001)
axs[1].legend()
plt.show()

# %%

var_names = [
    "acbh12.l1b1",
    "acbv13.l1b1",
    "acbh14.l1b1",
    "acbv15.l1b1",
    "acbh15.l2b1",
    "acbv16.l2b1",
    "acbv12.r1b1",
    "acbh13.r1b1",
    "acbv14.r1b1",
    "acbh15.r1b1",
    "acbv15.r8b1",
    "acbh16.r8b1",
]

old_vals = {}
for vv in var_names:
    old_vals[vv] = collider.varval[vv]
    print(collider.vars[vv]._expr)
    # collider.vars[vv] = 0


def rematch_disp_ir1_b1(collider: xt.Multiline, tol=5e-27, knob_name=""):
    kmcb_max = 800.0e-6
    twiss_pre = collider.lhcb1.twiss(at_elements="ip8")
    tw_init = xt.TwissInit(
        line=collider.lhcb1,
        element_name="ip8",
        x=0,
        px=0,
        y=0,
        py=0,
        betx=1,
        bety=1,
        dx=twiss_pre["dx"][0],
        dy=0,
        dpx=twiss_pre["dpx"][0],
        dpy=0,
        particle_ref=collider.lhcb1.particle_ref,
    )
    # print(collider.vars[knob_name]._value)
    res = collider.lhcb1.match_knob(
        knob_name=knob_name,
        # knob_value_start=1,
        knob_value_end=1,
        run=False,
        assert_within_tol=False,
        solver="jacobian",
        solver_options={
            "n_bisections": 3,
            "min_step": 1e-19,
            "n_steps_max": 15,
            "tol": tol,
        },
        default_tol=tol,
        ele_start="ip8",
        ele_stop="ip2",
        # twiss_init="preserve",
        twiss_init=tw_init,
        vary=[
            xt.Vary(name="acbh12.l1b1", limits=(-kmcb_max, kmcb_max), step=1e-18),
            xt.Vary(name="acbv13.l1b1", limits=(-kmcb_max, kmcb_max), step=1e-18),
            xt.Vary(name="acbh14.l1b1", limits=(-kmcb_max, kmcb_max), step=1e-18),
            xt.Vary(name="acbv15.l1b1", limits=(-kmcb_max, kmcb_max), step=1e-18),
            xt.Vary(name="acbh15.l2b1", limits=(-kmcb_max, kmcb_max), step=1e-18),
            xt.Vary(name="acbv16.l2b1", limits=(-kmcb_max, kmcb_max), step=1e-18),
            xt.Vary(name="acbv12.r1b1", limits=(-kmcb_max, kmcb_max), step=1e-18),
            xt.Vary(name="acbh13.r1b1", limits=(-kmcb_max, kmcb_max), step=1e-18),
            xt.Vary(name="acbv14.r1b1", limits=(-kmcb_max, kmcb_max), step=1e-18),
            xt.Vary(name="acbh15.r1b1", limits=(-kmcb_max, kmcb_max), step=1e-18),
            xt.Vary(name="acbv15.r8b1", limits=(-kmcb_max, kmcb_max), step=1e-18),
            xt.Vary(name="acbh16.r8b1", limits=(-kmcb_max, kmcb_max), step=1e-18),
        ],
        targets=[
            xt.TargetList(tars=["x", "y"], value=0, at="e.ds.l1.b1"),
            xt.TargetList(tars=["px", "py"], value=0, at="e.ds.l1.b1"),
            xt.TargetList(tars=["dx", "dy"], value=0, at="ip1"),
            xt.TargetList(tars=["x", "y"], value=0, at="e.ds.l2.b1"),
            xt.TargetList(tars=["px", "py"], value=0, at="e.ds.l2.b1"),
            xt.TargetList(tars=["dx", "dy"], value="preserve", at="ip2"),
            #     xt.Target(tar="x", value=0, at="e.ds.l1.b1"),
            #     xt.Target(tar="px", value=0, at="e.ds.l1.b1"),
            #     xt.Target(tar="dx", value=0, at="ip1"),
            #     xt.Target(tar="x", value=0, at="e.ds.l2.b1"),
            #     xt.Target(tar="px", value=0, at="e.ds.l2.b1"),
            #     xt.Target(tar="dx", value="preserve", at="ip2"),
        ],
    )

    sol = res.solve()
    return res


# %%
# collider.vars["on_disp"]._set_value(0)
# collider.vars["on_x1hs"]._set_value(295)
# collider.vars["on_sep1v"]._set_value(1)
# res = rematch_disp_ir1_b1(collider, knob_name="my_dx1")
# collider.vars["on_disp"]._set_value(0)
# collider.vars["on_x1hs"]._set_value(0)
# collider.vars["on_sep1v"]._set_value(0)
# print(res.log())

# collider.vars["on_disp"]._set_value(0)
# collider.vars["on_x1vs"]._set_value(295)
# collider.vars["on_sep1h"]._set_value(1)
# res = rematch_disp_ir1_b1(collider)
# collider.vars["on_disp"]._set_value(0)
# collider.vars["on_x1vs"]._set_value(0)
# collider.vars["on_sep1h"]._set_value(0)
# print(res.log())


# collider.vars["on_disp"]._set_value(0)
# collider.vars["on_x1hl"]._set_value(295)
# collider.vars["on_sep1v"]._set_value(1)
# res = rematch_disp_ir1_b1(collider)
# collider.vars["on_disp"]._set_value(0)
# collider.vars["on_x1hl"]._set_value(0)
# collider.vars["on_sep1v"]._set_value(0)
# print(res.log())

# collider.vars["on_disp"]._set_value(0)
# collider.vars["on_x1vl"]._set_value(295)
# collider.vars["on_sep1h"]._set_value(1)
# res = rematch_disp_ir1_b1(collider)
# collider.vars["on_disp"]._set_value(0)
# collider.vars["on_x1vl"]._set_value(0)
# collider.vars["on_sep1h"]._set_value(0)
# print(res.log())

# %%
# for vv in var_names:
# print(collider.vars[f"{vv}_from_my_dx1"]._value)
# print(collider.vars[f"{vv}_from_my_dx1"]._expr)
# print(collider.vars[vv]._value)
print()
for vv in var_names:
    print(collider.vars[vv]._expr)
    print(vv, collider.vars[vv]._value)
# %%
# res.generate_knob()
# %%
for vv in var_names:
    print(collider.vars[f"{vv}_from_my_dx1"]._value)
    print(collider.vars[vv]._value)
for vv in var_names:
    print(collider.vars[vv]._expr)
# for oldn, oldv in old_vals.items():
#     print(oldn, oldv)
# %%
# collider.vars["on_disp"]._set_value(1)
# collider.vars["on_x1"]._set_value(250)
# collider.vars["on_sep1h"]._set_value(1)
for vn in var_names:
    print(vn, collider.varval[vn])
    print(collider.vars[vn]._expr)
# for vn in var_names:
#     # collider.varval[vn] = old_vals[vn]
#     collider.vars[vn]._set_value(old_vals[vn])
# %%
collider.varval["on_disp"] = 0
# collider.varval["cd2q4"] = 1
# collider.varval["on_sep1"] = 0
# collider.varval["on_x1hs"] = 0
collider.varval["on_x1"] = 250
collider.varval["on_x5"] = 250
lb1_c = collider.lhcb1.cycle("ip3")
tw1_no_disp = lb1_c.twiss(method="4d")
collider.varval["on_disp"] = 1
# collider.varval["my_dx1"] = 0

lb1_c.twiss_default["method"] = "4d"
tw1_disp = lb1_c.twiss(method="4d")
# tw_init = xt.TwissInit(
#     line=lb1_c,
#     element_name="ip8",
#     particle_ref=lb1_c.particle_ref,
# )
# tw1 = lb1_c.twiss(ele_start="ip8", ele_stop="ip2", twiss_init=tw_init)

# tw1 = lb1_c.twiss()[:, f"s.ds.l1.b1":f"e.ds.r1.b1"]
# %%
fig, axs = plt.subplots(2, 1, figsize=(8, 8))
fig.suptitle("Dispersion Off")
axs[0].plot(tw1_no_disp["s"], tw1_no_disp["x"], label="x")
axs[0].plot(tw1_no_disp["s"], tw1_no_disp["y"], label="y")
axs[0].set_xlabel("s [m]")
axs[0].set_ylabel("x,y [m]")
axs[0].legend()

axs[1].plot(tw1_no_disp["s"], tw1_no_disp["x"], label="x")
axs[1].plot(tw1_no_disp["s"], tw1_no_disp["y"], label="y")
axs[1].set_xlabel("s [m]")
axs[1].set_ylabel("x,y [m]")
axs[1].set_ylim(-0.0001, 0.0001)
axs[1].legend()
plt.show()

fig, axs = plt.subplots(2, 1, figsize=(8, 8))
fig.suptitle("Dispersion On")
axs[0].plot(tw1_disp["s"], tw1_disp["x"], label="x")
axs[0].plot(tw1_disp["s"], tw1_disp["y"], label="y")
axs[0].set_xlabel("s [m]")
axs[0].set_ylabel("x,y [m]")
axs[0].legend()

axs[1].plot(tw1_disp["s"], tw1_disp["x"], label="x")
axs[1].plot(tw1_disp["s"], tw1_disp["y"], label="y")
axs[1].set_xlabel("s [m]")
axs[1].set_ylabel("x,y [m]")
axs[1].set_ylim(-0.0001, 0.0001)
axs[1].legend()
plt.show()

fig, axs = plt.subplots(figsize=(8, 8))
axs.plot(tw1_disp["s"], tw1_disp["x"] - tw1_no_disp["x"], label="x")
axs.plot(tw1_disp["s"], tw1_disp["y"] - tw1_no_disp["y"], label="y")
axs.set_xlabel("s [m]")
axs.set_ylabel("x,y [m]")
axs.legend()
plt.show()
# fig, axs = plt.subplots(figsize=(8, 8))
# axs.plot(tw1["s"], tw1["betx"], label="betx")
# axs.plot(tw1["s"], tw1["bety"], label="bety")
# axs.set_xlabel("s [m]")
# axs.set_ylabel("x,y [m]")
# axs.legend()
# plt.show()
# %%

# acbh14.l1b1 4.766198044868958e-08
# acbh15.l2b1 -1.8188971278606914e-07
# acbh13.r1b1 1.8188954439929028e-07
# acbh15.r1b1 -1.5274350330664665e-11
# acbh16.r8b1 1.571763635815536e-05


collider.varval["acbv13.l1b1"] = 0
collider.varval["acbv15.l1b1"] = 0
collider.varval["acbv16.l2b1"] = 0
collider.varval["acbv12.r1b1"] = 0
collider.varval["acbv14.r1b1"] = 0
collider.varval["acbv15.r8b1"] = 0

# %%
