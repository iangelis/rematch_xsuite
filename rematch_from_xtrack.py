import xtrack as xt


def rematch_ir2(
    collider,
    line_name,
    boundary_conditions_left,
    boundary_conditions_right,
    mux_ir2,
    muy_ir2,
    betx_ip2,
    bety_ip2,
    relax_match=False,
    solve=True,
    staged_match=False,
    default_tol=None,
):
    assert line_name in ["lhcb1", "lhcb2"]
    bn = line_name[-2:]

    opt = collider[f"lhc{bn}"].match(
        solve=False,
        default_tol=default_tol,
        assert_within_tol=False,
        ele_start=f"s.ds.l2.{bn}",
        ele_stop=f"e.ds.r2.{bn}",
        # Left boundary
        twiss_init="preserve_start",
        table_for_twiss_init=boundary_conditions_left,
        targets=[
            xt.TargetSet(
                at=xt.END,
                tars=("betx", "bety", "alfx", "alfy", "dx", "dpx"),
                value=boundary_conditions_right,
                tag="stage0",
            ),
            xt.TargetSet(
                at="ip2",
                betx=betx_ip2,
                bety=bety_ip2,
                alfx=0,
                alfy=0,
                dx=0,
                dpx=0,
                tag="stage2",
            ),
            xt.TargetRelPhaseAdvance("mux", mux_ir2, tag="stage0"),
            xt.TargetRelPhaseAdvance("muy", muy_ir2, tag="stage0"),
        ],
        vary=[
            xt.VaryList(
                [
                    f"kq9.l2{bn}",
                    f"kq10.l2{bn}",
                    f"kqtl11.l2{bn}",
                    f"kqt12.l2{bn}",
                    f"kqt13.l2{bn}",
                    f"kq9.r2{bn}",
                    f"kq10.r2{bn}",
                    f"kqtl11.r2{bn}",
                    f"kqt12.r2{bn}",
                    f"kqt13.r2{bn}",
                ],
                tag="stage0",
            ),
            xt.VaryList(
                [
                    f"kq4.l2{bn}",
                    f"kq5.l2{bn}",
                    f"kq6.l2{bn}",
                    f"kq7.l2{bn}",
                    f"kq8.l2{bn}",
                    f"kq6.r2{bn}",
                    f"kq7.r2{bn}",
                    f"kq8.r2{bn}",
                ],
                tag="stage1",
            ),
            xt.VaryList([f"kq4.r2{bn}", f"kq5.r2{bn}"], tag="stage2"),
        ],
    )

    if solve:
        if staged_match:
            opt.disable_all_vary()
            opt.disable_all_targets()

            opt.enable_vary(tag="stage0")
            opt.enable_targets(tag="stage0")
            opt.solve()

            opt.enable_vary(tag="stage1")
            opt.solve()

            if not relax_match:
                opt.enable_vary(tag="stage2")
                opt.enable_targets(tag="stage2")
                opt.solve()
        else:
            if not relax_match:
                opt.disable_vary(tag="stage2")
                opt.disable_targets(tag="stage2")
            opt.solve()

    return opt


def rematch_ir3(
    collider,
    line_name,
    boundary_conditions_left,
    boundary_conditions_right,
    mux_ir3,
    muy_ir3,
    alfx_ip3,
    alfy_ip3,
    betx_ip3,
    bety_ip3,
    dx_ip3,
    dpx_ip3,
    solve=True,
    staged_match=False,
    default_tol=None,
):
    assert line_name in ["lhcb1", "lhcb2"]
    bn = line_name[-2:]

    opt = collider[f"lhc{bn}"].match(
        solve=False,
        assert_within_tol=False,
        default_tol=default_tol,
        ele_start=f"s.ds.l3.{bn}",
        ele_stop=f"e.ds.r3.{bn}",
        twiss_init="preserve_start",
        table_for_twiss_init=boundary_conditions_left,
        targets=[
            xt.TargetSet(
                at="ip3",
                alfx=alfx_ip3,
                alfy=alfy_ip3,
                betx=betx_ip3,
                bety=bety_ip3,
                dx=dx_ip3,
                dpx=dpx_ip3,
                tag="stage1",
            ),
            xt.TargetSet(
                at=xt.END,
                tars=("betx", "bety", "alfx", "alfy", "dx", "dpx"),
                value=boundary_conditions_right,
            ),
            xt.TargetRelPhaseAdvance("mux", mux_ir3),
            xt.TargetRelPhaseAdvance("muy", muy_ir3),
        ],
        vary=[
            xt.VaryList(
                [
                    f"kqt13.l3{bn}",
                    f"kqt12.l3{bn}",
                    f"kqtl11.l3{bn}",
                    f"kqtl10.l3{bn}",
                    f"kqtl9.l3{bn}",
                    f"kqtl8.l3{bn}",
                    f"kqtl7.l3{bn}",
                    f"kq6.l3{bn}",
                    f"kq6.r3{bn}",
                    f"kqtl7.r3{bn}",
                    f"kqtl8.r3{bn}",
                    f"kqtl9.r3{bn}",
                    f"kqtl10.r3{bn}",
                    f"kqtl11.r3{bn}",
                    f"kqt12.r3{bn}",
                    f"kqt13.r3{bn}",
                ]
            )
        ],
    )

    if solve:
        if staged_match:
            opt.disable_targets(tag="stage1")
            opt.solve()
            opt.enable_targets(tag="stage1")
            opt.solve()
        else:
            opt.solve()

    return opt


def rematch_ir4(
    collider,
    line_name,
    boundary_conditions_left,
    boundary_conditions_right,
    mux_ir4,
    muy_ir4,
    alfx_ip4,
    alfy_ip4,
    betx_ip4,
    bety_ip4,
    dx_ip4,
    dpx_ip4,
    solve=True,
    staged_match=False,
    default_tol=None,
):
    assert line_name in ["lhcb1", "lhcb2"]
    bn = line_name[-2:]

    opt = collider[f"lhc{bn}"].match(
        assert_within_tol=False,
        solve=False,
        default_tol=default_tol,
        ele_start=f"s.ds.l4.{bn}",
        ele_stop=f"e.ds.r4.{bn}",
        twiss_init="preserve_start",
        table_for_twiss_init=boundary_conditions_left,
        targets=[
            xt.TargetSet(
                at="ip4",
                alfx=alfx_ip4,
                alfy=alfy_ip4,
                betx=betx_ip4,
                bety=bety_ip4,
                dx=dx_ip4,
                dpx=dpx_ip4,
                tag="stage1",
            ),
            xt.TargetSet(
                at=xt.END,
                tars=("betx", "bety", "alfx", "alfy", "dx", "dpx"),
                value=boundary_conditions_right,
            ),
            xt.TargetRelPhaseAdvance("mux", mux_ir4),
            xt.TargetRelPhaseAdvance("muy", muy_ir4),
        ],
        vary=[
            xt.VaryList(
                [
                    f"kqt13.l4{bn}",
                    f"kqt12.l4{bn}",
                    f"kqtl11.l4{bn}",
                    f"kq10.l4{bn}",
                    f"kq9.l4{bn}",
                    f"kq8.l4{bn}",
                    f"kq7.l4{bn}",
                    f"kq6.l4{bn}",
                    f"kq5.l4{bn}",
                    f"kq5.r4{bn}",
                    f"kq6.r4{bn}",
                    f"kq7.r4{bn}",
                    f"kq8.r4{bn}",
                    f"kq9.r4{bn}",
                    f"kq10.r4{bn}",
                    f"kqtl11.r4{bn}",
                    f"kqt12.r4{bn}",
                    f"kqt13.r4{bn}",
                ]
            )
        ],
    )

    if solve:
        if staged_match:
            opt.disable_targets(tag="stage1")
            opt.solve()
            opt.enable_targets(tag="stage1")
            opt.solve()
        else:
            opt.solve()

    return opt


def rematch_ir6(
    collider,
    line_name,
    boundary_conditions_left,
    boundary_conditions_right,
    mux_ir6,
    muy_ir6,
    alfx_ip6,
    alfy_ip6,
    betx_ip6,
    bety_ip6,
    dx_ip6,
    dpx_ip6,
    solve=True,
    staged_match=False,
    default_tol=None,
):
    assert line_name in ["lhcb1", "lhcb2"]
    bn = line_name[-2:]

    opt = collider[f"lhc{bn}"].match(
        assert_within_tol=False,
        solve=False,
        default_tol=default_tol,
        ele_start=f"s.ds.l6.{bn}",
        ele_stop=f"e.ds.r6.{bn}",
        # Left boundary
        twiss_init="preserve_start",
        table_for_twiss_init=boundary_conditions_left,
        targets=[
            xt.TargetSet(
                at="ip6",
                alfx=alfx_ip6,
                alfy=alfy_ip6,
                betx=betx_ip6,
                bety=bety_ip6,
                dx=dx_ip6,
                dpx=dpx_ip6,
                tag="stage1",
            ),
            xt.TargetSet(
                at=xt.END,
                tars=("betx", "bety", "alfx", "alfy", "dx", "dpx"),
                value=boundary_conditions_right,
            ),
            xt.TargetRelPhaseAdvance("mux", mux_ir6),
            xt.TargetRelPhaseAdvance("muy", muy_ir6),
        ],
        vary=(
            [
                xt.VaryList(
                    [
                        f"kqt13.l6{bn}",
                        f"kqt12.l6{bn}",
                        f"kqtl11.l6{bn}",
                        f"kq10.l6{bn}",
                        f"kq9.l6{bn}",
                        f"kq8.l6{bn}",
                        f"kq5.l6{bn}",
                        f"kq5.r6{bn}",
                        f"kq8.r6{bn}",
                        f"kq9.r6{bn}",
                        f"kq10.r6{bn}",
                        f"kqtl11.r6{bn}",
                        f"kqt12.r6{bn}",
                        f"kqt13.r6{bn}",
                    ]
                )
            ]
            + [xt.Vary((f"kq4.r6{bn}" if bn == "b1" else f"kq4.l6{bn}"))]
        ),
    )

    # No staged match for IR6
    if solve:
        opt.solve()

    return opt


def rematch_ir7(
    collider,
    line_name,
    boundary_conditions_left,
    boundary_conditions_right,
    mux_ir7,
    muy_ir7,
    alfx_ip7,
    alfy_ip7,
    betx_ip7,
    bety_ip7,
    dx_ip7,
    dpx_ip7,
    solve=True,
    staged_match=False,
    default_tol=None,
):
    assert line_name in ["lhcb1", "lhcb2"]
    bn = line_name[-2:]

    opt = collider[f"lhc{bn}"].match(
        assert_within_tol=False,
        solve=False,
        default_tol=default_tol,
        ele_start=f"s.ds.l7.{bn}",
        ele_stop=f"e.ds.r7.{bn}",
        # Left boundary
        twiss_init="preserve_start",
        table_for_twiss_init=boundary_conditions_left,
        targets=[
            xt.TargetSet(
                at="ip7",
                alfx=alfx_ip7,
                alfy=alfy_ip7,
                betx=betx_ip7,
                bety=bety_ip7,
                dx=dx_ip7,
                dpx=dpx_ip7,
                tag="stage1",
            ),
            xt.TargetSet(
                at=xt.END,
                tars=("betx", "bety", "alfx", "alfy", "dx", "dpx"),
                value=boundary_conditions_right,
            ),
            xt.TargetRelPhaseAdvance("mux", mux_ir7),
            xt.TargetRelPhaseAdvance("muy", muy_ir7),
        ],
        vary=[
            xt.VaryList(
                [
                    f"kqt13.l7{bn}",
                    f"kqt12.l7{bn}",
                    f"kqtl11.l7{bn}",
                    f"kqtl10.l7{bn}",
                    f"kqtl9.l7{bn}",
                    f"kqtl8.l7{bn}",
                    f"kqtl7.l7{bn}",
                    f"kq6.l7{bn}",
                    f"kq6.r7{bn}",
                    f"kqtl7.r7{bn}",
                    f"kqtl8.r7{bn}",
                    f"kqtl9.r7{bn}",
                    f"kqtl10.r7{bn}",
                    f"kqtl11.r7{bn}",
                    f"kqt12.r7{bn}",
                    f"kqt13.r7{bn}",
                ]
            )
        ],
    )

    if solve:
        if staged_match:
            opt.disable_targets(tag="stage1")
            opt.solve()
            opt.enable_targets(tag="stage1")
            opt.solve()
        else:
            opt.solve()

    return opt


def rematch_ir8(
    collider,
    line_name,
    boundary_conditions_left,
    boundary_conditions_right,
    mux_ir8,
    muy_ir8,
    alfx_ip8,
    alfy_ip8,
    betx_ip8,
    bety_ip8,
    dx_ip8,
    dpx_ip8,
    solve=True,
    staged_match=False,
    default_tol=None,
):
    assert line_name in ["lhcb1", "lhcb2"]
    bn = line_name[-2:]

    opt = collider[f"lhc{bn}"].match(
        assert_within_tol=False,
        solve=False,
        default_tol=default_tol,
        ele_start=f"s.ds.l8.{bn}",
        ele_stop=f"e.ds.r8.{bn}",
        # Left boundary
        twiss_init="preserve_start",
        table_for_twiss_init=boundary_conditions_left,
        targets=[
            xt.TargetSet(
                at="ip8",
                alfx=alfx_ip8,
                alfy=alfy_ip8,
                betx=betx_ip8,
                bety=bety_ip8,
                dx=dx_ip8,
                dpx=dpx_ip8,
                tag="stage1",
            ),
            xt.TargetSet(
                at=xt.END,
                tars=("betx", "bety", "alfx", "alfy", "dx", "dpx"),
                value=boundary_conditions_right,
                tag="stage2",
            ),
            xt.TargetRelPhaseAdvance("mux", mux_ir8),
            xt.TargetRelPhaseAdvance("muy", muy_ir8),
        ],
        vary=[
            xt.VaryList(
                [
                    f"kq6.l8{bn}",
                    f"kq7.l8{bn}",
                    f"kq8.l8{bn}",
                    f"kq9.l8{bn}",
                    f"kq10.l8{bn}",
                    f"kqtl11.l8{bn}",
                    f"kqt12.l8{bn}",
                    f"kqt13.l8{bn}",
                ]
            ),
            xt.VaryList([f"kq4.l8{bn}", f"kq5.l8{bn}"], tag="stage1"),
            xt.VaryList(
                [
                    f"kq4.r8{bn}",
                    f"kq5.r8{bn}",
                    f"kq6.r8{bn}",
                    f"kq7.r8{bn}",
                    f"kq8.r8{bn}",
                    f"kq9.r8{bn}",
                    f"kq10.r8{bn}",
                    f"kqtl11.r8{bn}",
                    f"kqt12.r8{bn}",
                    f"kqt13.r8{bn}",
                ],
                tag="stage2",
            ),
        ],
    )

    if solve:
        if staged_match:
            opt.disable_targets(tag=["stage1", "stage2"])
            opt.disable_vary(tag=["stage1", "stage2"])
            opt.solve()

            opt.enable_vary(tag="stage1")
            opt.solve()

            opt.enable_targets(tag="stage1")
            opt.enable_targets(tag="stage2")
            opt.enable_vary(tag="stage2")
            opt.solve()
        else:
            opt.solve()

    return opt
