# %%
import os
from pathlib import Path
import numpy as np
import pandas as pd
from cpymad import libmadx
from cpymad.madx import Madx
import matplotlib.pyplot as plt


import xtrack as xt
import xmask as xm
import xpart as xp
import xmask.lhc as xlhc
import xobjects as xo


from misc import *

try:
    get_ipython().run_line_magic("matplotlib", "inline")
except:
    pass


# %%
prefix = Path("../").absolute()

f1 = prefix + "summer_studies/temp_optics/opt_round_1100_1500.madx"

simple_name = f1.split("/")[-1].split(".")[0] + ".json"
simple_name = "collider_from_madx.json"
simple_name = "opt_round_1100_1500_thick.json"

collider0 = xt.Multiline.from_json(simple_name)


collider = xt.Multiline.from_json(simple_name)

# %%
for ln in ["lhcb1", "lhcb2"]:
    for nir in [1, 5]:
        install_marker_ms(collider, ln, nir)
        install_marker_ms(collider0, ln, nir)

collider0.build_trackers()
collider0.vars.load_madx_optics_file(f1)

# collider0.lhcb1.twiss_default["only_markers"] = True
# collider0.lhcb2.twiss_default["only_markers"] = True
collider0.lhcb1.twiss_default["method"] = "4d"
collider0.lhcb2.twiss_default["method"] = "4d"
collider0.lhcb2.twiss_default["reverse"] = True


collider.build_trackers()

collider.vars.load_madx_optics_file(f1)

# collider.lhcb1.twiss_default["only_markers"] = True
# collider.lhcb2.twiss_default["only_markers"] = True

collider.lhcb1.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["reverse"] = True


# %%
tw0 = collider.lhcb1.twiss()
print(tw0[["betx", "bety"], "ip1"])
print(tw0[["betx", "bety"], "ip5"])
# %%

bn = "b1"
line_name = f"lhc{bn}"

betx_ip1 = 1.1
bety_ip1 = 1.1
betx_ip5 = 1.1
bety_ip5 = 1.1

# %%

print(collider0.lhcb1.twiss()[:, "ip1"])
print(collider.lhcb1.twiss()[:, "ip1"])

# %%
tw = collider0.lhcb1.twiss()

# pos1 = collider["lhcb1"].varval["l.ms"] * 0.5 + tw["s", "ms.14l5.b1"]
# pos2 = collider["lhcb1"].varval["l.ms"] * 0.5 + tw["s", "ms.15l5.b1"]
# pos4 = collider["lhcb1"].varval["l.ms"] * 0.5 - tw["s", "ms.15r5.b1"]
# pos3 = collider["lhcb1"].varval["l.ms"] * 0.5 - tw["s", "ms.14r5.b1"]
# tw[:, pos1:pos1:"s"]

# %%
opt = {}
tw_sq_a45_ip5_a56_b1 = selectIR15(
    collider, line_name="lhcb1", NIR=5, SEC1=45, SEC2=56, install_markers=False
)
tw_sq_a45_ip5_a56_b2 = selectIR15(
    collider, line_name="lhcb2", NIR=5, SEC1=45, SEC2=56, install_markers=False
)
# %%
tw45_56_xt = lm.get_arc_periodic_solution(collider, arc_name=["45", "56"])

# %%
# opt["opt_b1"] = rematch_ir15_b1(
#     collider, betx_ip1, bety_ip1, tw_sq_a45_ip5_a56=tw_sq_a45_ip5_a56_b1
# )
opt["opt_b1"] = rematch_ir15_b1(
    collider, betx_ip1, bety_ip1, tw_sq_a45_ip5_a56=tw45_56_xt["lhcb1"]
)
# %%
# opt["opt_b2"] = rematch_ir15_b2(
#     collider, betx_ip1, bety_ip1, tw_sq_a45_ip5_a56=tw_sq_a45_ip5_a56_b2, restore=False
# )
opt["opt_b2"] = rematch_ir15_b2(
    collider, betx_ip1, bety_ip1, tw_sq_a45_ip5_a56=tw45_56_xt["lhcb2"], restore=False
)

# %%
print("Beam 1")
tars = opt["opt_b1"].targets
tt = opt["opt_b1"].log().rows[0].targets

for i in range(len(tars)):
    print(tars[i])
    # print(tars[i].tar, tt[0][i])
print("Beam 2")
tars = opt["opt_b2"].targets
tt = opt["opt_b2"].log().rows[0].targets

for i in range(len(tars)):
    print(tars[i])

# %%

print(opt["opt_b1"].show())
print(opt["opt_b1"].log())

print(opt["opt_b2"].show())
print(opt["opt_b2"].log())

# %%

print(
    collider.lhcb1.twiss(method="4d")[
        ["betx", "alfx", "mux", "bety", "alfy", "muy", "dx", "dpx"], ["ip1", "ip5"]
    ]
)

# %%

print("Before")
print(" Beam 1")
print(collider0.lhcb1.twiss(method="4d")[cols, ["ip1", "ip5"]])
print(" Beam 2")
print(collider0.lhcb2.twiss(method="4d")[cols, ["ip1", "ip5"]])

print("After")
print(" Beam 1")
print(collider.lhcb1.twiss(method="4d")[cols, ["ip1", "ip5"]])
print(" Beam 2")
print(collider.lhcb2.twiss(method="4d")[cols, ["ip1", "ip5"]])
# %%
print(collider.varval["kq4.l5b1"])
print(collider0.varval["kq4.l5b1"])
print(collider.varval["kq4.l5b2"])
print(collider0.varval["kq4.l5b2"])
# %%


for kk in strengths_to_check:
    print(f"{kk}: Before {collider0.varval[kk]}, After: {collider.varval[kk]}")

# %%
tw_before = collider0.lhcb1.twiss(method="4d")
tw_after = collider.lhcb1.twiss(method="4d")
# %%


fig, axs = plt.subplots(figsize=(11, 11))
axs.plot(tw_before.s, tw_before.betx, label="Before")
axs.plot(tw_after.s, tw_after.betx, label="After")
axs.legend()
plt.show()

fig, axs = plt.subplots(figsize=(11, 11))
axs.plot(tw_before.s, tw_before.betx - tw_after.betx)
axs.set_yscale("log")
plt.show()
# %%
rra = "s.ds.l5.b1"
rrb = "e.ds.r5.b1"


fig, axs = plt.subplots(figsize=(11, 11))
axs.plot(tw_before["s", rra:rrb], tw_before["betx", rra:rrb], label="Before", ls="-")
axs.plot(tw_after["s", rra:rrb], tw_after["betx", rra:rrb], label="After", ls="--")
axs.plot(
    tw_before["s", rra:rrb],
    tw_before["bety", rra:rrb],
    label="Before y",
    ls="-",
    lw=0.5,
)
axs.plot(tw_after["s", rra:rrb], tw_after["bety", rra:rrb], label="After y", ls="--")
axs.legend()
# axs.set_yscale("log")
plt.show()

fig, axs = plt.subplots(figsize=(11, 11))
axs.plot(
    tw_before["s", rra:rrb],
    tw_before["betx", rra:rrb] - tw_after["betx", rra:rrb],
)

plt.show()
# %%
ang = 250

collider.varval["on_disp"] = 0
collider.varval["on_x1"] = ang
collider.varval["on_x5"] = ang
collider0.varval["on_disp"] = 0
collider0.varval["on_x1"] = ang
collider0.varval["on_x5"] = ang


# %%
var = "x"
tw_before = collider0.lhcb1.cycle("ip3").twiss(method="4d")
tw_after = collider.lhcb1.cycle("ip3").twiss(method="4d")

fig, axs = plt.subplots(figsize=(11, 11))
axs.plot(tw_before.s, tw_before[var], label="Before")
axs.plot(tw_after.s, tw_after[var], label="After")
axs.legend()
plt.show()

fig, axs = plt.subplots(figsize=(11, 11))
axs.plot(tw_before["s", rra:rrb], tw_before[var, rra:rrb], label="Before", ls="-")
axs.plot(tw_after["s", rra:rrb], tw_after[var, rra:rrb], label="After", ls="--")
axs.legend()
plt.show()

fig, axs = plt.subplots(figsize=(11, 11))
axs.plot(
    tw_before["s", rra:rrb],
    tw_before[var, rra:rrb] - tw_after[var, rra:rrb],
)

plt.show()

# %%
tw_before_b2 = collider0.lhcb2.cycle("ip3").twiss(method="4d")
tw_after_b2 = collider.lhcb2.cycle("ip3").twiss(method="4d")
# %%
rra = "s.ds.r1.b2"
rrb = "e.ds.l1.b2"

var = "betx"
var2 = "bety"
fig, axs = plt.subplots(figsize=(11, 11))
axs.plot(
    tw_before_b2["s", rra:rrb],
    tw_before_b2[var, rra:rrb],
    label="Before x",
    ls="-",
    lw=0.5,
)
axs.plot(tw_after_b2["s", rra:rrb], tw_after_b2[var, rra:rrb], label="After x", ls="--")

axs.plot(
    tw_before_b2["s", rra:rrb],
    tw_before_b2[var2, rra:rrb],
    label="Before y",
    ls="-",
    lw=0.5,
)
axs.plot(
    tw_after_b2["s", rra:rrb], tw_after_b2[var2, rra:rrb], label="After y", ls="--"
)
axs.legend()
plt.show()

fig, axs = plt.subplots(figsize=(11, 11))
axs.plot(
    tw_before_b2["s", rra:rrb],
    tw_before_b2["bety", rra:rrb] - tw_after_b2["bety", rra:rrb],
)

plt.show()

# %%
# collider.varval["on_disp"] = 0
# collider.varval["on_x1"] = 0
# collider.varval["on_x5"] = 0
# lm.gen_madx_optics_file_auto(collider, "opt_round_1000_1500_xs.madx")

# %%
collider0.varval["on_disp"] = 0
collider0.varval["on_x1"] = 0
collider0.varval["on_x5"] = 0
# %%
s3b1 = select(collider0, "lhcb1", 3, 23, 34)
print(s3b1["bir"][["betx", "alfx", "mux", "bety", "alfy", "muy", "dx", "dpx"]])
print(s3b1["eir"][["betx", "alfx", "mux", "bety", "alfy", "muy", "dx", "dpx"]])
# %%
s3b2 = select(collider0, "lhcb2", 3, 23, 34)
print(s3b2["bir"][["betx", "alfx", "mux", "bety", "alfy", "muy", "dx", "dpx"]])
print(s3b2["eir"][["betx", "alfx", "mux", "bety", "alfy", "muy", "dx", "dpx"]])

# %%
