# %%
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt

import scienceplots

plt.style.use(["notebook", "science"])
plt.rcParams["legend.frameon"] = True
plt.rcParams["legend.title_fontsize"] = 25
plt.rcParams["axes.labelweight"] = "bold"
plt.rcParams["axes.labelsize"] = 25
plt.rcParams["text.usetex"] = False
plt.rcParams["xtick.labelsize"] = 24
plt.rcParams["ytick.labelsize"] = 24
plt.rcParams["legend.framealpha"] = 1
plt.rcParams["axes.titlesize"] = 26
plt.rcParams["axes.titleweight"] = "bold"

import xtrack as xt
from misc import *

try:
    # get_ipython().run_line_magic("matplotlib", "inline")
    get_ipython().run_line_magic("matplotlib", "qt6")
except:
    pass


get_ipython().run_line_magic("autoreload", "2")
# %%


# def print_names():
#     for qq in quads_ir15_knob_names.split():
#         print(qq, collider_xtr["lhcb1"].vars[qq]._find_dependant_targets())


# %%


def get_presqueeze(collider, line_name):
    bim = line_name[-2:]
    tw81_45_xt = lm.get_arc_periodic_solution(collider, arc_name=["81", "45"])

    beta0 = tw81_45_xt[line_name]["45"].get_twiss_init(f"s.ds.l5.{bim}")
    beta0.mux = 0
    beta0.muy = 0
    tw_ip5_pre = collider[line_name].twiss(
        ele_start="s.ds.l5.b1", ele_stop=f"e.ds.r5.{bim}", twiss_init=beta0
    )

    beta0 = tw81_45_xt[line_name]["81"].get_twiss_init(f"s.ds.l1.{bim}")
    beta0.mux = 0
    beta0.muy = 0
    tw_ip1_pre = collider[line_name].twiss(
        ele_start=f"s.ds.l1.{bim}", ele_stop=f"e.ds.r1.{bim}", twiss_init=beta0
    )

    # return xt.TwissTable.concatenate([tw_ip5_pre, tw_ip1_pre])
    res = {"ir1": tw_ip1_pre, "ir5": tw_ip5_pre}
    return res


def get_collider(collider_path, optics_name):
    collider = xt.Multiline.from_json(collider_path)
    collider.build_trackers()
    collider.vars.load_madx_optics_file(optics_name)

    collider.lhcb1.cycle("ip3", inplace=True)
    collider.lhcb2.cycle("ip3", inplace=True)

    collider.lhcb1.twiss_default["only_markers"] = True
    collider.lhcb2.twiss_default["only_markers"] = True

    collider.lhcb1.twiss_default["method"] = "4d"
    collider.lhcb2.twiss_default["method"] = "4d"
    collider.lhcb2.twiss_default["reverse"] = True

    return collider


# %%
prefix = Path("../").absolute()

optics_madx = prefix / "summer_studies/collapse/opt_round_1100_bets0_1000.madx"

optics_madx = [
    "madx_vs_xsuite_rematch/madx/opt_collapse_flathv_700_2800_asym_700_700.madx",
    "madx_vs_xsuite_rematch/madx/opt_collapse_flathv_700_2800_asym_700_1000.madx",
    "madx_vs_xsuite_rematch/madx/opt_collapse_flathv_700_2800_asym_700_1200.madx",
    "madx_vs_xsuite_rematch/madx/opt_collapse_flathv_700_2800_asym_700_1400.madx",
    "madx_vs_xsuite_rematch/madx/opt_collapse_flathv_700_2800_asym_700_1600.madx",
    "madx_vs_xsuite_rematch/madx/opt_collapse_flathv_700_2800_asym_700_1800.madx",
    "madx_vs_xsuite_rematch/madx/opt_collapse_flathv_700_2800_asym_700_2000.madx",
    "madx_vs_xsuite_rematch/madx/opt_collapse_flathv_700_2800_asym_700_2200.madx",
]

# optics_xtrack = "./opt_round_1100_xsuite_test2.madx"
# optics_xtrack = "optics_xsuite/opt_round_1100_ats_700.madx"
optics_xtrack = "optics_xsuite/opt_round_1100_ats_600.madx"
optics_xtrack = [
    "madx_vs_xsuite_rematch/xsuite/opt_flathv_700_2800_bet0_0.7_from_madx.madx",
    "madx_vs_xsuite_rematch/xsuite/opt_flathv_700_2800_asym_700_1000.madx",
    "madx_vs_xsuite_rematch/xsuite/opt_flathv_700_2800_asym_700_1200.madx",
    "madx_vs_xsuite_rematch/xsuite/opt_flathv_700_2800_asym_700_1400.madx",
    "madx_vs_xsuite_rematch/xsuite/opt_flathv_700_2800_asym_700_1600.madx",
    "madx_vs_xsuite_rematch/xsuite/opt_flathv_700_2800_asym_700_1800.madx",
    "madx_vs_xsuite_rematch/xsuite/opt_flathv_700_2800_asym_700_2000.madx",
]

# optics_xtrack = [prefix/ 'summer_studies/collapse/opt_collapse_1000_1500_no_triplet_match.madx']
optics_xtrack = [prefix / "acc-models-lhc/strengths/round/opt_round_150_1500.madx"]
# optics_madx = [prefix / "acc-models-lhc/strengths/round/opt_round_380_1500.madx"]
# optics_xtrack = ["madx_vs_xsuite_rematch/madx/opt_round_1100_1500_meh.madx"]

# optics_madx = [prefix / "summer_studies/collapse/opt_collapse_1100_1500.madx"]

# optics_madx = [prefix / "acc-models-lhc/strengths/ramp/opt_ramp_500_1500.madx"]


optics_xtrack = [
    "../madx_vs_xsuite_rematch/madx/opt_round_1100_cc_angle_optim_again.madx"
]

# optics_madx = ["../madx_vs_xsuite_rematch/madx/opt_round_1000_cc_angle_optim_all.madx"]


optics_madx = [prefix / "summer_studies/collapse/opt_collapse_1100_1500.madx"]
# optics_xtrack = ["/home/iangelis/Desktop/broken_optics.madx"]


collider_name = "collider_hl16.json"
# %%
optics_id = 0
# %%
collider_madx = get_collider(collider_name, optics_madx[optics_id])
# %%
collider_xtr = get_collider(collider_name, optics_xtrack[optics_id])
# %%
collider_madx.vars["on_disp"] = 0
collider_xtr.vars["on_disp"] = 0
collider_madx.vars["on_x1"] = 0
collider_madx.vars["on_x5"] = 0
collider_xtr.vars["on_x1"] = 0
collider_xtr.vars["on_x5"] = 0
# enable_crossing_all(collider_madx)
# enable_crossing_all(collider_xtr)
# disable_crossing_all(collider_madx)
# disable_crossing_all(collider_xtr)

collider_madx.vars["on_crab1"] = 0
collider_madx.vars["on_crab5"] = 0
collider_madx.vars["z_crab"] = 0

collider_xtr.vars["on_crab1"] = 0
collider_xtr.vars["on_crab5"] = 0
collider_xtr.vars["z_crab"] = 0


tw_madx = collider_madx.twiss(method="4d")
tw_xtr = collider_xtr.twiss(method="4d")

print(tw_madx["lhcb1"][["name", "betx", "bety"], "ip.*"])
print(tw_xtr["lhcb1"][["name", "betx", "bety"], "ip.*"])

# %%
plot_orbit(tw_madx, "lhcb1")
plot_orbit(tw_xtr, "lhcb1")
# %%

madx_bet0 = get_presqueeze(collider_madx, "lhcb1")
xtr_bet0 = get_presqueeze(collider_xtr, "lhcb1")

# %%
for ip in ["ir1", "ir5"]:
    print(madx_bet0[ip][["name", "betx", "bety"], "ip.*"])
    print(xtr_bet0[ip][["name", "betx", "bety"], "ip.*"])


# %%
def plot_betas(
    collider, line_name, range_low=None, range_high=None, plot_lattice=False, ir="5"
):
    twiss_table = collider[line_name].twiss()
    fig, axs = plt.subplots(figsize=(11, 11))
    axs.plot(
        twiss_table["s", range_low:range_high],
        twiss_table["betx", range_low:range_high],
        label=r"$\beta_x$",
        ls="-",
        color="black",
    )
    axs.plot(
        twiss_table["s", range_low:range_high],
        twiss_table["bety", range_low:range_high],
        label=r"$\beta_y$",
        ls="-",
        color="red",
    )
    if plot_lattice:
        axs_t2 = axs.twinx()
        axs_t2.set_yticks([])
        ss, ll, kk = plot_ir15_quads(collider, line_name, ir=ir)
        for iss, ill, ikk in zip(ss, ll, kk):
            plotLatticeSeries(
                axs_t2, iss[0] + ill * 0.5, ill, ikk, ikk / 2, color="cyan", alpha=0.5
            )
        axs_t2.set_ylim(-0.01, 0.01)

    # axs.axvline(twiss_table["s", "mqxfb.a2r5_exit"], linestyle="--")
    # axs.axvline(twiss_table["s", "mqxfb.b2r5_entry"], linestyle="--")
    # axs.axvline(twiss_table["s", "mqxfa.b3r5_exit"], linestyle="--")

    axs.axvline(twiss_table["s", f"acfca.4ar{ir}.b1_exit"], linestyle="--")
    axs.axvline(twiss_table["s", f"acfca.4al{ir}.b1_exit"], linestyle="--")

    axs.set_xlabel("s [m]")
    axs.set_ylabel(r"$\beta_{x,y}$ [m]")
    axs.legend(loc=1)
    axs.grid()
    print(
        twiss_table["bety", "mqxfb.b2r5_entry"] / twiss_table["betx", "mqxfa.b3r5_exit"]
    )
    print(twiss_table.rows[f"acfca.4ar{ir}.b1_exit"].cols["betx bety bety/betx"])
    plt.show()


# %%
plot_betas(collider_madx, "lhcb1", "s.ds.l5.b1", "e.ds.r5.b1")
plot_betas(collider_xtr, "lhcb1", "s.ds.l5.b1", "e.ds.r5.b1")

# print(tw_madx.lhcb1.rows["mqxfb.a2r5_exit"].cols['s betx bety'])
# print(tw_madx.lhcb1.rows["mqxfa.b3r5_exit"].cols['s betx bety'])

# print(tw_madx.lhcb1.rows["mqxfb.b2r5_entry"].cols['s betx bety'])
# print(tw_madx.lhcb1.rows["mqxfb.b2r5_exit"].cols['s betx bety'])
# print(tw_madx.lhcb1.rows["mqxfa.a3r5_entry"].cols['s betx bety'])
# print(tw_madx.lhcb1.rows["mqxfa.a3r5_exit"].cols['s betx bety'])
# print(tw_madx.lhcb1.rows["mqxfa.b3r5_entry"].cols['s betx bety'])

# %%

fig, axs = plt.subplots(figsize=(11, 11))
axs.plot(
    madx_bet0["ir5"]["s", :],
    madx_bet0["ir5"]["betx", :],
    label=r"$\beta_x$, Good xing",
    color="black",
)
axs.plot(
    madx_bet0["ir5"]["s", :],
    madx_bet0["ir5"]["bety", :],
    label=r"$\beta_y$, Good xing",
    color="red",
)
axs.plot(
    xtr_bet0["ir5"]["s", :],
    xtr_bet0["ir5"]["betx", :],
    label=r"$\beta_x$, Bad xing",
    ls="--",
    color="black",
)
axs.plot(
    xtr_bet0["ir5"]["s", :],
    xtr_bet0["ir5"]["bety", :],
    label=r"$\beta_y$, Bad xing",
    ls="--",
    color="red",
)
# axs_t2 = axs.twinx()
# axs_t2.set_yticks([])
# ss, ll, kk = plot_ir15_quads(collider_xtr, "lhcb1", ir="5")
# for iss, ill, ikk in zip(ss, ll, kk):
#     plotLatticeSeries(
#         axs_t2, iss[0] + ill * 0.5, ill, ikk, ikk / 2, color="cyan", alpha=0.5
#     )
# axs_t2.set_ylim(-0.01, 0.01)

axs.set_xlabel("s [m]")
axs.set_ylabel(r"$\beta_{x,y}$ [m]")
axs.legend(loc=1)
axs.grid()
plt.show()

# %%
rra = "s.ds.l5.b1"
rrb = "e.ds.r5.b1"

# rra = "s.ds.l1.b1"
# rrb = "e.ds.r1.b1"


# rra = "s.ds.r4.b1"
# rrb = "e.ds.l6.b1"

# rra = None
# rrb = None

fig, axs = plt.subplots(figsize=(11, 11))
axs.plot(
    tw_madx["lhcb1"]["s", rra:rrb],
    tw_madx["lhcb1"]["betx", rra:rrb],
    label=r"$\beta_x$, Bad CC angle",
    color="black",
)
axs.plot(
    tw_madx["lhcb1"]["s", rra:rrb],
    tw_madx["lhcb1"]["bety", rra:rrb],
    label=r"$\beta_y$, Bad CC angle",
    color="red",
)
axs.plot(
    tw_xtr["lhcb1"]["s", rra:rrb],
    tw_xtr["lhcb1"]["betx", rra:rrb],
    label=r"$\beta_x$, Good CC angle",
    ls="--",
    color="black",
)
axs.plot(
    tw_xtr["lhcb1"]["s", rra:rrb],
    tw_xtr["lhcb1"]["bety", rra:rrb],
    label=r"$\beta_y$, Good CC angle",
    ls="--",
    color="red",
)
# axs_tx = axs.twinx()
# axs_tx.plot(
#     tw_madx["s", rra:rrb],
#     tw_madx["dx", rra:rrb],
#     label=r"$dx$, MAD-X",
#     color="blue",
# )
# axs_tx.plot(
#     tw_madx["s", rra:rrb],
#     tw_madx["dy", rra:rrb],
#     label=r"$dy$, MAD-X",
#     color="green",
# )
# axs_tx.plot(
#     tw_xtr["s", rra:rrb],
#     tw_xtr["dx", rra:rrb],
#     label=r"$dx$, Xsuite",
#     ls="--",
#     color="blue",
# )
# axs_tx.plot(
#     tw_xtr["s", rra:rrb],
#     tw_xtr["dy", rra:rrb],
#     label=r"$dy$, Xsuite",
#     ls="--",
#     color="green",
# )
# axs_tx.set_ylabel(f"$D [m]$")
# axs_tx.set_ylim(-np.max(tw_xtr["dx", rra:rrb]), np.max(tw_xtr["dx", rra:rrb]))
# axs_tx.legend(loc=2)

axs_t = axs.twiny()
axs_t.set_xticks(
    tw_xtr["lhcb1"][["s"], "ip.*"],
    tw_xtr["lhcb1"][["name"], "ip.*"],
)
axs_t.set_xlim(axs.get_xlim())
# [axs_t.axvline(xpos, linestyle="--") for xpos in tw_xtr["lhcb1"][["s"], "ip.*"]]
axs.axvline(tw_xtr["lhcb1"]["s", "acfca.4ar5.b1_exit"], linestyle="--")
axs.axvline(tw_xtr["lhcb1"]["s", "acfca.4al5.b1_exit"], linestyle="--")

axs_t2 = axs.twinx()
ss, ll, kk = plot_ir15_quads(collider_xtr, "lhcb1", ir="5")
for iss, ill, ikk in zip(ss, ll, kk):
    plotLatticeSeries(
        axs_t2, iss[0] + ill * 0.5, ill, ikk, ikk / 2, color="grey", alpha=0.5
    )
axs_t2.set_yticks([])
axs_t2.set_ylim(-0.01, 0.01)

axs.set_xlabel("s [m]")
axs.set_ylabel(r"$\beta_{x,y}$ [m]")
axs.legend(loc=1)
axs.grid(axis='both')
plt.show()

# %%
rra = None
rrb = None
fig, axs = plt.subplots(figsize=(11, 11))
axs.plot(
    tw_madx["lhcb1"]["s", rra:rrb],
    (tw_madx["lhcb1"]["betx", rra:rrb] - tw_xtr["lhcb1"]["betx", rra:rrb])
    / tw_madx["lhcb1"]["betx", rra:rrb],
    label=r"$\beta_x$, MAD-X - Xsuite",
    color="black",
)
axs.plot(
    tw_madx["lhcb1"]["s", rra:rrb],
    (tw_madx["lhcb1"]["bety", rra:rrb] - tw_xtr["lhcb1"]["bety", rra:rrb])
    / tw_madx["lhcb1"]["bety", rra:rrb],
    label=r"$\beta_y$, MAD-X - Xsuite",
    color="red",
)

axs_t = axs.twiny()
axs_t.set_xticks(
    tw_xtr["lhcb1"][["s"], "ip.*"],
    tw_xtr["lhcb1"][["name"], "ip.*"],
)
axs_t.set_xlim(axs.get_xlim())
[axs_t.axvline(xpos, linestyle="--") for xpos in tw_xtr["lhcb1"][["s"], "ip.*"]]
axs.set_xlabel("s [m]")
axs.set_ylabel(r"$\Delta\beta_{x,y}/ \beta_{x,y}$ [m] ((MADX-Xsuite)/MADX)")
axs.set_ylabel(r"$\beta_{x,y}^{MADX} -\beta_{x,y}^{Xsuite} )/ \beta_{x,y}^{MADX}$ [m]")
axs.legend()
plt.show()

# %%
for quad_name in quads_ir15_defaults.keys():
    print(
        f"{quad_name}: 0.5:{collider_madx.varval[quad_name]},  1.1:{collider_xtr.varval[quad_name]}"
    )
# %%
z_crab = 0.075
collider_madx.vars["on_crab1"] = 190
collider_madx.vars["on_crab5"] = 190
collider_madx.vars["z_crab"] = z_crab

collider_xtr.vars["on_crab1"] = 190
collider_xtr.vars["on_crab5"] = 190
collider_xtr.vars["z_crab"] = z_crab

cc_knobs = (
    "on_ccpl1h on_ccml1h on_ccpl1v on_ccml1v "
    "on_ccpr1h on_ccmr1h on_ccpr1v on_ccmr1v"
    "on_ccsl1hb1 on_ccsl1vb1 on_ccsr1hb1 on_ccsr1vb1"
)

# for cck in cc_knobs.split():
#     # print(cck)
#     collider_madx.vars[cck] = 0
#     cck.replace("l1", "l5").replace("r1", "r5")
#     collider_madx.vars[cck] = 0

# collider_madx.vars["on_crab1"] = 0
# collider_madx.vars["on_crab5"] = 0
# collider_madx.vars["z_crab"] = 0.0

# collider_xtr.vars["on_crab1"] = 0
# collider_xtr.vars["on_crab5"] = 0
# collider_xtr.vars["z_crab"] = 0.0

tw_madx = collider_madx.twiss(method="4d")
tw_xtr = collider_xtr.twiss(method="4d")

# %%
# Plot Orbits
fig, axs = plt.subplots(figsize=(21, 15))
axs.plot(
    tw_xtr["lhcb1"]["s", :],
    tw_xtr["lhcb1"]["x", :] * 1e6,
    color="black",
    label=r"$x$",
)
axs.plot(
    tw_xtr["lhcb1"]["s", :], tw_xtr["lhcb1"]["y", :] * 1e6, color="red", label=r"$y$"
)

axs_t = axs.twiny()
axs_t.set_xticks(
    tw_xtr["lhcb1"][["s"], "ip.*"],
    tw_xtr["lhcb1"][["name"], "ip.*"],
)
axs_t.set_xlim(axs.get_xlim())
axs.set_xlabel("s [m]")
[axs_t.axvline(xpos, linestyle="--") for xpos in tw_xtr["lhcb1"][["s"], "ip.*"]]

# axs.set_ylim(-80, 80)
axs.legend()
axs.set_xlabel(r"$s [m]$")
axs.set_ylabel(r"$co [\mu m]$")
plt.show()

fig, axs = plt.subplots(figsize=(21, 15))
axs.plot(
    tw_xtr["lhcb1"]["s", :],
    tw_xtr["lhcb1"]["x", :] / np.sqrt(tw_xtr["lhcb1"]["betx", :] * 2.3 * 1e-6),
    color="black",
    label=r"$x$",
)
axs.plot(
    tw_xtr["lhcb1"]["s", :],
    tw_xtr["lhcb1"]["y", :] / np.sqrt(tw_xtr["lhcb1"]["bety", :] * 2.3 * 1e-6),
    color="red",
    label=r"$y$",
)
axs_t = axs.twiny()
axs_t.set_xticks(
    tw_xtr["lhcb1"][["s"], "ip.*"],
    tw_xtr["lhcb1"][["name"], "ip.*"],
)
axs_t.set_xlim(axs.get_xlim())
axs.set_xlabel("s [m]")
[axs_t.axvline(xpos, linestyle="--") for xpos in tw_xtr["lhcb1"][["s"], "ip.*"]]

axs.legend()
axs.set_xlabel(r"$s [m]$")
axs.set_ylabel(r"$\text{Orbit} / \sqrt{\epsilon \beta} $")
plt.show()
# %%
# rra = "ip4"
# rrb = "ip6"
# Plot Orbits
fig, axs = plt.subplots(figsize=(21, 15))
axs.plot(
    tw_madx["lhcb1"]["s", :],
    tw_madx["lhcb1"]["x", :] * 1e6,
    color="black",
    label=r"$x$",
)
axs.plot(
    tw_madx["lhcb1"]["s", :],
    tw_madx["lhcb1"]["y", :] * 1e6,
    color="red",
    label=r"$y$",
)
# axs_t = axs.twinx()
# axs_t.plot(
#     tw_madx["lhcb1"]["s", :],
#     tw_madx["lhcb1"]["betx", :],
#     color="black",
#     ls="--",
#     label=r"$\beta_x$",
# )
# axs_t.plot(
#     tw_madx["lhcb1"]["s", :],
#     tw_madx["lhcb1"]["bety", :],
#     color="red",
#     ls="--",
#     label=r"$\beta_y$",
# )
# axs_t.set_ylim(-3000, 3000)

axs_t = axs.twiny()
axs_t.set_xticks(
    tw_madx["lhcb1"][["s"], "ip.*"],
    tw_madx["lhcb1"][["name"], "ip.*"],
)
axs_t.set_xlim(axs.get_xlim())
axs.set_xlabel("s [m]")
[axs_t.axvline(xpos, linestyle="--") for xpos in tw_madx["lhcb1"][["s"], "ip.*"]]

axs.set_ylim(-80, 80)
axs.legend()
axs.set_xlabel(r"$s [m]$")
axs.set_ylabel(r"$co [\mu m]$")
plt.show()
# %%

fig, axs = plt.subplots(figsize=(21, 15))
axs.plot(
    tw_madx["lhcb1"]["s", :],
    tw_madx["lhcb1"]["x", :] / np.sqrt(tw_madx["lhcb1"]["betx", :] * 2.3 * 1e-6),
    color="black",
    label=r"$x$",
)
axs.plot(
    tw_madx["lhcb1"]["s", :],
    tw_madx["lhcb1"]["y", :] / np.sqrt(tw_madx["lhcb1"]["bety", :] * 2.3 * 1e-6),
    color="red",
    label=r"$y$",
)
axs_t = axs.twiny()
axs_t.set_xticks(
    tw_madx["lhcb1"][["s"], "ip.*"],
    tw_madx["lhcb1"][["name"], "ip.*"],
)
axs_t.set_xlim(axs.get_xlim())
axs.set_xlabel("s [m]")
# [axs_t.axvline(xpos, linestyle="--") for xpos in tw_madx["lhcb1"][["s"], "ip.*"]]

axs.legend()
axs.set_xlabel(r"$s [m]$")
axs.set_ylabel(r"$\text{Orbit} / \sqrt{\epsilon \beta} $")
axs.grid()
plt.show()


# %%
# Compare chromatic functions
fig, axs = plt.subplots(2, 1, figsize=(21, 15))
axs[0].plot(
    tw_madx["lhcb1"]["s", :],
    tw_madx["lhcb1"]["wx_chrom", :],
    color="black",
    label=r"$W_x$",
)
axs[1].plot(
    tw_madx["lhcb1"]["s", :],
    tw_madx["lhcb1"]["wy_chrom", :],
    color="red",
    label=r"$W_y$",
)
axs[0].plot(
    tw_xtr["lhcb1"]["s", :],
    tw_xtr["lhcb1"]["wx_chrom", :],
    ls="--",
    color="blue",
    label=r"$W_x$",
)
axs[1].plot(
    tw_xtr["lhcb1"]["s", :],
    tw_xtr["lhcb1"]["wy_chrom", :],
    ls="--",
    color="green",
    label=r"$W_y$",
)

for i in range(2):
    axs_t = axs[i].twiny()
    axs_t.set_xticks(
        tw_madx["lhcb1"][["s"], "ip.*"],
        tw_madx["lhcb1"][["name"], "ip.*"],
    )
    axs_t.set_xlim(axs[i].get_xlim())
    axs[i].set_xlabel("s [m]")
    [axs_t.axvline(xpos, linestyle="--") for xpos in tw_madx["lhcb1"][["s"], "ip.*"]]

    axs[i].legend()
    axs[i].set_xlabel(r"$s [m]$")
axs[0].set_ylabel(r"$W_{x}$")
axs[1].set_ylabel(r"$W_{y}$")
plt.show()

# %%
fig, axs = plt.subplots(2, 1, figsize=(21, 15))
axs[0].plot(
    tw_madx["lhcb1"]["s", :],
    tw_madx["lhcb1"]["ax_chrom", :],
    color="black",
    label=r"$A_x$",
)
axs[1].plot(
    tw_madx["lhcb1"]["s", :],
    tw_madx["lhcb1"]["ay_chrom", :],
    color="red",
    label=r"$A_y$",
)
axs[0].plot(
    tw_xtr["lhcb1"]["s", :],
    tw_xtr["lhcb1"]["ax_chrom", :],
    ls="--",
    color="blue",
    label=r"$A_x$",
)
axs[1].plot(
    tw_xtr["lhcb1"]["s", :],
    tw_xtr["lhcb1"]["ay_chrom", :],
    ls="--",
    color="green",
    label=r"$A_y$",
)

for i in range(2):
    axs_t = axs[i].twiny()
    axs_t.set_xticks(
        tw_madx["lhcb1"][["s"], "ip.*"],
        tw_madx["lhcb1"][["name"], "ip.*"],
    )
    axs_t.set_xlim(axs[i].get_xlim())
    axs[i].set_xlabel("s [m]")
    [axs_t.axvline(xpos, linestyle="--") for xpos in tw_madx["lhcb1"][["s"], "ip.*"]]

    axs[i].legend()
    axs[i].set_xlabel(r"$s [m]$")
axs[0].set_ylabel(r"$A_{x}$")
axs[1].set_ylabel(r"$A_{y}$")
plt.show()
# %%
fig, axs = plt.subplots(2, 1, figsize=(21, 15))
axs[0].plot(
    tw_madx["lhcb1"]["s", :],
    tw_madx["lhcb1"]["bx_chrom", :],
    color="black",
    label=r"$B_x$",
)
axs[1].plot(
    tw_madx["lhcb1"]["s", :],
    tw_madx["lhcb1"]["by_chrom", :],
    color="red",
    label=r"$B_y$",
)
axs[0].plot(
    tw_xtr["lhcb1"]["s", :],
    tw_xtr["lhcb1"]["bx_chrom", :],
    ls="--",
    color="blue",
    label=r"$B_x$",
)
axs[1].plot(
    tw_xtr["lhcb1"]["s", :],
    tw_xtr["lhcb1"]["by_chrom", :],
    ls="--",
    color="green",
    label=r"$B_y$",
)

for i in range(2):
    axs_t = axs[i].twiny()
    axs_t.set_xticks(
        tw_madx["lhcb1"][["s"], "ip.*"],
        tw_madx["lhcb1"][["name"], "ip.*"],
    )
    axs_t.set_xlim(axs[i].get_xlim())
    axs[i].set_xlabel("s [m]")
    [axs_t.axvline(xpos, linestyle="--") for xpos in tw_madx["lhcb1"][["s"], "ip.*"]]

    axs[i].legend()
    axs[i].set_xlabel(r"$s [m]$")
axs[0].set_ylabel(r"$B_{x}$")
axs[1].set_ylabel(r"$B_{y}$")
plt.show()

# %%
fig, axs = plt.subplots(2, 1, figsize=(21, 15))
axs[0].plot(
    tw_madx["s", :], tw_madx["dmux", :], color="black", label=r"$\Delta \mu_{x}$"
)
axs[1].plot(tw_madx["s", :], tw_madx["dmuy", :], color="red", label=r"$\Delta \mu_{y}$")
axs[0].plot(
    tw_xtr["s", :], tw_xtr["dmux", :], ls="--", color="blue", label=r"$\Delta \mu_{x}$"
)
axs[1].plot(
    tw_xtr["s", :], tw_xtr["dmuy", :], ls="--", color="green", label=r"$\Delta \mu_{y}$"
)

for i in range(2):
    axs_t = axs[i].twiny()
    axs_t.set_xticks(
        tw_madx[["s"], "ip.*"],
        tw_madx[["name"], "ip.*"],
    )
    axs_t.set_xlim(axs[i].get_xlim())
    axs[i].set_xlabel("s [m]")
    [axs_t.axvline(xpos, linestyle="--") for xpos in tw_madx[["s"], "ip.*"]]

    axs[i].legend()
    axs[i].set_xlabel(r"$s [m]$")
axs[0].set_ylabel(r"$\Delta \mu_{x}$")
axs[1].set_ylabel(r"$\Delta \mu_{y}$")
plt.show()
# %%
