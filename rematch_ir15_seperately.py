import xtrack as xt
import xtrack._temp.lhc_match as lm
from misc import *

triplet_ir1 = "kqx1.l1 kqx1.r1 kqx2a.l1 kqx2a.r1 kqx2b.l1 kqx2b.r1 kqx3.l1 kqx3.r1"

triplet_ir5 = "kqx1.l5 kqx1.r5 kqx2a.l5 kqx2a.r5 kqx2b.l5 kqx2b.r5 kqx3.l5 kqx3.r5"

quads_ir1_b1 = (
    "kq4.l1b1 kq4.r1b1 kq5.l1b1 kq5.r1b1 kq6.l1b1 kq6.r1b1 "
    "kq7.l1b1 kq7.r1b1 kq8.l1b1 kq8.r1b1 kq9.l1b1 kq9.r1b1 "
    "kq10.l1b1 kq10.r1b1 kqtl11.l1b1 kqtl11.r1b1 "
    "kqt12.l1b1 kqt12.r1b1 kqt13.l1b1 kqt13.r1b1"
)

quads_ir1_b2 = (
    "kq4.l1b2 kq4.r1b2 kq5.l1b2 kq5.r1b2 kq6.l1b2 kq6.r1b2 "
    "kq7.l1b2 kq7.r1b2 kq8.l1b2 kq8.r1b2 kq9.l1b2 kq9.r1b2 "
    "kq10.l1b2 kq10.r1b2 kqtl11.l1b2 kqtl11.r1b2 kqt12.l1b2 "
    "kqt12.r1b2 kqt13.l1b2 kqt13.r1b2"
)

quads_ir5_b1 = (
    "kq4.l5b1 kq4.r5b1 kq5.l5b1 kq5.r5b1 kq6.l5b1 kq6.r5b1 "
    "kq7.l5b1 kq7.r5b1 kq8.l5b1 kq8.r5b1 kq9.l5b1 kq9.r5b1 "
    "kq10.l5b1 kq10.r5b1 kqtl11.l5b1 kqtl11.r5b1 "
    "kqt12.l5b1 kqt12.r5b1 kqt13.l5b1 kqt13.r5b1"
)

quads_ir5_b2 = (
    "kq4.l5b2 kq4.r5b2 kq5.l5b2 kq5.r5b2 kq6.l5b2 kq6.r5b2 "
    "kq7.l5b2 kq7.r5b2 kq8.l5b2 kq8.r5b2 kq9.l5b2 kq9.r5b2 "
    "kq10.l5b2 kq10.r5b2 kqtl11.l5b2 kqtl11.r5b2 kqt12.l5b2 "
    "kqt12.r5b2 kqt13.l5b2 kqt13.r5b2"
)


def rematch_ir1(
    collider,
    betxip1,
    betyip1,
    tw_sq_a81_ip1_a12=None,
    match_on_triplet=0,
    match_inj_tunes=0,
    no_match_beta=False,
    ir1q4sym=0,
    ir1q5sym=0,
    ir1q6sym=0,
    restore=True,
    solve=False,
    cycle=True,
):
    """Match IP5 for the beta_x, beta_y, and copy strenghts to IP1

    Parameters
    ----------
    collider : xtrack.Multiline
        HL-LHC collider with b1 and b2
    betxip5 : float
        Presquezzed betax
    betyip5 : float
        Presquezzed betay
    tw_sq_a45_ip5_a56 : dict
        Dictionary for beam1, beam2 with twiss tables for arcs 45, 56
    match_on_triplet: int
        Flag to select how to match the triplet.
        0 : Don't touch the triplet
        1 : Q1, Q2, Q3 free
        2 : Q2 free
        3 :
        4 : Link Q1 with Q3
    match_inj_tunes: bool
    no_match_beta: bool
        If False, match beta at the ip
    ir5q4sym: int
        Flag to connect Q4
        0 : Connect Q4 left for both beams and Q4 right for both beams
        1 : Connect Q4 left and right
    ir5q5sym: int
        Flag to connect Q5
        0 : Connect Q5 left for both beams and Q5 right for both beams
        1 : Connect Q5 left and right
    ir5q6sym: int
        Flag to connect Q6
        0 :Connect Q6 left for both beams and Q6 right for both beams
        1 : Connect Q6 left and right
    restore: bool
        Flag to restore the variables if the match fails
    solve: bool
        Flag to solve or not

    """
    if cycle:
        collider.lhcb1.cycle("ip3", inplace=True)
        collider.lhcb2.cycle("ip3", inplace=True)
        collider.lhcb1.twiss_default["only_markers"] = True
        collider.lhcb2.twiss_default["only_markers"] = True

        collider.lhcb1.twiss_default["method"] = "4d"
        collider.lhcb2.twiss_default["method"] = "4d"
        collider.lhcb2.twiss_default["reverse"] = True

    if tw_sq_a81_ip1_a12 is None:
        tw_sq_a81_ip1_a12 = lm.get_arc_periodic_solution(
            collider, arc_name=["81", "12"]
        )

    targets = []

    if no_match_beta == False:
        targets.append(
            xt.TargetSet(
                line="lhcb1",
                at="ip1",
                betx=betxip1,
                bety=betyip1,
                alfx=0,
                alfy=0,
                dx=0,
                dpx=0,
            )
        )
        targets.append(
            xt.TargetSet(
                line="lhcb2",
                at="ip1",
                betx=betxip1,
                bety=betyip1,
                alfx=0,
                alfy=0,
                dx=0,
                dpx=0,
            )
        )
    else:
        targets.append(
            xt.TargetSet(line="lhcb1", at="ip1", alfx=0, alfy=0, dx=0, dpx=0)
        )
        targets.append(
            xt.TargetSet(line="lhcb2", at="ip1", alfx=0, alfy=0, dx=0, dpx=0)
        )

    targets.append(
        xt.TargetSet(
            line="lhcb1",
            at="e.ds.r1.b1",
            betx=tw_sq_a81_ip1_a12["lhcb1"]["12"]["betx", "e.ds.r1.b1"],
            bety=tw_sq_a81_ip1_a12["lhcb1"]["12"]["bety", "e.ds.r1.b1"],
            alfx=tw_sq_a81_ip1_a12["lhcb1"]["12"]["alfx", "e.ds.r1.b1"],
            alfy=tw_sq_a81_ip1_a12["lhcb1"]["12"]["alfy", "e.ds.r1.b1"],
            dx=tw_sq_a81_ip1_a12["lhcb1"]["12"]["dx", "e.ds.r1.b1"],
            dpx=tw_sq_a81_ip1_a12["lhcb1"]["12"]["dpx", "e.ds.r1.b1"],
        )
    )
    targets.append(
        xt.TargetSet(
            line="lhcb2",
            at="e.ds.r1.b2",
            betx=tw_sq_a81_ip1_a12["lhcb2"]["12"]["betx", "e.ds.r1.b2"],
            bety=tw_sq_a81_ip1_a12["lhcb2"]["12"]["bety", "e.ds.r1.b2"],
            alfx=tw_sq_a81_ip1_a12["lhcb2"]["12"]["alfx", "e.ds.r1.b2"],
            alfy=tw_sq_a81_ip1_a12["lhcb2"]["12"]["alfy", "e.ds.r1.b2"],
            dx=tw_sq_a81_ip1_a12["lhcb2"]["12"]["dx", "e.ds.r1.b2"],
            dpx=tw_sq_a81_ip1_a12["lhcb2"]["12"]["dpx", "e.ds.r1.b2"],
        )
    )

    muxIP1b1_l = collider["lhcb1"].varval["muxip1b1_l"]
    muxIP1b1_r = collider["lhcb1"].varval["muxip1b1_r"]
    muyIP1b1_l = collider["lhcb1"].varval["muyip1b1_l"]
    muyIP1b1_r = collider["lhcb1"].varval["muyip1b1_r"]
    muxIP1b1 = muxIP1b1_l + muxIP1b1_r
    muyIP1b1 = muyIP1b1_l + muyIP1b1_r

    muxIP1b2_l = collider["lhcb2"].varval["muxip1b2_l"]
    muxIP1b2_r = collider["lhcb2"].varval["muxip1b2_r"]
    muyIP1b2_l = collider["lhcb2"].varval["muyip1b2_l"]
    muyIP1b2_r = collider["lhcb2"].varval["muyip1b2_r"]
    muxIP1b2 = muxIP1b2_l + muxIP1b2_r
    muyIP1b2 = muyIP1b2_l + muyIP1b2_r

    if match_inj_tunes == 0:
        targets.append(
            xt.TargetSet(line="lhcb1", at="ip1", mux=muxIP1b1_l, muy=muyIP1b1_l)
        )
        targets.append(
            xt.TargetSet(line="lhcb1", at="e.ds.r1.b1", mux=muxIP1b1, muy=muyIP1b1)
        )
        targets.append(
            xt.TargetSet(line="lhcb2", at="ip1", mux=muxIP1b2_l, muy=muyIP1b2_l)
        )
        targets.append(
            xt.TargetSet(line="lhcb2", at="e.ds.r1.b2", mux=muxIP1b2, muy=muyIP1b2)
        )

    else:
        targets.append(
            xt.TargetSet(
                line="lhcb1",
                at="e.ds.r1.b1",
                mux=muxIP1b1,
                muy=muyIP1b1,
            )
        )
        targets.append(
            xt.TargetSet(
                line="lhcb2",
                at="e.ds.r1.b2",
                mux=muxIP1b2,
                muy=muyIP1b2,
            )
        )

    vary_list = []

    if ir1q4sym == 0:
        collider.vars["imq4l"] = (
            -collider.vars["kq4.l1b2"] / collider.vars["kq4.l1b1"] / 100
        )
        collider.vars["imq4r"] = (
            -collider.vars["kq4.r1b2"] / collider.vars["kq4.r1b1"] / 100
        )

        collider.vars["kq4.l1b2"] = -(
            collider.vars["kq4.l1b1"] * collider.vars["imq4l"] * 100
        )
        collider.vars["kq4.r1b2"] = -(
            collider.vars["kq4.r1b1"] * collider.vars["imq4r"] * 100
        )

        if (
            collider.varval["imq4l"] < 1 / imb / 100
            or collider.varval["imq4l"] > imb / 100
        ):
            collider.vars["imq4l"]._set_value(1 / imb / 100)
        if (
            collider.varval["imq4r"] < 1 / imb / 100
            or collider.varval["imq4r"] > imb / 100
        ):
            collider.vars["imq4r"]._set_value(1 / imb / 100)

        vary_list.append(xt.Vary("imq4l", limits=(1 / imb / 100, imb / 100)))
        vary_list.append(xt.Vary("imq4r", limits=(1 / imb / 100, imb / 100)))
        vary_list.append(xt.Vary("kq4.l1b1"))
        vary_list.append(xt.Vary("kq4.r1b1"))
    elif ir1q4sym == 1:
        connect_lr_qs(collider, nq=4, ir=1, bim=1)
        connect_lr_qs(collider, nq=4, ir=1, bim=2)
        vary_list.append(xt.Vary("kq4.l1b1"))
        vary_list.append(xt.Vary("kq4.l1b2"))

    if ir1q5sym == 0:
        collider.vars["imq5l"] = (
            -collider.vars["kq5.l1b2"] / collider.vars["kq5.l1b1"] / 100
        )
        collider.vars["imq5r"] = (
            -collider.vars["kq5.r1b2"] / collider.vars["kq5.r1b1"] / 100
        )

        collider.vars["kq5.l1b2"] = -(
            collider.vars["kq5.l1b1"] * collider.vars["imq5l"] * 100
        )

        collider.vars["kq5.r1b2"] = -(
            collider.vars["kq5.r1b1"] * collider.vars["imq5r"] * 100
        )

        if (
            collider.varval["imq5l"] < 1 / imb / 100
            or collider.varval["imq5l"] > imb / 100
        ):
            collider.vars["imq5l"]._set_value(1 / imb / 100)
        if (
            collider.varval["imq5r"] < 1 / imb / 100
            or collider.varval["imq5r"] > imb / 100
        ):
            collider.vars["imq5r"]._set_value(1 / imb / 100)

        vary_list.append(xt.Vary("imq5l", limits=(1 / imb / 100, imb / 100)))
        vary_list.append(xt.Vary("imq5r", limits=(1 / imb / 100, imb / 100)))
        vary_list.append(xt.Vary("kq5.l1b1"))
        vary_list.append(xt.Vary("kq5.r1b1"))
    elif ir1q5sym == 1:
        connect_lr_qs(collider, nq=5, ir=1, bim=1)
        connect_lr_qs(collider, nq=5, ir=1, bim=2)
        vary_list.append(xt.Vary("kq5.l1b1"))
        vary_list.append(xt.Vary("kq5.l1b2"))

    if ir1q6sym == 0:
        collider.vars["imq6l"] = (
            -collider.vars["kq6.l1b2"] / collider.vars["kq6.l1b1"] / 100
        )
        collider.vars["imq6r"] = (
            -collider.vars["kq6.r1b2"] / collider.vars["kq6.r1b1"] / 100
        )

        collider.vars["kq6.l1b2"] = -(
            collider.vars["kq6.l1b1"] * collider.vars["imq6l"] * 100
        )
        collider.vars["kq6.r1b2"] = -(
            collider.vars["kq6.r1b1"] * collider.vars["imq6r"] * 100
        )

        if (
            collider.varval["imq6l"] < 1 / imb / 100
            or collider.varval["imq6l"] > imb / 100
        ):
            collider.vars["imq6l"]._set_value(1 / imb / 100)
        if (
            collider.varval["imq6r"] < 1 / imb / 100
            or collider.varval["imq6r"] > imb / 100
        ):
            collider.vars["imq6r"]._set_value(1 / imb / 100)

        vary_list.append(xt.Vary("imq6l", limits=(1 / imb / 100, imb / 100)))
        vary_list.append(xt.Vary("imq6r", limits=(1 / imb / 100, imb / 100)))
        vary_list.append(xt.Vary("kq6.l1b1"))
        vary_list.append(xt.Vary("kq6.r1b1"))
    elif ir1q6sym == 1:
        connect_lr_qs(collider, nq=6, ir=1, bim=1)
        connect_lr_qs(collider, nq=6, ir=1, bim=2)
        vary_list.append(xt.Vary("kq6.l1b1"))
        vary_list.append(xt.Vary("kq6.l1b2"))

    # Beam 1
    for quad in quads_ir1_b1.split()[6:]:
        vary_list.append(xt.Vary(quad))

    # Beam 2
    for quad in quads_ir1_b2.split()[6:]:
        vary_list.append(xt.Vary(quad))

    if match_on_triplet == 0:  # Q1,Q2,Q3 untouched
        pass
    elif match_on_triplet == 1:  # Q1 Q2 Q3 free
        connect_q2s(collider, ir=1)
        connect_lr_qx(collider, nqx=1, ir=1)
        connect_lr_qx(collider, nqx=3, ir=1)
        vary_list.append(xt.Vary("kqx1.l1"))
        vary_list.append(xt.Vary("kqx2a.l1"))
        vary_list.append(xt.Vary("kqx3.l1"))
    elif match_on_triplet == 2:
        # Use only Q2
        connect_q2s(collider, ir=1)
        vary_list.append(xt.Vary("kqx2a.l1"))
    elif match_on_triplet == 4:
        # link q3 and q1
        connect_q2s(collider, ir=1)
        connect_lr_qx(collider, nqx=1, ir=1)
        connect_lr_qx(collider, nqx=3, ir=1)
        collider.vars["kqx3.l1"] = collider.vars["kqx1.l1"]

        vary_list.append(xt.Vary("kqx1.l1"))
        vary_list.append(xt.Vary("kqx2a.l1"))
    elif match_on_triplet == 5:
        connect_q2s(collider, ir=1)
        connect_lr_qx(collider, nqx=1, ir=1)
        connect_lr_qx(collider, nqx=3, ir=1)
        vary_list.append(xt.Vary("kqx1.l1"))
        vary_list.append(xt.Vary("kqx2a.l1"))
        vary_list.append(xt.Vary("kqx3.l1"))
        targets.append(
            xt.Target(
                lambda tt: tt.lhcb1["bety", "mqxfb.b2r1_exit"]
                - tt.lhcb1["betx", "mqxfa.b3r1_exit"],
                value=xt.GreaterThan(0),
                tol=1e-2,
            )
        )
    elif match_on_triplet == 6:
        connect_q2s(collider, ir=1)
        connect_lr_qx(collider, nqx=1, ir=1)
        connect_lr_qx(collider, nqx=3, ir=1)
        vary_list.append(xt.Vary("kqx1.l1"))
        vary_list.append(xt.Vary("kqx2a.l1"))
        vary_list.append(xt.Vary("kqx3.l1"))
    targets.append(
        xt.Target(
            lambda tt: tt.lhcb1["bety", "acfca.4ar1.b1_exit"]
            / tt.lhcb1["betx", "acfca.4ar1.b1_exit"],
            value=0.98,
            tol=1e-2,
        )
    )

    # targets.append(
    #     xt.Target(
    #         lambda tt: tt.lhcb1["bety", "mqxfb.a2r1_exit"]
    #         / tt.lhcb1["betx", "mqxfa.b3r1_exit"],
    #         value=0.997,
    #         tol=1e-3,
    #     )
    # )

    twiss_init_b1 = tw_sq_a81_ip1_a12["lhcb1"]["81"].get_twiss_init("s.ds.l1.b1")
    twiss_init_b1.mux = 0
    twiss_init_b1.muy = 0

    twiss_init_b2 = tw_sq_a81_ip1_a12["lhcb2"]["81"].get_twiss_init("s.ds.l1.b2")
    twiss_init_b2.mux = 0
    twiss_init_b2.muy = 0

    opt = collider.match(
        solve=False,
        only_markers=False,
        method="4d",
        solver_options={"n_bisections": 3, "min_step": 1e-6, "n_steps_max": 25},
        default_tol=default_tol,
        ele_start=["s.ds.l1.b1", "s.ds.l1.b2"],
        ele_stop=["e.ds.r1.b1", "e.ds.r1.b2"],
        restore_if_fail=restore,
        assert_within_tol=False,
        twiss_init=[twiss_init_b1, twiss_init_b2],
        targets=targets,
        vary=vary_list,
        verbose=False,
        n_steps_max=25,
    )

    if solve:
        opt.solve()

    collider.varval["kqx1.r1"] = -collider.varval["kqx1.l1"]
    collider.varval["kqx2.l1"] = collider.varval["kqx2a.l1"]
    collider.varval["kqx2a.l1"] = collider.varval["kqx2.l1"]
    collider.varval["kqx2b.l1"] = collider.varval["kqx2.l1"]
    collider.varval["kqx2a.r1"] = -collider.varval["kqx2.l1"]
    collider.varval["kqx2b.r1"] = -collider.varval["kqx2.l1"]
    collider.varval["kqx3.r1"] = -collider.varval["kqx3.l1"]

    # remove the imqXlr knobs if any
    quads_imqlr = "kq4.l1b2 kq4.r1b2 kq5.l1b2 kq5.r1b2 kq6.l1b2 kq6.r1b2"
    for quad in quads_imqlr.split():
        collider.vars[quad] = collider.varval[quad]

    if cycle:
        collider.lhcb1.cycle("lhcb1$start", inplace=True)
        collider.lhcb2.cycle("lhcb2$start", inplace=True)
        collider.lhcb1.twiss_default["only_markers"] = True
        collider.lhcb2.twiss_default["only_markers"] = True

        collider.lhcb1.twiss_default["method"] = "4d"
        collider.lhcb2.twiss_default["method"] = "4d"
        collider.lhcb2.twiss_default["reverse"] = True

    return opt


def rematch_ir5(
    collider,
    betxip5,
    betyip5,
    tw_sq_a45_ip5_a56=None,
    match_on_triplet=0,
    match_inj_tunes=0,
    no_match_beta=False,
    ir5q4sym=0,
    ir5q5sym=0,
    ir5q6sym=0,
    restore=True,
    solve=False,
    ratio_at_CC=1.0,
    beta_peak_ratio=0.995,
):
    """Match IP5 for the beta_x, beta_y, and copy strenghts to IP1

    Parameters
    ----------
    collider : xtrack.Multiline
        HL-LHC collider with b1 and b2
    betxip5 : float
        Presquezzed betax
    betyip5 : float
        Presquezzed betay
    tw_sq_a45_ip5_a56 : dict
        Dictionary for beam1, beam2 with twiss tables for arcs 45, 56
    match_on_triplet: int
        Flag to select how to match the triplet.
        0 : Don't touch the triplet
        1 : Q1, Q2, Q3 free
        2 : Q2 free
        3 :
        4 : Link Q1 with Q3
    match_inj_tunes: bool
    no_match_beta: bool
        If False, match beta at the ip
    ir5q4sym: int
        Flag to connect Q4
        0 : Connect Q4 left for both beams and Q4 right for both beams
        1 : Connect Q4 left and right
    ir5q5sym: int
        Flag to connect Q5
        0 : Connect Q5 left for both beams and Q5 right for both beams
        1 : Connect Q5 left and right
    ir5q6sym: int
        Flag to connect Q6
        0 :Connect Q6 left for both beams and Q6 right for both beams
        1 : Connect Q6 left and right
    restore: bool
        Flag to restore the variables if the match fails
    solve: bool
        Flag to solve or not

    """

    if tw_sq_a45_ip5_a56 is None:
        tw_sq_a45_ip5_a56 = lm.get_arc_periodic_solution(
            collider, arc_name=["45", "56"]
        )

    targets = []

    if no_match_beta == False:
        targets.append(
            xt.TargetSet(
                line="lhcb1",
                at="ip5",
                betx=betxip5,
                bety=betyip5,
                alfx=0,
                alfy=0,
                dx=0,
                dpx=0,
            )
        )
        targets.append(
            xt.TargetSet(
                line="lhcb2",
                at="ip5",
                betx=betxip5,
                bety=betyip5,
                alfx=0,
                alfy=0,
                dx=0,
                dpx=0,
            )
        )
    else:
        targets.append(
            xt.TargetSet(line="lhcb1", at="ip5", alfx=0, alfy=0, dx=0, dpx=0)
        )
        targets.append(
            xt.TargetSet(line="lhcb2", at="ip5", alfx=0, alfy=0, dx=0, dpx=0)
        )

    targets.append(
        xt.TargetSet(
            line="lhcb1",
            at="e.ds.r5.b1",
            betx=tw_sq_a45_ip5_a56["lhcb1"]["56"]["betx", "e.ds.r5.b1"],
            bety=tw_sq_a45_ip5_a56["lhcb1"]["56"]["bety", "e.ds.r5.b1"],
            alfx=tw_sq_a45_ip5_a56["lhcb1"]["56"]["alfx", "e.ds.r5.b1"],
            alfy=tw_sq_a45_ip5_a56["lhcb1"]["56"]["alfy", "e.ds.r5.b1"],
            dx=tw_sq_a45_ip5_a56["lhcb1"]["56"]["dx", "e.ds.r5.b1"],
            dpx=tw_sq_a45_ip5_a56["lhcb1"]["56"]["dpx", "e.ds.r5.b1"],
        )
    )
    targets.append(
        xt.TargetSet(
            line="lhcb2",
            at="e.ds.r5.b2",
            betx=tw_sq_a45_ip5_a56["lhcb2"]["56"]["betx", "e.ds.r5.b2"],
            bety=tw_sq_a45_ip5_a56["lhcb2"]["56"]["bety", "e.ds.r5.b2"],
            alfx=tw_sq_a45_ip5_a56["lhcb2"]["56"]["alfx", "e.ds.r5.b2"],
            alfy=tw_sq_a45_ip5_a56["lhcb2"]["56"]["alfy", "e.ds.r5.b2"],
            dx=tw_sq_a45_ip5_a56["lhcb2"]["56"]["dx", "e.ds.r5.b2"],
            dpx=tw_sq_a45_ip5_a56["lhcb2"]["56"]["dpx", "e.ds.r5.b2"],
        )
    )

    muxIP5b1_l = collider["lhcb1"].varval["muxip5b1_l"]
    muxIP5b1_r = collider["lhcb1"].varval["muxip5b1_r"]
    muyIP5b1_l = collider["lhcb1"].varval["muyip5b1_l"]
    muyIP5b1_r = collider["lhcb1"].varval["muyip5b1_r"]
    muxIP5b1 = muxIP5b1_l + muxIP5b1_r
    muyIP5b1 = muyIP5b1_l + muyIP5b1_r

    muxIP5b2_l = collider["lhcb2"].varval["muxip5b2_l"]
    muxIP5b2_r = collider["lhcb2"].varval["muxip5b2_r"]
    muyIP5b2_l = collider["lhcb2"].varval["muyip5b2_l"]
    muyIP5b2_r = collider["lhcb2"].varval["muyip5b2_r"]
    muxIP5b2 = muxIP5b2_l + muxIP5b2_r
    muyIP5b2 = muyIP5b2_l + muyIP5b2_r

    if match_inj_tunes == 0:
        targets.append(
            xt.TargetSet(line="lhcb1", at="ip5", mux=muxIP5b1_l, muy=muyIP5b1_l)
        )
        targets.append(
            xt.TargetSet(line="lhcb1", at="e.ds.r5.b1", mux=muxIP5b1, muy=muyIP5b1)
        )
        targets.append(
            xt.TargetSet(line="lhcb2", at="ip5", mux=muxIP5b2_l, muy=muyIP5b2_l)
        )
        targets.append(
            xt.TargetSet(line="lhcb2", at="e.ds.r5.b2", mux=muxIP5b2, muy=muyIP5b2)
        )

    else:
        targets.append(
            xt.TargetSet(
                line="lhcb1",
                at="e.ds.r5.b1",
                mux=muxIP5b1,
                muy=muyIP5b1,
            )
        )
        targets.append(
            xt.TargetSet(
                line="lhcb2",
                at="e.ds.r5.b2",
                mux=muxIP5b2,
                muy=muyIP5b2,
            )
        )

    vary_list = []

    if ir5q4sym == 0:
        collider.vars["imq4l"] = (
            -collider.vars["kq4.l5b2"] / collider.vars["kq4.l5b1"] / 100
        )
        collider.vars["imq4r"] = (
            -collider.vars["kq4.r5b2"] / collider.vars["kq4.r5b1"] / 100
        )

        collider.vars["kq4.l5b2"] = -(
            collider.vars["kq4.l5b1"] * collider.vars["imq4l"] * 100
        )
        collider.vars["kq4.r5b2"] = -(
            collider.vars["kq4.r5b1"] * collider.vars["imq4r"] * 100
        )

        vary_list.append(xt.Vary("imq4l"))
        vary_list.append(xt.Vary("imq4r"))
        vary_list.append(xt.Vary("kq4.l5b1"))
        vary_list.append(xt.Vary("kq4.r5b1"))
    elif ir5q4sym == 1:
        connect_lr_qs(collider, nq=4, ir=5, bim=1)
        connect_lr_qs(collider, nq=4, ir=5, bim=2)
        vary_list.append(xt.Vary("kq4.l5b1"))
        vary_list.append(xt.Vary("kq4.l5b2"))

    if ir5q5sym == 0:
        collider.vars["imq5l"] = (
            -collider.vars["kq5.l5b2"] / collider.vars["kq5.l5b1"] / 100
        )
        collider.vars["imq5r"] = (
            -collider.vars["kq5.r5b2"] / collider.vars["kq5.r5b1"] / 100
        )

        collider.vars["kq5.l5b2"] = -(
            collider.vars["kq5.l5b1"] * collider.vars["imq5l"] * 100
        )
        collider.vars["kq5.r5b2"] = -(
            collider.vars["kq5.r5b1"] * collider.vars["imq5r"] * 100
        )

        vary_list.append(xt.Vary("imq5l", limits=(1 / imb / 100, imb / 100)))
        vary_list.append(xt.Vary("imq5r", limits=(1 / imb / 100, imb / 100)))
        vary_list.append(xt.Vary("kq5.l5b1"))
        vary_list.append(xt.Vary("kq5.r5b1"))
    elif ir5q5sym == 1:
        connect_lr_qs(collider, nq=5, ir=5, bim=1)
        connect_lr_qs(collider, nq=5, ir=5, bim=2)
        vary_list.append(xt.Vary("kq5.l5b1"))
        vary_list.append(xt.Vary("kq5.l5b2"))

    if ir5q6sym == 0:
        collider.vars["imq6l"] = (
            -collider.vars["kq6.l5b2"] / collider.vars["kq6.l5b1"] / 100
        )
        collider.vars["imq6r"] = (
            -collider.vars["kq6.r5b2"] / collider.vars["kq6.r5b1"] / 100
        )

        collider.vars["kq6.l5b2"] = -(
            collider.vars["kq6.l5b1"] * collider.vars["imq6l"] * 100
        )
        collider.vars["kq6.r5b2"] = -(
            collider.vars["kq6.r5b1"] * collider.vars["imq6r"] * 100
        )

        vary_list.append(xt.Vary("imq6l", limits=(1 / imb / 100, imb / 100)))
        vary_list.append(xt.Vary("imq6r", limits=(1 / imb / 100, imb / 100)))
        vary_list.append(xt.Vary("kq6.l5b1"))
        vary_list.append(xt.Vary("kq6.r5b1"))
    elif ir5q6sym == 1:
        connect_lr_qs(collider, nq=6, ir=5, bim=1)
        connect_lr_qs(collider, nq=6, ir=5, bim=2)
        vary_list.append(xt.Vary("kq6.l5b1"))
        vary_list.append(xt.Vary("kq6.l5b2"))

    # Beam 1
    for quad in quads_ir5_b1.split()[6:]:
        vary_list.append(xt.Vary(quad))

    # Beam 2
    for quad in quads_ir5_b2.split()[6:]:
        vary_list.append(xt.Vary(quad))

    if match_on_triplet == 0:  # q1 q2 q3 untouched
        pass
    elif match_on_triplet == 1:  # Q1 Q2 Q3 free
        connect_q2s(collider)
        connect_lr_qx(collider, 1)
        connect_lr_qx(collider, 3)
        vary_list.append(xt.Vary("kqx1.l5"))
        vary_list.append(xt.Vary("kqx2a.l5"))
        vary_list.append(xt.Vary("kqx3.l5"))
    elif match_on_triplet == 2:
        # connect kqx2a and kqx2b left and right
        connect_q2s(collider)
        vary_list.append(xt.Vary("kqx2a.l5"))
    elif match_on_triplet == 4:
        # link q3 and q1
        # First set everything to zero
        collider.vars["kqx1.r5"]._set_value(0)
        collider.vars["kqx3.l5"]._set_value(0)
        collider.vars["kqx3.r5"]._set_value(0)

        collider.vars["kqx1.r5"] -= collider.vars["kqx1.l5"]
        collider.vars["kqx3.l5"] += collider.vars["kqx1.l5"]
        collider.vars["kqx3.r5"] -= collider.vars["kqx1.l5"]

        connect_q2s(collider)
        vary_list.append(xt.Vary("kqx1.l5"))
        vary_list.append(xt.Vary("kqx2a.l5"))
    elif match_on_triplet == 5:
        connect_q2s(collider, ir=1)
        connect_lr_qx(collider, nqx=1, ir=1)
        connect_lr_qx(collider, nqx=3, ir=1)
        vary_list.append(xt.Vary("kqx1.l5"))
        vary_list.append(xt.Vary("kqx2a.l5"))
        vary_list.append(xt.Vary("kqx3.l5"))
        targets.append(
            xt.Target(
                lambda tt: tt.lhcb1["bety", "mqxfb.b2r5_exit"]
                - tt.lhcb1["betx", "mqxfa.b3r5_exit"],
                value=xt.GreaterThan(0),
                tol=1e-2,
            )
        )
    elif match_on_triplet == 6:
        connect_q2s(collider, ir=5)
        connect_lr_qx(collider, nqx=1, ir=5)
        connect_lr_qx(collider, nqx=3, ir=5)
        # vary_list.append(xt.Vary(tag="triplet", name="kqx1.l5"))
        vary_list.append(xt.Vary(tag="triplet", name="kqx2a.l5"))
        vary_list.append(xt.Vary(tag="triplet", name="kqx3.l5"))
        targets.append(
            xt.Target(
                lambda tt: tt.lhcb1["bety", "acfca.4ar5.b1_exit"]
                / tt.lhcb1["betx", "acfca.4ar5.b1_exit"],
                value=ratio_at_CC,  # 0.98
                tol=1e-2,
            )
        )
        targets.append(
            xt.Target(
                lambda tt: tt.lhcb1["bety", "mqxfb.a2r5_exit"]
                / tt.lhcb1["betx", "mqxfa.b3r5_exit"],
                value=beta_peak_ratio,
                tol=1e-2,
            )
        )

    twiss_init_b1 = tw_sq_a45_ip5_a56["lhcb1"]["45"].get_twiss_init("s.ds.l5.b1")
    twiss_init_b1.mux = 0
    twiss_init_b1.muy = 0

    twiss_init_b2 = tw_sq_a45_ip5_a56["lhcb2"]["45"].get_twiss_init("s.ds.l5.b2")
    twiss_init_b2.mux = 0
    twiss_init_b2.muy = 0

    opt = collider.match(
        solve=False,
        only_markers=False,
        method="4d",
        solver_options={"n_bisections": 3, "min_step": 1e-6, "n_steps_max": 25},
        default_tol=default_tol,
        ele_start=["s.ds.l5.b1", "s.ds.l5.b2"],
        ele_stop=["e.ds.r5.b1", "e.ds.r5.b2"],
        restore_if_fail=restore,
        assert_within_tol=False,
        twiss_init=[twiss_init_b1, twiss_init_b2],
        targets=targets,
        vary=vary_list,
        verbose=False,
        n_steps_max=25,
    )

    if solve:
        opt.solve()

    collider.varval["kqx1.r5"] = -collider.varval["kqx1.l5"]
    collider.varval["kqx2.l5"] = collider.varval["kqx2a.l5"]
    collider.varval["kqx2a.l5"] = collider.varval["kqx2.l5"]
    collider.varval["kqx2b.l5"] = collider.varval["kqx2.l5"]
    collider.varval["kqx2a.r5"] = -collider.varval["kqx2.l5"]
    collider.varval["kqx2b.r5"] = -collider.varval["kqx2.l5"]
    collider.varval["kqx3.r5"] = -collider.varval["kqx3.l5"]

    # remove the imqXlr knobs if any
    quads_imqlr = "kq4.l5b2 kq4.r5b2 kq5.l5b2 kq5.r5b2 kq6.l5b2 kq6.r5b2"
    for quad in quads_imqlr.split():
        collider.vars[quad] = collider.varval[quad]

    return opt
