# %%
import os
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches

import xtrack as xt
import xtrack._temp.lhc_match as lm
from misc import *

from rematch_from_xtrack import *
from step_and_limits import *

from rematch_xing15 import match_orbit_knobs_ip15

# %%
prefix = Path("../").absolute()

# optics_file = prefix / "summer_studies/collapse/opt_collapse_1100_1500.madx"
# optics_file = "opt_round_1100_xsuite.madx";
# optics_file = "./opt_round_1100_xsuite_bet0_800.madx"
# optics_file = "optics_xsuite/opt_round_1100_ats_1000.madx"
# optics_file = "optics_xsuite/opt_round_1100_ats_800.madx"
# optics_file = "optics_xsuite/opt_round_1100_ats_700.madx"
# optics_file = "optics_xsuite/opt_round_1100_ats_600.madx"

# optics_file = prefix / "summer_studies/SoL/opt_flathv_300_1200_1500.madx"
# optics_file = prefix / "summer_studies/SoL/opt_flathv_420_840_1500.madx"

# optics_file = prefix / "summer_studies/collapse/opt_collapse_flathv_900_1800_1500.madx"
# optics_file = prefix / "summer_studies/collapse/opt_collapse_flathv_700_2800.madx"
# optics_file = "optics_xsuite/opt_flathv_300_720_again.madx"

# optics_file = "cc_ratio_study/opt_cc1.9.madx"

# optics_file = (
#     prefix / "acc-models-lhc/strengths/round/start_collapse/opt_collapse_980_1500.madx"
# )

# optics_file = "cc_ratio_study/opt_1100_cc0.97_peak0.98_part4.madx"

# optics_file = prefix / "summer_studies/collapse/opt_collapse_1000_1500.madx"

optics_file = "../madx_vs_xsuite_rematch/madx/opt_round_1100_cc_angle_optim_again.madx"
collider_name = "collider_hl16.json"


collider = xt.Multiline.from_json(collider_name)
collider.build_trackers()
# %%
collider.vars.load_madx_optics_file(optics_file)

collider.lhcb1.twiss_default["only_markers"] = True
collider.lhcb2.twiss_default["only_markers"] = True

collider.lhcb1.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["reverse"] = True

# %%
lm.set_var_limits_and_steps(collider)
set_var_limits_and_steps_quads_ip15(collider)
collider.vars.vary_default.update(
    {
        "kqx1.l5": {"step": 1e-6, "limits": (-qtlim1, 0)},
        "kqx2a.l5": {"step": 1e-6, "limits": (-qtlim1, 0)},
        "kqx3.l5": {"step": 1e-6, "limits": (-qtlim1, 0)},
    }
)
# %%
staged_match = True
first_pass = False

# %%
betx0 = 0.5
bety0 = 0.5

betx_ip1 = 1.1
bety_ip1 = 1.1
betx_ip5 = 1.1
bety_ip5 = 1.1
# %%

scxir1 = betx_ip1 / betx0
scyir1 = bety_ip1 / bety0
scxir5 = betx_ip5 / betx0
scyir5 = bety_ip5 / bety0
# %%
tw0b1 = collider["lhcb1"].cycle("ip3").twiss(method="4d")
tw0b2 = collider["lhcb2"].cycle("ip3").twiss(method="4d")


# %%
print(tw0b1[["betx", "bety"], "ip.*"])
print(tw0b2[["betx", "bety"], "ip.*"])


# %%
muxs = {}
muys = {}

saved_vars = {}
saved_vars.update(
    {
        "b1": {"ip2": {}, "ip3": {}, "ip4": {}, "ip6": {}, "ip7": {}, "ip8": {}},
        "b2": {"ip2": {}, "ip3": {}, "ip4": {}, "ip6": {}, "ip7": {}, "ip8": {}},
    }
)

for bim in ["b1", "b2"]:
    for arc_name in lm.ARC_NAMES:
        muxs[f"mux{arc_name}{bim}"] = collider.varval[f"mux{arc_name}{bim}"]
        muys[f"muy{arc_name}{bim}"] = collider.varval[f"muy{arc_name}{bim}"]
    for ip_name in [2, 4, 6, 8]:
        muxs[f"muxip{ip_name}{bim}"] = collider.varval[f"muxip{ip_name}{bim}"]
        muys[f"muyip{ip_name}{bim}"] = collider.varval[f"muyip{ip_name}{bim}"]
    for ip_name in [1, 5]:
        muxs[f"muxip{ip_name}{bim}_l"] = collider.varval[f"muxip{ip_name}{bim}_l"]
        muys[f"muyip{ip_name}{bim}_l"] = collider.varval[f"muyip{ip_name}{bim}_l"]
        muxs[f"muxip{ip_name}{bim}_r"] = collider.varval[f"muxip{ip_name}{bim}_r"]
        muys[f"muyip{ip_name}{bim}_r"] = collider.varval[f"muyip{ip_name}{bim}_r"]

    saved_vars[bim]["ip2"]["betx"] = collider.varval[f"betxip2{bim}"]
    saved_vars[bim]["ip2"]["bety"] = collider.varval[f"betyip2{bim}"]

    for ip_name in [3, 7]:
        for param in ["betx", "bety", "alfx", "alfy", "dx", "dpx", "mux", "muy"]:
            saved_vars[bim][f"ip{ip_name}"][param] = collider.varval[
                f"{param}ip{ip_name}{bim}"
            ]

    for ip_name in [4, 6, 8]:
        for param in ["betx", "bety", "alfx", "alfy", "dx", "dpx"]:
            saved_vars[bim][f"ip{ip_name}"][param] = collider.varval[
                f"{param}ip{ip_name}{bim}"
            ]


# %%
tw45_56_xt = lm.get_arc_periodic_solution(collider, arc_name=["45", "56"])
tw81_12_xt = lm.get_arc_periodic_solution(collider, arc_name=["81", "12"])

# %%
beta0 = tw45_56_xt["lhcb1"]["45"].get_twiss_init("s.ds.l5.b1")
beta0.mux = 0
beta0.muy = 0
tw_ip5_pre_b1_0 = collider.lhcb1.twiss(
    ele_start="s.ds.l5.b1", ele_stop="e.ds.r5.b1", twiss_init=beta0
)
print(tw_ip5_pre_b1_0[["betx", "bety"], "ip5"])

# %%
optimizers = {}
# %%
# optimizers["ir15"] = rematch_ir15(
#     collider,
#     betx0,
#     bety0,
#     tw_sq_a45_ip5_a56=tw45_56_xt,
#     restore=False,
#     solve=True,
#     match_on_triplet=5,
# )


get_ipython().run_line_magic("load_ext", "autoreload")
get_ipython().run_line_magic("autoreload", "2")
from misc import rematch_ir15

# %%
# collider.vars['kqx1.l5']             =        -0.005661508856215987 ;
# collider.vars['kqx2a.l5']            =        -0.005583529554232232 ;
# collider.vars['kqx2b.l5']            =        -0.005583529554232232 ;
# collider.vars['kqx3.l5']             =        -0.005555652734786914 ;
# collider.vars['kqx1.l1']             =        -0.005661508856215987 ;
# collider.vars['kqx2a.l1']            =        -0.005583529554232232 ;
# collider.vars['kqx2b.l1']            =        -0.005583529554232232 ;
# collider.vars['kqx3.l1']             =        -0.005555652734786914 ;


# collider.vars['kqx1.r5']             =        0.005661508856215987 ;
# collider.vars['kqx2a.r5']            =        0.005583529554232232 ;
# collider.vars['kqx2b.r5']            =        0.005583529554232232 ;
# collider.vars['kqx3.r5']             =        0.005555652734786914 ;
# collider.vars['kqx1.r1']             =        0.005661508856215987 ;
# collider.vars['kqx2a.r1']            =        0.005583529554232232 ;
# collider.vars['kqx2b.r1']            =        0.005583529554232232 ;
# collider.vars['kqx3.r1']             =        0.005555652734786914 ;
def get_beta_ratio_at_cc(collider):
    tw = collider.twiss()
    ratiob1 = (
        tw.lhcb1["bety", "acfca.4ar5.b1_exit"] / tw.lhcb1["betx", "acfca.4ar5.b1_exit"]
    )
    ratiob2 = (
        tw.lhcb2["betx", "acfca.4al5.b2_exit"] / tw.lhcb2["bety", "acfca.4al5.b2_exit"]
    )
    print(f"acfca.4ar5.b1= {tw.lhcb1[['betx','bety'],'acfca.4ar5.b1_exit']}")

    return ratiob1, ratiob2


def get_beta_peak_ratio(collider):
    tw = collider.twiss()
    ratio = tw.lhcb1["bety", "mqxfb.a2r5_exit"] / tw.lhcb1["betx", "mqxfa.b3r5_exit"]
    return ratio


# %%
# collider.varval['kqx1.l5'] = -qtlim1
optimizers["ir15"] = rematch_ir15(
    collider,
    betx0,
    bety0,
    tw_sq_a45_ip5_a56=tw45_56_xt,
    restore=False,
    solve=True,
    match_on_triplet=0,
    ratio_at_CC=0.979,
    beta_peak_ratio=0.98,
)
# %%
# print(optimizers["ir15"].target_status())
print(optimizers["ir15"].log())

# %%
optimizers["ir15_run2"] = rematch_ir15(
    collider,
    betx0,
    bety0,
    tw_sq_a45_ip5_a56=tw45_56_xt,
    restore=False,
    solve=True,
    match_on_triplet=0,
)
# %%
# print(optimizers["ir15_run2"].target_status())
print(optimizers["ir15_run2"].log())

# %%
tw45_xt = lm.get_arc_periodic_solution(collider, arc_name=["45"])
beta0 = tw45_xt["lhcb1"]["45"].get_twiss_init("s.ds.l5.b1")
beta0.mux = 0
beta0.muy = 0
tw_ip5_pre_b1 = collider.lhcb1.twiss(
    ele_start="s.ds.l5.b1", ele_stop="e.ds.r5.b1", twiss_init=beta0
)

print(tw_ip5_pre_b1[["betx", "bety"], "ip5"])

# %%
fig, axs = plt.subplots(figsize=(21, 11))
axs.set_title("IP5")
axs.plot(
    tw_ip5_pre_b1_0.s,
    tw_ip5_pre_b1_0.betx,
    color="black",
    label=rf"$\beta_x^0$ Old,{tw_ip5_pre_b1_0['betx', 'ip5']:2.3f}",
)
axs.plot(
    tw_ip5_pre_b1_0.s,
    tw_ip5_pre_b1_0.bety,
    color="red",
    label=rf"$\beta_y^0$ Old,{tw_ip5_pre_b1_0['bety', 'ip5']:2.3f}",
)
axs.plot(
    tw_ip5_pre_b1.s,
    tw_ip5_pre_b1.betx,
    color="black",
    ls="--",
    label=rf"$\beta_x^0$ New,{tw_ip5_pre_b1['betx', 'ip5']:2.3f}",
)
axs.plot(
    tw_ip5_pre_b1.s,
    tw_ip5_pre_b1.bety,
    color="red",
    ls="--",
    label=rf"$\beta_y^0$ New,{tw_ip5_pre_b1['bety', 'ip5']:2.3f}",
)
axs_t = axs.twiny()
# axs_t.set_xticks(
#     tw_ip5_pre_b1[["s"], "mq.*._entry"],
#     tw_ip5_pre_b1[["name"], "mq.*._entry"],
#     rotation=45,
# )
axs_t.set_xticks(
    np.concatenate(
        [tw_ip5_pre_b1[["s"], "mq.*._entry"], tw_ip5_pre_b1[["s"], "acfca.*._entry"]]
    ),
    np.concatenate(
        [
            tw_ip5_pre_b1[["name"], "mq.*._entry"],
            tw_ip5_pre_b1[["name"], "acfca.*._entry"],
        ]
    ),
    rotation=45,
)
axs_t.set_xlim(axs.get_xlim())
# [axs_t.axvline(xpos, linestyle="--") for xpos in tw_ip5_pre_b1[["s"], "mq.*._entry"]]
# [axs_t.axvline(xpos, linestyle="--") for xpos in tw_ip5_pre_b1[["s"], "acfca.*._entry"]]

axs_t2 = axs.twinx()
axs_t2.set_yticks([])
ss, ll, kk = plot_ir15_quads(collider, "lhcb1", ir="5")
for iss, ill, ikk in zip(ss, ll, kk):
    plotLatticeSeries(
        axs_t2, iss[0] + ill * 0.5, ill, ikk, ikk / 2, color="cyan", alpha=0.5
    )
axs_t2.set_ylim(-0.01, 0.01)
axs.set_xlabel("s [m]")
axs.set_ylabel(r"$\beta_{x,y} [m]$")
axs.legend()
axs.grid()
plt.show()


# %%
tw_non_ats_arcs = lm.get_arc_periodic_solution(
    collider, arc_name=["23", "34", "67", "78"]
)

# %%
for bim in ["b1", "b2"]:
    optimizers[f"ir3{bim}"] = lm.rematch_ir3(
        collider=collider,
        line_name=f"lhc{bim}",
        boundary_conditions_left=tw_non_ats_arcs[f"lhc{bim}"]["23"],
        boundary_conditions_right=tw_non_ats_arcs[f"lhc{bim}"]["34"],
        mux_ir3=saved_vars[bim]["ip3"]["mux"],
        muy_ir3=saved_vars[bim]["ip3"]["muy"],
        alfx_ip3=saved_vars[bim]["ip3"]["alfx"],
        alfy_ip3=saved_vars[bim]["ip3"]["alfy"],
        betx_ip3=saved_vars[bim]["ip3"]["betx"],
        bety_ip3=saved_vars[bim]["ip3"]["bety"],
        dx_ip3=saved_vars[bim]["ip3"]["dx"],
        dpx_ip3=saved_vars[bim]["ip3"]["dpx"],
        solve=True,
        staged_match=True,
        default_tol=default_tol,
    )

# %%
optimizers["ir3b1"].target_status()
optimizers["ir3b2"].target_status()
# %%
print(optimizers["ir3b1"].log())
print(optimizers["ir3b2"].log())
# %%
for bim in ["b1", "b2"]:
    optimizers[f"ir7{bim}"] = lm.rematch_ir7(
        collider=collider,
        line_name=f"lhc{bim}",
        boundary_conditions_left=tw_non_ats_arcs[f"lhc{bim}"]["67"],
        boundary_conditions_right=tw_non_ats_arcs[f"lhc{bim}"]["78"],
        mux_ir7=saved_vars[bim]["ip7"]["mux"],
        muy_ir7=saved_vars[bim]["ip7"]["muy"],
        alfx_ip7=saved_vars[bim]["ip7"]["alfx"],
        alfy_ip7=saved_vars[bim]["ip7"]["alfy"],
        betx_ip7=saved_vars[bim]["ip7"]["betx"],
        bety_ip7=saved_vars[bim]["ip7"]["bety"],
        dx_ip7=saved_vars[bim]["ip7"]["dx"],
        dpx_ip7=saved_vars[bim]["ip7"]["dpx"],
        solve=True,
        staged_match=True,
        default_tol=default_tol,
    )

# %%
optimizers["ir7b1"].target_status()
optimizers["ir7b2"].target_status()
# %%
print(optimizers["ir7b1"].log())
print(optimizers["ir7b2"].log())

# %%
twb1 = collider["lhcb1"].cycle("ip3").twiss(method="4d")
twb2 = collider["lhcb2"].cycle("ip3").twiss(method="4d")


# %%
print(twb1[["betx", "bety"], "ip.*"])
print(twb2[["betx", "bety"], "ip.*"])
# %%


# %%
tw_sq_a81_ip1_a12_b1 = lm.propagate_optics_from_beta_star(
    collider,
    ip_name="ip1",
    line_name="lhcb1",
    ele_start="s.ds.r8.b1",
    ele_stop="e.ds.l2.b1",
    beta_star_x=betx_ip1,
    beta_star_y=bety_ip1,
)

tw_sq_a45_ip5_a56_b1 = lm.propagate_optics_from_beta_star(
    collider,
    ip_name="ip5",
    line_name="lhcb1",
    ele_start="s.ds.r4.b1",
    ele_stop="e.ds.l6.b1",
    beta_star_x=betx_ip5,
    beta_star_y=bety_ip5,
)

tw_sq_a81_ip1_a12_b2 = lm.propagate_optics_from_beta_star(
    collider,
    ip_name="ip1",
    line_name="lhcb2",
    ele_start="s.ds.r8.b2",
    ele_stop="e.ds.l2.b2",
    beta_star_x=betx_ip1,
    beta_star_y=bety_ip1,
)

tw_sq_a45_ip5_a56_b2 = lm.propagate_optics_from_beta_star(
    collider,
    ip_name="ip5",
    line_name="lhcb2",
    ele_start="s.ds.r4.b2",
    ele_stop="e.ds.l6.b2",
    beta_star_x=betx_ip5,
    beta_star_y=bety_ip5,
)

# %%
(
    mux_ir2_target_b1,
    muy_ir2_target_b1,
    mux_ir4_target_b1,
    muy_ir4_target_b1,
    mux_ir6_target_b1,
    muy_ir6_target_b1,
    mux_ir8_target_b1,
    muy_ir8_target_b1,
) = lm.compute_ats_phase_advances_for_auxiliary_irs(
    "lhcb1",
    tw_sq_a81_ip1_a12_b1,
    tw_sq_a45_ip5_a56_b1,
    muxs["muxip1b1_l"],
    muys["muyip1b1_l"],
    muxs["muxip1b1_r"],
    muys["muyip1b1_r"],
    muxs["muxip5b1_l"],
    muys["muyip5b1_l"],
    muxs["muxip5b1_r"],
    muys["muyip5b1_r"],
    muxs["muxip2b1"],
    muys["muyip2b1"],
    muxs["muxip4b1"],
    muys["muyip4b1"],
    muxs["muxip6b1"],
    muys["muyip6b1"],
    muxs["muxip8b1"],
    muys["muyip8b1"],
    muxs["mux12b1"],
    muys["muy12b1"],
    muxs["mux45b1"],
    muys["muy45b1"],
    muxs["mux56b1"],
    muys["muy56b1"],
    muxs["mux81b1"],
    muys["muy81b1"],
)

(
    mux_ir2_target_b2,
    muy_ir2_target_b2,
    mux_ir4_target_b2,
    muy_ir4_target_b2,
    mux_ir6_target_b2,
    muy_ir6_target_b2,
    mux_ir8_target_b2,
    muy_ir8_target_b2,
) = lm.compute_ats_phase_advances_for_auxiliary_irs(
    "lhcb2",
    tw_sq_a81_ip1_a12_b2,
    tw_sq_a45_ip5_a56_b2,
    muxs["muxip1b2_l"],
    muys["muyip1b2_l"],
    muxs["muxip1b2_r"],
    muys["muyip1b2_r"],
    muxs["muxip5b2_l"],
    muys["muyip5b2_l"],
    muxs["muxip5b2_r"],
    muys["muyip5b2_r"],
    muxs["muxip2b2"],
    muys["muyip2b2"],
    muxs["muxip4b2"],
    muys["muyip4b2"],
    muxs["muxip6b2"],
    muys["muyip6b2"],
    muxs["muxip8b2"],
    muys["muyip8b2"],
    muxs["mux12b2"],
    muys["muy12b2"],
    muxs["mux45b2"],
    muys["muy45b2"],
    muxs["mux56b2"],
    muys["muy56b2"],
    muxs["mux81b2"],
    muys["muy81b2"],
)
# %%

optimizers["ir2b1"] = rematch_ir2(
    collider,
    line_name="lhcb1",
    boundary_conditions_left=tw_sq_a81_ip1_a12_b1,
    boundary_conditions_right=tw_non_ats_arcs["lhcb1"]["23"],
    mux_ir2=mux_ir2_target_b1,
    muy_ir2=muy_ir2_target_b1,
    betx_ip2=saved_vars["b1"]["ip2"]["betx"],
    bety_ip2=saved_vars["b1"]["ip2"]["bety"],
    solve=True,
    staged_match=staged_match,
    default_tol=default_tol,
)


optimizers["ir2b2"] = rematch_ir2(
    collider,
    line_name="lhcb2",
    boundary_conditions_left=tw_sq_a81_ip1_a12_b2,
    boundary_conditions_right=tw_non_ats_arcs["lhcb2"]["23"],
    mux_ir2=mux_ir2_target_b2,
    muy_ir2=muy_ir2_target_b2,
    betx_ip2=saved_vars["b2"]["ip2"]["betx"],
    bety_ip2=saved_vars["b2"]["ip2"]["bety"],
    solve=True,
    staged_match=staged_match,
    default_tol=default_tol,
)


# %%
optimizers["ir2b1"].target_status()
optimizers["ir2b2"].target_status()
# %%
print(optimizers["ir2b1"].log())
print(optimizers["ir2b2"].log())
# %%

optimizers["ir8b1"] = lm.rematch_ir8(
    collider,
    line_name="lhcb1",
    boundary_conditions_left=tw_non_ats_arcs["lhcb1"]["78"],
    boundary_conditions_right=tw_sq_a81_ip1_a12_b1,
    solve=True,
    mux_ir8=mux_ir8_target_b1,
    muy_ir8=muy_ir8_target_b1,
    alfx_ip8=saved_vars["b1"]["ip8"]["alfx"],
    alfy_ip8=saved_vars["b1"]["ip8"]["alfy"],
    betx_ip8=saved_vars["b1"]["ip8"]["betx"],
    bety_ip8=saved_vars["b1"]["ip8"]["bety"],
    dx_ip8=saved_vars["b1"]["ip8"]["dx"],
    dpx_ip8=saved_vars["b1"]["ip8"]["dpx"],
    staged_match=staged_match,
    default_tol=default_tol,
)

optimizers["ir8b2"] = lm.rematch_ir8(
    collider,
    line_name="lhcb2",
    boundary_conditions_left=tw_non_ats_arcs["lhcb2"]["78"],
    boundary_conditions_right=tw_sq_a81_ip1_a12_b2,
    solve=True,
    mux_ir8=mux_ir8_target_b2,
    muy_ir8=muy_ir8_target_b2,
    alfx_ip8=saved_vars["b2"]["ip8"]["alfx"],
    alfy_ip8=saved_vars["b2"]["ip8"]["alfy"],
    betx_ip8=saved_vars["b2"]["ip8"]["betx"],
    bety_ip8=saved_vars["b2"]["ip8"]["bety"],
    dx_ip8=saved_vars["b2"]["ip8"]["dx"],
    dpx_ip8=saved_vars["b2"]["ip8"]["dpx"],
    staged_match=staged_match,
    default_tol=default_tol,
)


# %%
optimizers["ir8b1"].target_status()
optimizers["ir8b2"].target_status()
# %%
print(optimizers["ir8b1"].log())
print(optimizers["ir8b2"].log())

# %%
optimizers["ir4b1"] = lm.rematch_ir4(
    collider=collider,
    line_name="lhcb1",
    boundary_conditions_left=tw_non_ats_arcs["lhcb1"]["34"],
    boundary_conditions_right=tw_sq_a45_ip5_a56_b1,
    mux_ir4=mux_ir4_target_b1,
    muy_ir4=muy_ir4_target_b1,
    alfx_ip4=saved_vars["b1"]["ip4"]["alfx"],
    alfy_ip4=saved_vars["b1"]["ip4"]["alfy"],
    betx_ip4=saved_vars["b1"]["ip4"]["betx"],
    bety_ip4=saved_vars["b1"]["ip4"]["bety"],
    dx_ip4=saved_vars["b1"]["ip4"]["dx"],
    dpx_ip4=saved_vars["b1"]["ip4"]["dpx"],
    solve=True,
    staged_match=staged_match,
    default_tol=default_tol,
)

optimizers["ir4b2"] = lm.rematch_ir4(
    collider=collider,
    line_name="lhcb2",
    boundary_conditions_left=tw_non_ats_arcs["lhcb2"]["34"],
    boundary_conditions_right=tw_sq_a45_ip5_a56_b2,
    mux_ir4=mux_ir4_target_b2,
    muy_ir4=muy_ir4_target_b2,
    alfx_ip4=saved_vars["b2"]["ip4"]["alfx"],
    alfy_ip4=saved_vars["b2"]["ip4"]["alfy"],
    betx_ip4=saved_vars["b2"]["ip4"]["betx"],
    bety_ip4=saved_vars["b2"]["ip4"]["bety"],
    dx_ip4=saved_vars["b2"]["ip4"]["dx"],
    dpx_ip4=saved_vars["b2"]["ip4"]["dpx"],
    solve=True,
    staged_match=staged_match,
    default_tol=default_tol,
)
# %%
optimizers["ir4b1"].target_status()
optimizers["ir4b2"].target_status()
# %%
print(optimizers["ir4b1"].log())
print(optimizers["ir4b2"].log())
# %%
optimizers["ir6b1"] = rematch_ir6(
    collider=collider,
    line_name="lhcb1",
    boundary_conditions_left=tw_sq_a45_ip5_a56_b1,
    boundary_conditions_right=tw_non_ats_arcs["lhcb1"]["67"],
    mux_ir6=mux_ir6_target_b1,
    muy_ir6=muy_ir6_target_b1,
    alfx_ip6=saved_vars["b1"]["ip6"]["alfx"],
    alfy_ip6=saved_vars["b1"]["ip6"]["alfy"],
    betx_ip6=saved_vars["b1"]["ip6"]["betx"],
    bety_ip6=saved_vars["b1"]["ip6"]["bety"],
    dx_ip6=saved_vars["b1"]["ip6"]["dx"],
    dpx_ip6=saved_vars["b1"]["ip6"]["dpx"],
    solve=True,
    staged_match=staged_match,
    default_tol=default_tol,
)

# %%
optimizers["ir6b2"] = rematch_ir6(
    collider=collider,
    line_name="lhcb2",
    boundary_conditions_left=tw_sq_a45_ip5_a56_b2,
    boundary_conditions_right=tw_non_ats_arcs["lhcb2"]["67"],
    mux_ir6=mux_ir6_target_b2,
    muy_ir6=muy_ir6_target_b2,
    alfx_ip6=saved_vars["b2"]["ip6"]["alfx"],
    alfy_ip6=saved_vars["b2"]["ip6"]["alfy"],
    betx_ip6=saved_vars["b2"]["ip6"]["betx"],
    bety_ip6=saved_vars["b2"]["ip6"]["bety"],
    dx_ip6=saved_vars["b2"]["ip6"]["dx"],
    dpx_ip6=saved_vars["b2"]["ip6"]["dpx"],
    solve=True,
    staged_match=staged_match,
    default_tol=default_tol,
)
# %%
optimizers["ir6b1"].target_status()
optimizers["ir6b2"].target_status()
# %%
print(optimizers["ir6b1"].log())
print(optimizers["ir6b2"].log())


# %%
optimizers["orbit_knobs"] = lm.match_orbit_knobs_ip2_ip8(collider)
# optimizers["orbit_knobs"] = match_orbit_knobs_ip2_ip8(collider, first_pass=first_pass)

# %%
for key, val in optimizers["orbit_knobs"].items():
    print(20 * "#", key, 20 * "#")
    print(val.log())

# %%
set_var_limits_and_steps_orbit_ip15(collider)
optimizers["orbit_knobs_ip15"] = match_orbit_knobs_ip15(collider, first_pass=True)
# %%
nrj = 7000
z_crab = 1e-3
opts, crabh_angle_max, crabv_angle_max = match_all_cc(collider, reset_values=True)
print(f"{crabh_angle_max=}, {crabv_angle_max=}")
# %%
optimizers["tune"] = rematch_tune(collider, 62.31, 60.32, 62.31, 60.32, solve=True)
optimizers["chroma"] = rematch_chroma(collider, 2, 2, 2, 2, solve=True)

# %%
optimizers["tune"]["b1"].target_status()
optimizers["tune"]["b2"].target_status()
optimizers["chroma"]["b1"].target_status()
optimizers["chroma"]["b2"].target_status()

# %%
print(optimizers["tune"]["b1"].log())
print(optimizers["tune"]["b2"].log())
print(optimizers["chroma"]["b1"].log())
print(optimizers["chroma"]["b2"].log())


# %%


# %%

# disable_crossing_all(collider)
enable_crossing_all(collider)

twb1 = collider["lhcb1"].cycle("ip3").twiss(method="4d")
twb2 = collider["lhcb2"].cycle("ip3").twiss(method="4d")

# %%
print(twb1[["betx", "bety", "x", "y", "px", "py"], "ip.*"])
print(twb2[["betx", "bety", "x", "y", "px", "py"], "ip.*"])

# %%
rra = "s.ds.l5.b1"
rrb = "e.ds.r5.b1"
rra = None
rrb = None

fig, axs = plt.subplots(figsize=(11, 11))
axs.plot(
    twb1["s", rra:rrb],
    twb1["x", rra:rrb],
    label=r"$x$, $\beta_x^*$ new",
)
axs.plot(
    twb1["s", rra:rrb],
    twb1["y", rra:rrb],
    label=r"$y$, $\beta_y^*$ new",
)
axs.plot(
    tw0b1["s", rra:rrb],
    tw0b1["x", rra:rrb],
    label=r"$x$, $\beta_x^*$ old",
    ls="--",
)
axs.plot(
    tw0b1["s", rra:rrb],
    tw0b1["y", rra:rrb],
    label=r"$y$, $\beta_y^*$ old",
    ls="--",
)
axs_t = axs.twiny()
axs_t.set_xticks(
    tw0b1[["s"], "ip.*"],
    tw0b1[["name"], "ip.*"],
)
axs_t.set_xlim(axs.get_xlim())
[axs_t.axvline(xpos, linestyle="--") for xpos in tw0b1[["s"], "ip.*"]]
axs.set_xlabel("s [m]")
axs.set_ylabel(r"$CO [m]$")
axs.legend()
plt.show()


# %%
os.makedirs("optics_xsuite", exist_ok=True)
save_optics_hl(collider, f"/tmp/opt_round_1000_cc_angle_optim_part3.madx")
# save_optics_hl(
#     collider,
#     f"optics_xsuite/opt_flathv_{int(bety_ip1*1000)}_{int(betx_ip1*1000)}_bet0_{betx_ip5}.madx",
# )

# %%


# %%
tw81_45_xt = lm.get_arc_periodic_solution(collider, arc_name=["81", "45"])

beta0 = tw81_45_xt["lhcb1"]["45"].get_twiss_init("s.ds.l5.b1")
beta0.mux = 0
beta0.muy = 0
tw_ip5_pre = collider.lhcb1.twiss(
    ele_start="s.ds.l5.b1", ele_stop="e.ds.r5.b1", twiss_init=beta0
)


beta0 = tw81_45_xt["lhcb1"]["81"].get_twiss_init("s.ds.l1.b1")
beta0.mux = 0
beta0.muy = 0
tw_ip1_pre = collider.lhcb1.cycle("ip3").twiss(
    ele_start="s.ds.l1.b1", ele_stop="e.ds.r1.b1", twiss_init=beta0
)

print(tw_ip5_pre[["betx", "bety"], "ip5"])
print(tw_ip1_pre[["betx", "bety"], "ip1"])

# %%
rra = "s.ds.l5.b1"
rrb = "e.ds.r5.b1"
rra = None
rrb = None

fig, axs = plt.subplots(figsize=(11, 11))
axs.plot(
    twb1["s", rra:rrb],
    twb1["betx", rra:rrb],
    label=r"$\beta_x$, $\beta_x^*$ new",
    color="black",
)
axs.plot(
    twb1["s", rra:rrb],
    twb1["bety", rra:rrb],
    label=r"$\beta_y$, $\beta_y^*$ new",
    color="red",
)
axs.plot(
    tw0b1["s", rra:rrb],
    tw0b1["betx", rra:rrb],
    label=r"$\beta_x$, $\beta_x^*$ old",
    ls="--",
    color="grey",
)
axs.plot(
    tw0b1["s", rra:rrb],
    tw0b1["bety", rra:rrb],
    label=r"$\beta_y$, $\beta_y^*$ old",
    ls="--",
    color="blue",
)
axs_t = axs.twiny()
axs_t.set_xticks(
    tw0b1[["s"], "ip.*"],
    tw0b1[["name"], "ip.*"],
)
axs_t.set_xlim(axs.get_xlim())
[axs_t.axvline(xpos, linestyle="--") for xpos in tw0b1[["s"], "ip.*"]]
axs.set_xlabel("s [m]")
axs.set_ylabel(r"$\beta_{x,y}$ [m]")
axs.legend()
plt.show()
# %%
