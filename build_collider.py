import xtrack as xt
import xpart as xp
import xmask as xm

from cpymad.madx import Madx

links = {"hl16": "../acc-models-lhc"}
xm.make_mad_environment(links=links)


optics_filepath = "../summer_studies/temp_optics/opt_round_1100_1500.madx"
# optics_filepath = '../summer_studies/collapse/opt_collapse_700_1500.madx'


mad1 = Madx()
mad1.call("hl16/lhc.seq")
mad1.call("hl16/hllhc_sequence.madx")
mad1.input("l.mbh = 0.0010;")
mad1.input("beam, sequence=lhcb1, particle=proton, energy=7000;")
mad1.use("lhcb1")
mad1.call(optics_filepath)
mad1.twiss()

mad4 = Madx()
mad4.input("mylhcbeam=4")
mad4.call("hl16/lhcb4.seq")
mad4.call("hl16/hllhc_sequence.madx")
mad4.input("l.mbh = 0.0010;")
mad4.input("beam, sequence=lhcb2, particle=proton, energy=7000;")
mad4.use("lhcb2")
mad4.call(optics_filepath)
mad4.twiss()

line1 = xt.Line.from_madx_sequence(
    mad1.sequence.lhcb1,
    allow_thick=True,
    deferred_expressions=True,
    replace_in_expr={"bv_aux": "bvaux_b1"},
)
line1.particle_ref = xp.Particles(mass0=xp.PROTON_MASS_EV, p0c=7000e9)

line4 = xt.Line.from_madx_sequence(
    mad4.sequence.lhcb2,
    allow_thick=True,
    deferred_expressions=True,
    replace_in_expr={"bv_aux": "bvaux_b2"},
)
line4.particle_ref = xp.Particles(mass0=xp.PROTON_MASS_EV, p0c=7000e9)

collider = xt.Multiline(lines={"lhcb1": line1, "lhcb2": line4})
collider.lhcb1.particle_ref = xp.Particles(mass0=xp.PROTON_MASS_EV, p0c=7000e9)
collider.lhcb2.particle_ref = xp.Particles(mass0=xp.PROTON_MASS_EV, p0c=7000e9)

collider.lhcb1.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["reverse"] = True

collider.build_trackers()

# collider.to_json("collider_from_madx.json")
collider.to_json("opt_round_1100_1500_thick.json")
