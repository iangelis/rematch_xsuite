# %%
import matplotlib.pyplot as plt
from misc import *
import os

try:
    get_ipython().run_line_magic("matplotlib", "inline")
except:
    pass

# %%
hpre = os.getenv("HOME")
prefix = f"{hpre}/Projects/hllhc_optics/"

optics_name = (
    prefix
    # + "summer_studies/collapse/opt_collapse_flathv_900_1800_1500.madx"
    # + "summer_studies/collapse/opt_collapse_flathv_700_2800.madx"
    # + "summer_studies/collapse/opt_flathv_600_1200_1500.madx"
    # + "summer_studies/collapse/opt_flathv_450_1800_1500.madx"
    # + "acc-models-lhc/strengths/round/opt_round_150_1500.madx"
    # + "acc-models-lhc/strengths/flat/opt_flathv_500_2000.madx"
    # + "/summer_studies/SoL/opt_flathv_300_1200_1500_thin.madx"
    # + "/summer_studies/SoL/opt_levelling_580_1500.madx"
    # + 'opt_round_150_1500_test_w.madx'
    # + "/summer_studies/collapse/opt_collapse_700_1500_ats.madx"
    # + "/summer_studies/collapse/opt_collapse_1100_1500.madx"
    + "/madx_vs_xsuite_rematch/madx/opt_round_1100_cc_angle_optim_again.madx"
)

# optics_name = "optics_xsuite/opt_round_1100_ats_500.madx"

# optics_name = "optics_xsuite/opt_flathv_700_2800_asym_700_1400_finish_madx.madx"
simple_name = optics_name.split("/")[-1].split(".")[0] + ".json"

# %%
collider = None
collider_name = "collider_hl16.json"
if os.path.exists(collider_name):
    collider = xt.Multiline.from_json(collider_name)
else:
    collider = build_collider(optics_name)
    collider.to_json(collider_name)

# %%
collider.vars.load_madx_optics_file(optics_name)

# %%
collider.lhcb1.particle_ref = xp.Particles(p0c=7000e9, q0=1, mass0=xp.PROTON_MASS_EV)
collider.lhcb2.particle_ref = xp.Particles(p0c=7000e9, q0=1, mass0=xp.PROTON_MASS_EV)
collider.build_trackers()

# %%
collider.lhcb1.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["reverse"] = True
# %%
tw0 = collider.twiss(method="4d")
print(tw0["lhcb1"][["x", "y", "betx", "bety", "px", "py"], ["ip1", "ip5"]])
print(tw0["lhcb2"][["x", "y", "betx", "bety", "px", "py"], ["ip1", "ip5"]])

# %%
tw1 = collider.twiss(method="4d", delta0=27.0e-5)
print(tw1["lhcb1"][["x", "y", "betx", "bety", "px", "py"], ["ip1", "ip5"]])
print(tw1["lhcb2"][["x", "y", "betx", "bety", "px", "py"], ["ip1", "ip5"]])

# %%
tw2 = collider["lhcb1"].cycle("ip3").twiss(method="4d")
# %%

# %%
fig, axs = plt.subplots(figsize=(11, 11))
axs.set_title("IP5")
axs.plot(tw2["s", :], tw2["betx", :], label=r"$\beta_x$")
axs.plot(tw2["s", :], tw2["bety", :], label=r"$\beta_y$")
axs_t = axs.twiny()
axs_t.set_xticks(tw2[["s"], "ip.*"], tw2[["name"], "ip.*"])
axs_t.set_xlim(axs.get_xlim())
axs.set_xlabel("s [m]")
axs.set_ylabel(r"$\beta_{x,y}$ [m]")
axs.legend()
axs.grid()
plt.show()
# %%
fig, axs = plt.subplots(figsize=(21, 11))
axs.plot(tw2.s, tw2.wx_chrom, label="Wx", color="black")
axs.plot(tw2.s, tw2.wy_chrom, label="Wy", color="red")
axs.legend()
axs_t = axs.twiny()
axs_t.set_xticks(tw2[["s"], "ip.*"], tw2[["name"], "ip.*"])

axs.set_xlabel("s [m]")
axs.set_ylabel(r"$W_{x,y}$ [m]")
plt.show()

# %%
plot_w(collider, "lhcb1")
# plot_w(collider, "lhcb2")
# %%
sextupoles_to_check = (
    "ksf1.a81b1 ksd2.a81b1 "
    "ksf1.a12b1 ksd2.a12b1 "
    "ksf1.a45b1 ksd2.a45b1 "
    "ksf1.a56b1 ksd2.a56b1"
)

for isx in sextupoles_to_check.split():
    print(f"{isx} = {collider.varval[isx]}")
# %%
# collider.varval["ksf1.a81b1"] = 0.2635559458914279
# collider.varval["ksd2.a81b1"] = -0.379999999998
# collider.varval["ksf1.a12b1"] = 0.2241928826594957
# collider.varval["ksd2.a12b1"] = -0.3627471207546273
# collider.varval["ksf1.a45b1"] = 0.219851443573283
# collider.varval["ksd2.a45b1"] = -0.3473348056188917
# collider.varval["ksf1.a56b1"] = 0.216123689178818
# collider.varval["ksd2.a56b1"] = -0.3667444153795705
# %%
line1 = collider["lhcb1"].cycle("ip3")
tw = line1.twiss(method="4d", only_markers=True)
start_range = "mbxf.4l5_entry"
end_range = "ip5"

twiss_init = tw.get_twiss_init(start_range)
twiss_init.ax_chrom = 0
twiss_init.bx_chrom = 0
twiss_init.ay_chrom = 0
twiss_init.by_chrom = 0
tw_l5 = line1.twiss(
    ele_start=start_range,
    ele_stop=end_range,
    twiss_init=twiss_init,
    compute_chromatic_properties=True,
)

# Measure chromaticities of ITR5
start_range = "ip5"
end_range = "mbxf.4r5_exit"
tw = line1.twiss(method="4d", only_markers=True)
twiss_init = tw.get_twiss_init(end_range)
twiss_init.ax_chrom = 0
twiss_init.bx_chrom = 0
twiss_init.ay_chrom = 0
twiss_init.by_chrom = 0
tw_r5 = line1.twiss(
    ele_start=start_range,
    ele_stop=end_range,
    twiss_init=twiss_init,
    compute_chromatic_properties=True,
)

print(f"ITL5: dqx = {tw_l5.dmux[-1]:.2f}    dqy = {tw_l5.dmuy[-1]:.2f}")
print(f"ITR5: dqx = {tw_r5.dmux[-1]:.2f}    dqy = {tw_r5.dmuy[-1]:.2f}")

# %%
tar_ix1 = -0.5 * (tw_l5.dmux[-1] - tw_r5.dmux[-1])
tar_iy1 = 0.5 * (tw_l5.dmuy[-1] - tw_r5.dmuy[-1])
tar_ix5 = -0.5 * (tw_l5.dmux[-1] - tw_r5.dmux[-1])
tar_iy5 = 0.5 * (tw_l5.dmuy[-1] - tw_r5.dmuy[-1])
print(tar_ix1, tar_iy1, tar_ix5, tar_iy5)

# %%

k2max = 0.38
# %%
# set_sext_all(collider, 0.06, -0.099, 0.06, -0.099)
# set_sext_all(collider, k2max, -k2max/10, k2max, -k2max/10)
# %%
sextupoles_defaults = {
    # Beam 1
    "ksf1.a81b1": {"step": 1e-4, "limits": (-k2max, k2max)},
    # "ksd1.a81b1": {"step": 1e-4, "limits": (-k2max, k2max)},
    "ksd2.a81b1": {"step": 1e-4, "limits": (-k2max, k2max)},
    "ksf1.a12b1": {"step": 1e-4, "limits": (-k2max, k2max)},
    "ksd2.a12b1": {"step": 1e-4, "limits": (-k2max, k2max)},
    "ksf1.a45b1": {"step": 1e-4, "limits": (-k2max, k2max)},
    "ksd2.a45b1": {"step": 1e-4, "limits": (-k2max, k2max)},
    "ksf1.a56b1": {"step": 1e-4, "limits": (-k2max, k2max)},
    # "ksd1.a56b1": {"step": 1e-4, "limits": (-k2max, k2max)},
    "ksd2.a56b1": {"step": 1e-4, "limits": (-k2max, k2max)},
}
collider.vars.vary_default.update(sextupoles_defaults)
# %%
opt = collider["lhcb1"].match(
    solve=False,
    assert_within_tol=False,
    restore_if_fail=False,
    verbose=False,
    solver_options={"n_bisections": 1, "min_step": 1e-10, "n_steps_max": 20},
    # default_tol=default_tol,
    vary=[
        xt.Vary("ksf1.a81b1"),
        xt.Vary("ksd2.a81b1"),
        xt.Vary("ksf1.a12b1"),
        xt.Vary(tag="w", name="ksd2.a12b1"),
        xt.Vary("ksf1.a45b1"),
        xt.Vary("ksd2.a45b1"),
        xt.Vary("ksf1.a56b1"),
        xt.Vary(tag="w", name="ksd2.a56b1"),
        # xt.Vary(tag="w2", name="ksd1.a12b1"),
        # xt.Vary(tag="w2", name="ksd1.a56b1"),
    ],
    targets=[
        # xt.Target(
        #     lambda tt: tt["wx_chrom", "ip3"] - tt["wx_chrom", "ip7"], value=0, tol=1e-2
        # ),
        # xt.Target(
        #     lambda tt: tt["wy_chrom", "ip3"] - tt["wy_chrom", "ip7"], value=0, tol=1e-2
        # ),
        # IR3, IR7
        xt.Target(tag="bxy", at="ip3", tar="bx_chrom", value=0),
        xt.Target(tag="bxy", at="ip3", tar="by_chrom", value=0),
        xt.Target(tag="axy", at="ip3", tar="ax_chrom", value=0, tol=1e-2),
        xt.Target(tag="axy", at="ip3", tar="ay_chrom", value=0, tol=1e-2),
        xt.Target(tag="bxy", at="ip7", tar="bx_chrom", value=0),
        xt.Target(tag="bxy", at="ip7", tar="by_chrom", value=0),
        xt.Target(tag="axy", at="ip7", tar="ax_chrom", value=0, tol=1e-2),
        xt.Target(tag="axy", at="ip7", tar="ay_chrom", value=0, tol=1e-2),
        # IP1, IP5
        xt.Target(tag="bxy", at="ip1", tar="bx_chrom", value=0),
        xt.Target(tag="bxy", at="ip1", tar="by_chrom", value=0),
        xt.Target(tag="axy", at="ip1", tar="ax_chrom", value=tar_ix1, tol=1e-2),
        xt.Target(tag="axy", at="ip1", tar="ay_chrom", value=-tar_iy1, tol=1e-2),
        xt.Target(tag="bxy", at="ip5", tar="bx_chrom", value=0),
        xt.Target(tag="bxy", at="ip5", tar="by_chrom", value=0),
        xt.Target(tag="axy", at="ip5", tar="ax_chrom", value=tar_ix5, tol=1e-2),
        xt.Target(tag="axy", at="ip5", tar="ay_chrom", value=-tar_iy5, tol=1e-2),
        xt.Target(at="ms.10l1.b1", tar="bx_chrom", value=xt.GreaterThan(500)),
        xt.Target(at="ms.10r1.b1", tar="bx_chrom", value=xt.GreaterThan(500)),
    ],
)

opt.disable_vary(tag="w2")

# %%
# %%
# opt.disable_all_targets()
# opt.enable_targets(tag="bxy")
# %%
# opt.step(10)
# %%
opt.vary_status()
# %%
opt.target_status()
# %%
opt.enable_all_targets()
# %%
opt.solve()
# %%
opt.target_status()
# %%
# print(opt.show())
print(opt.log())

# %%
sextupoles_to_check = [
    "ksf1.a81b1",
    "ksd2.a81b1",
    "ksf1.a12b1",
    "ksd2.a12b1",
    "ksf1.a45b1",
    "ksd2.a45b1",
    "ksf1.a56b1",
    "ksd2.a56b1",
]

for isx in sextupoles_to_check:
    print(f"{isx} = {collider.varval[isx]}")
# %%
rematch_chroma(collider, 2, 2, 2, 2, True)
# %%
tw3 = collider["lhcb1"].cycle("ip3").twiss(method="4d")
# tw3 = collider["lhcb2"].cycle("ip3").twiss(method="4d", reverse=True)

# %%
fig, axs = plt.subplots(2, 1, figsize=(21, 15))
axs[0].plot(tw2["s", :], tw2["wx_chrom", :], color="black", label=r"before")
axs[0].plot(
    tw3["s", :], tw3["wx_chrom", :], color="red", linestyle="--", label=r"after"
)
axs[0].axhline(tw3.wx_chrom[0], linestyle="--", color="lightgray")


axs[0].set_ylabel(r"$W_{x}$")

axs[1].plot(tw2["s", :], tw2["wy_chrom", :], color="black", label=r"before")
axs[1].plot(
    tw3["s", :], tw3["wy_chrom", :], color="red", linestyle="--", label=r"after"
)
axs[1].axhline(tw3.wy_chrom[0], linestyle="--", color="lightgray")
axs[1].set_ylabel(r"$W_{y}$")

for i in range(2):
    axs_t = axs[i].twiny()
    axs_t.set_xticks(
        tw3[["s"], "ip.*"],
        tw3[["name"], "ip.*"],
    )
    axs_t.set_xlim(axs[i].get_xlim())
    [axs_t.axvline(xpos, linestyle="--") for xpos in tw3[["s"], "ip.*"]]
    axs[i].set_xlabel("s [m]")
    axs[i].legend()
    axs[i].axvline(tw3[["s"], "ms.10l1.b1"], color="lightgray", linestyle="--")
# for i in range(10, 28):
#     for j in range(2):
#         for iip in [1, 5]:
#             for dd in ["l", "r"]:
#                 axs[j].axvline(
#                     tw3[["s"], f"ms.{i}{dd}{iip}.b1"], color="lightgray", linestyle="--"
#                 )

# for i in range(11, 28):
#     for j in range(2):
#         for iip in [8]:
#             for dd in ["l", "r"]:
#                 axs[j].axvline(
#                     tw3[["s"], f"ms.{i}{dd}{iip}.b1"], color="lightgray", linestyle="--"
#                 )

plt.show()

# %%
fig, axs = plt.subplots(2, 1, figsize=(11, 15))
axs[0].plot(tw2["s", :], tw2["ax_chrom", :], color="black", label=r"before")
axs[0].plot(
    tw3["s", :], tw3["ax_chrom", :], color="red", linestyle="--", label=r"after"
)
axs[0].set_ylabel(r"$A_{x}$")
axs[1].plot(tw2["s", :], tw2["ay_chrom", :], color="black", label=r"before")
axs[1].plot(
    tw3["s", :], tw3["ay_chrom", :], color="red", linestyle="--", label=r"after"
)
axs[1].set_ylabel(r"$A_{y}$")

for i in range(2):
    axs_t = axs[i].twiny()
    axs_t.set_xticks(
        tw3[["s"], "ip.*"],
        tw3[["name"], "ip.*"],
    )
    axs_t.set_xlim(axs[i].get_xlim())
    axs[i].set_xlabel("s [m]")
    [axs_t.axvline(xpos, linestyle="--") for xpos in tw3[["s"], "ip.*"]]

    axs[i].legend()
plt.show()

# %%
fig, axs = plt.subplots(2, 1, figsize=(11, 15))
axs[0].plot(tw2["s", :], tw2["bx_chrom", :], color="black", label=r"before")
axs[0].plot(
    tw3["s", :], tw3["bx_chrom", :], color="red", linestyle="--", label=r"after"
)
axs[0].set_ylabel(r"$B_{x}$")
axs[1].plot(tw2["s", :], tw2["by_chrom", :], color="black", label=r"before")
axs[1].plot(
    tw3["s", :], tw3["by_chrom", :], color="red", linestyle="--", label=r"after"
)
axs[1].set_ylabel(r"$B_{y}$")

for i in range(2):
    axs_t = axs[i].twiny()
    axs_t.set_xticks(
        tw3[["s"], "ip.*"],
        tw3[["name"], "ip.*"],
    )
    axs_t.set_xlim(axs[i].get_xlim())
    [axs_t.axvline(xpos, linestyle="--") for xpos in tw3[["s"], "ip.*"]]
    axs[i].set_xlabel("s [m]")
    axs[i].legend()
plt.show()

# %%
fig, axs = plt.subplots(2, 1, figsize=(11, 15))
axs[0].plot(tw2["s", :], tw2["dmux", :], color="black", label=r"before")
axs[0].plot(tw3["s", :], tw3["dmux", :], color="red", linestyle="--", label=r"after")
axs[0].set_ylabel(r"$d\mu_{x}$")
axs[1].plot(tw2["s", :], tw2["dmuy", :], color="black", label=r"before")
axs[1].plot(tw3["s", :], tw3["dmuy", :], color="red", linestyle="--", label=r"after")
axs[1].set_ylabel(r"$d\mu_{y}$")

for i in range(2):
    axs_t = axs[i].twiny()
    axs_t.set_xticks(
        tw3[["s"], "ip.*"],
        tw3[["name"], "ip.*"],
    )
    axs_t.set_xlim(axs[i].get_xlim())
    axs[i].set_xlabel("s [m]")
    [axs_t.axvline(xpos, linestyle="--") for xpos in tw3[["s"], "ip.*"]]

    axs[i].legend()
plt.show()

# %%
print(tw3.dqx, tw3.dqy)
# %%


# %%


collider.varval["ksd1.a12b1"] = collider2.varval["ksd1.a12b1"]
collider.varval["ksd1.a56b1"] = collider2.varval["ksd1.a56b1"]
collider.varval["ksd2.a12b1"] = collider2.varval["ksd2.a12b1"]
collider.varval["ksd2.a56b1"] = collider2.varval["ksd2.a56b1"]
collider.varval["ksf1.a12b1"] = collider2.varval["ksf1.a12b1"]
collider.varval["ksf1.a56b1"] = collider2.varval["ksf1.a56b1"]
collider.varval["ksf2.a12b1"] = collider2.varval["ksf2.a12b1"]
collider.varval["ksf2.a56b1"] = collider2.varval["ksf2.a56b1"]
collider.varval["ksd1.a12b2"] = collider2.varval["ksd1.a12b2"]
collider.varval["ksd1.a56b2"] = collider2.varval["ksd1.a56b2"]
collider.varval["ksd2.a12b2"] = collider2.varval["ksd2.a12b2"]
collider.varval["ksd2.a56b2"] = collider2.varval["ksd2.a56b2"]
collider.varval["ksf1.a12b2"] = collider2.varval["ksf1.a12b2"]
collider.varval["ksf1.a56b2"] = collider2.varval["ksf1.a56b2"]
collider.varval["ksf2.a12b2"] = collider2.varval["ksf2.a12b2"]
collider.varval["ksf2.a56b2"] = collider2.varval["ksf2.a56b2"]

collider.vars["ksd1.a23b1"] = -0.1288719607855443 + (1.0 * collider.vars["ksd.b1"])
collider.vars["ksd1.a67b1"] = -0.1288719607855443 + (1.0 * collider.vars["ksd.b1"])
collider.vars["ksd2.a23b1"] = -0.1288719607855443 + (1.0 * collider.vars["ksd.b1"])
collider.vars["ksd2.a67b1"] = -0.1288719607855443 + (1.0 * collider.vars["ksd.b1"])
collider.vars["ksf1.a23b1"] = 0.06137927073224231 + (1.0 * collider.vars["ksf.b1"])
collider.vars["ksf1.a67b1"] = 0.06137927073224231 + (1.0 * collider.vars["ksf.b1"])
collider.vars["ksf2.a23b1"] = 0.06137927073224231 + (1.0 * collider.vars["ksf.b1"])
collider.vars["ksf2.a67b1"] = 0.06137927073224231 + (1.0 * collider.vars["ksf.b1"])
collider.vars["ksd1.a23b2"] = -0.1573001724761903 + (1.0 * collider.vars["ksd.b2"])
collider.vars["ksd1.a67b2"] = -0.1573001724761903 + (1.0 * collider.vars["ksd.b2"])
collider.vars["ksd2.a23b2"] = -0.1573001724761903 + (1.0 * collider.vars["ksd.b2"])
collider.vars["ksd2.a67b2"] = -0.1573001724761903 + (1.0 * collider.vars["ksd.b2"])
collider.vars["ksf1.a23b2"] = 0.1084208029218782 + (1.0 * collider.vars["ksf.b2"])
collider.vars["ksf1.a67b2"] = 0.1084208029218782 + (1.0 * collider.vars["ksf.b2"])
collider.vars["ksf2.a23b2"] = 0.1084208029218782 + (1.0 * collider.vars["ksf.b2"])
collider.vars["ksf2.a67b2"] = 0.1084208029218782 + (1.0 * collider.vars["ksf.b2"])

collider.vars["ksd1.a34b1"] = -0.1288719607855443 + (1.0 * collider.vars["ksd.b1"])
collider.vars["ksd1.a78b1"] = -0.1288719607855443 + (1.0 * collider.vars["ksd.b1"])
collider.vars["ksd2.a34b1"] = -0.1288719607855443 + (1.0 * collider.vars["ksd.b1"])
collider.vars["ksd2.a78b1"] = -0.1288719607855443 + (1.0 * collider.vars["ksd.b1"])
collider.vars["ksf1.a34b1"] = 0.06137927073224231 + (1.0 * collider.vars["ksf.b1"])
collider.vars["ksf1.a78b1"] = 0.06137927073224231 + (1.0 * collider.vars["ksf.b1"])
collider.vars["ksf2.a34b1"] = 0.06137927073224231 + (1.0 * collider.vars["ksf.b1"])
collider.vars["ksf2.a78b1"] = 0.06137927073224231 + (1.0 * collider.vars["ksf.b1"])
collider.vars["ksd1.a34b2"] = -0.1573001724761903 + (1.0 * collider.vars["ksd.b2"])
collider.vars["ksd1.a78b2"] = -0.1573001724761903 + (1.0 * collider.vars["ksd.b2"])
collider.vars["ksd2.a34b2"] = -0.1573001724761903 + (1.0 * collider.vars["ksd.b2"])
collider.vars["ksd2.a78b2"] = -0.1573001724761903 + (1.0 * collider.vars["ksd.b2"])
collider.vars["ksf1.a34b2"] = 0.1084208029218782 + (1.0 * collider.vars["ksf.b2"])
collider.vars["ksf1.a78b2"] = 0.1084208029218782 + (1.0 * collider.vars["ksf.b2"])
collider.vars["ksf2.a34b2"] = 0.1084208029218782 + (1.0 * collider.vars["ksf.b2"])
collider.vars["ksf2.a78b2"] = 0.1084208029218782 + (1.0 * collider.vars["ksf.b2"])

collider.varval["ksd1.a45b1"] = collider2.varval["ksd1.a45b1"]
collider.varval["ksd1.a81b1"] = collider2.varval["ksd1.a81b1"]
collider.varval["ksd2.a45b1"] = collider2.varval["ksd2.a45b1"]
collider.varval["ksd2.a81b1"] = collider2.varval["ksd2.a81b1"]
collider.varval["ksf1.a45b1"] = collider2.varval["ksf1.a45b1"]
collider.varval["ksf1.a81b1"] = collider2.varval["ksf1.a81b1"]
collider.varval["ksf2.a45b1"] = collider2.varval["ksf2.a45b1"]
collider.varval["ksf2.a81b1"] = collider2.varval["ksf2.a81b1"]
collider.varval["ksd1.a45b2"] = collider2.varval["ksd1.a45b2"]
collider.varval["ksd1.a81b2"] = collider2.varval["ksd1.a81b2"]
collider.varval["ksd2.a45b2"] = collider2.varval["ksd2.a45b2"]
collider.varval["ksd2.a81b2"] = collider2.varval["ksd2.a81b2"]
collider.varval["ksf1.a45b2"] = collider2.varval["ksf1.a45b2"]
collider.varval["ksf1.a81b2"] = collider2.varval["ksf1.a81b2"]
collider.varval["ksf2.a45b2"] = collider2.varval["ksf2.a45b2"]
collider.varval["ksf2.a81b2"] = collider2.varval["ksf2.a81b2"]


# print(collider2.vars["ksd1.a23b1"]._expr())
# print(collider2.vars["ksd1.a67b1"]._expr())
# print(collider2.vars["ksd2.a23b1"]._expr())
# print(collider2.vars["ksd2.a67b1"]._expr())
# print(collider2.vars["ksf1.a23b1"]._expr())
# print(collider2.vars["ksf1.a67b1"]._expr())
# print(collider2.vars["ksf2.a23b1"]._expr())
# print(collider2.vars["ksf2.a67b1"]._expr())
# print(collider2.vars["ksd1.a23b2"]._expr())
# print(collider2.vars["ksd1.a67b2"]._expr())
# print(collider2.vars["ksd2.a23b2"]._expr())
# print(collider2.vars["ksd2.a67b2"]._expr())
# print(collider2.vars["ksf1.a23b2"]._expr())
# print(collider2.vars["ksf1.a67b2"]._expr())
# print(collider2.vars["ksf2.a23b2"]._expr())
# print(collider2.vars["ksf2.a67b2"]._expr())

# print(collider2.vars["ksd1.a34b1"]._expr())
# print(collider2.vars["ksd1.a78b1"]._expr())
# print(collider2.vars["ksd2.a34b1"]._expr())
# print(collider2.vars["ksd2.a78b1"]._expr())
# print(collider2.vars["ksf1.a34b1"]._expr())
# print(collider2.vars["ksf1.a78b1"]._expr())
# print(collider2.vars["ksf2.a34b1"]._expr())
# print(collider2.vars["ksf2.a78b1"]._expr())
# print(collider2.vars["ksd1.a34b2"]._expr())
# print(collider2.vars["ksd1.a78b2"]._expr())
# print(collider2.vars["ksd2.a34b2"]._expr())
# print(collider2.vars["ksd2.a78b2"]._expr())
# print(collider2.vars["ksf1.a34b2"]._expr())
# print(collider2.vars["ksf1.a78b2"]._expr())
# print(collider2.vars["ksf2.a34b2"]._expr())
# print(collider2.vars["ksf2.a78b2"]._expr())

# %%
fig, axs = plt.subplots(figsize=(21, 15))
axs.plot(tw2["s", :], tw2["wx_chrom", :], color="black", label=r"$W_x$")
axs.set_ylabel(r"$W_{x,y}$")
axs.plot(tw2["s", :], tw2["wy_chrom", :], color="red", label=r"$W_y$")
axs_t = axs.twiny()
axs_t.set_xticks(
    tw3[["s"], "ip.*"],
    tw3[["name"], "ip.*"],
)
axs_t.set_xlim(axs.get_xlim())
[axs_t.axvline(xpos, linestyle="--") for xpos in tw3[["s"], "ip.*"]]
axs.set_xlabel("s [m]")
axs.legend()
plt.show()

# %%
fig, axs = plt.subplots(figsize=(21, 15))
axs.plot(tw2["s", :], tw2["ax_chrom", :], color="black", label=r"$A_x$")
axs.set_ylabel(r"$A_{x,y}$")
axs.plot(tw2["s", :], tw2["ay_chrom", :], color="red", label=r"$A_y$")
axs_t = axs.twiny()
axs_t.set_xticks(
    tw3[["s"], "ip.*"],
    tw3[["name"], "ip.*"],
)
axs_t.set_xlim(axs.get_xlim())
[axs_t.axvline(xpos, linestyle="--") for xpos in tw3[["s"], "ip.*"]]
axs.set_xlabel("s [m]")
axs.legend()
plt.show()


# %%
fig, axs = plt.subplots(figsize=(21, 15))
axs.plot(tw2["s", :], tw2["bx_chrom", :], color="black", label=r"$B_{x}$")
axs.set_ylabel(r"$B_{x,y}$")
axs.plot(tw2["s", :], tw2["by_chrom", :], color="red", label=r"$B_{y}$")
axs_t = axs.twiny()
axs_t.set_xticks(
    tw3[["s"], "ip.*"],
    tw3[["name"], "ip.*"],
)
axs_t.set_xlim(axs.get_xlim())
[axs_t.axvline(xpos, linestyle="--") for xpos in tw3[["s"], "ip.*"]]
axs.set_xlabel("s [m]")
axs.legend()
plt.show()

# %%
fig, axs = plt.subplots(figsize=(21, 15))
axs.plot(tw2["s", :], tw2["dmux", :], color="black", label=r"$\Delta \mu_{x}$")
axs.set_ylabel(r"$\Delta \mu_{x,y}$")
axs.plot(tw2["s", :], tw2["dmuy", :], color="red", label=r"$\Delta \mu_{y}$")
axs_t = axs.twiny()
axs_t.set_xticks(
    tw3[["s"], "ip.*"],
    tw3[["name"], "ip.*"],
)
axs_t.set_xlim(axs.get_xlim())
[axs_t.axvline(xpos, linestyle="--") for xpos in tw3[["s"], "ip.*"]]
axs.set_xlabel("s [m]")
axs.legend()
plt.show()

# %%
