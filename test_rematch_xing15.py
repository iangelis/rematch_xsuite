# %%
import matplotlib.pyplot as plt
import os

import xtrack as xt

from copy import copy

from misc import *

from step_and_limits import set_var_limits_and_steps_orbit_ip15

get_ipython().run_line_magic("matplotlib", "inline")
# get_ipython().run_line_magic("load_ext", "autoreload")
try:
    get_ipython().run_line_magic("autoreload", "2")
except:
    print("Not in a notebook")
from rematch_xing15 import *

# %%
# %%
default_tol = {
    None: 1e-8,
    "betx": 1e-6,
    "bety": 1e-6,
}  # to have no rematching w.r.t. madx

# %%
hpre = os.getenv("HOME")
prefix = f"{hpre}/Projects/hllhc_optics/"

# optics_name = prefix + "/test_cycle/opt_1200_1500_badxing.madx"
# optics_name = (
#     prefix + "/xsuite_rematch/optics_xsuite/opt_flathv_300_720_asymetric_again.madx"
# )
# optics_name = "optics_xsuite/opt_flathv_700_2800_asym_700_1400.madx"
# optics_name = "optics_xsuite/opt_flathv_700_2800_bet0_0.7.madx"
# optics_name = "../madx_vs_xsuite_rematch/madx/opt_round_1100_1500_again3.madx"
optics_name = "../madx_vs_xsuite_rematch/madx/opt_round_1100_cc_angle_optim_again.madx"

# %%
collider = None
collider_name = "collider_hl16.json"
if os.path.exists(collider_name):
    collider = xt.Multiline.from_json(collider_name)
else:
    collider = build_collider(optics_name, cycling=True)
    collider.to_json(collider_name)

# %%
collider.vars.load_madx_optics_file(optics_name)
# %%
tw0 = collider.twiss()
print(tw0["lhcb1"][["x", "y", "betx", "bety", "px", "py"], "ip.*"])
print(tw0["lhcb2"][["x", "y", "betx", "bety", "px", "py"], "ip.*"])


# %%
collider.lhcb1.cycle("ip3", inplace=True)
collider.lhcb2.cycle("ip3", inplace=True)
collider.lhcb1.twiss_default["only_markers"] = True
collider.lhcb2.twiss_default["only_markers"] = True

collider.lhcb1.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["reverse"] = True

# %%
set_var_limits_and_steps_orbit_ip15(collider)
opt_x15 = match_orbit_knobs_ip15(collider, first_pass=True)


# %%
def set_vars(collider, config):
    print(config)
    for key, val in config.items():
        collider.varval[key] = val


configs = {
    "on_0": {
        "cd2q4": 0,
        "on_x1": 0,
        "on_x5": 0,
        "on_sep1": 0,
        "on_sep5": 0,
        "phi_ir1": 0,
        "phi_ir5": 90,
        "on_a1": 0,
        "on_a5": 0,
    },
    "on_x1hs": {
        "cd2q4": 0,
        "on_x1": 250,
        "on_x5": 0,
        "on_sep1": 0,
        "phi_ir1": 0,
        "phi_ir5": 90,
    },
    "on_x1hl": {
        "cd2q4": 1,
        "on_x1": 250,
        "on_x5": 0,
        "on_sep1": 0,
        "phi_ir1": 0,
        "phi_ir5": 90,
    },
    "on_x1vs": {
        "cd2q4": 0,
        "on_x1": 250,
        "on_x5": 0,
        "on_sep1": 0,
        "phi_ir1": 90,
        "phi_ir5": 0,
    },
    "on_x1vl": {
        "cd2q4": 1,
        "on_x1": 250,
        "on_x5": 0,
        "on_sep1": 0,
        "phi_ir1": 90,
        "phi_ir5": 0,
    },
    "on_x5hs": {
        "cd2q4": 0,
        "on_x1": 0,
        "on_x5": 250,
        "on_sep1": 0,
        "phi_ir1": 90,
        "phi_ir5": 0,
    },
    "on_x5hl": {
        "cd2q4": 1,
        "on_x1": 0,
        "on_x5": 250,
        "on_sep1": 0,
        "phi_ir1": 90,
        "phi_ir5": 0,
    },
    "on_x5vs": {
        "cd2q4": 0,
        "on_x1": 0,
        "on_x5": 250,
        "on_sep1": 0,
        "phi_ir1": 0,
        "phi_ir5": 90,
    },
    "on_x5vl": {
        "cd2q4": 1,
        "on_x1": 0,
        "on_x5": 250,
        "on_sep1": 0,
        "phi_ir1": 0,
        "phi_ir5": 90,
    },
    "on_x15hvs": {
        "cd2q4": 0,
        "on_x1": 250,
        "on_x5": 250,
        "on_sep1": 0,
        "phi_ir1": 0,
        "phi_ir5": 90,
    },
    "on_x15hvl": {
        "cd2q4": 1,
        "on_x1": 250,
        "on_x5": 250,
        "on_sep1": 0,
        "phi_ir1": 0,
        "phi_ir5": 90,
    },
    "on_sep1v": {"on_sep1": 2, "phi_ir1": 0},
    "on_sep5h": {"on_sep5": 2, "phi_ir5": 90},
    "on_sep1h": {"on_sep1": 2, "phi_ir1": 90},
    "on_sep5v": {"on_sep5": 2, "phi_ir5": 0},
    "on_a1v": {"on_a1": 1},
    "on_a1h": {"on_a1": 1, "phi_ir1": 90},
    "on_a5h": {"on_a5": 1},
    "on_a5v": {"on_a5": 1, "phi_ir5": 0},
}


# %%
set_vars(collider, configs["on_0"])
# set_vars(collider, configs["on_x1hs"])
# set_vars(collider, configs["on_x1hl"])
# set_vars(collider, configs["on_x1vs"])
# set_vars(collider, configs["on_x1vl"])

# set_vars(collider, configs["on_x5hs"])
# set_vars(collider, configs["on_x5hl"])
# set_vars(collider, configs["on_x5vs"])
# set_vars(collider, configs["on_x5vl"])

# set_vars(collider, configs["on_sep1v"])
# set_vars(collider, configs["on_sep1h"])
# set_vars(collider, configs["on_sep5v"])
# set_vars(collider, configs["on_sep5h"])

# set_vars(collider, configs["on_x15hvs"])
# set_vars(collider, configs["on_x15hvl"])

# set_vars(collider, configs["on_a1v"])
# set_vars(collider, configs["on_a1h"])

# set_vars(collider, configs["on_a5h"])
# set_vars(collider, configs["on_a5v"])

tw1 = collider.twiss()
# %%
print(tw1["lhcb1"][["x", "y", "px", "py"], "ip.*"])
print(tw1["lhcb2"][["x", "y", "px", "py"], "ip.*"])

# %%
plot_orbit(tw1, "lhcb1")
plot_orbit(tw1, "lhcb2")

# %%
set_vars(collider, configs["on_0"])
# set_vars(collider, configs["on_x1hs"])
# set_vars(collider, configs["on_x5hs"])
set_vars(collider, configs["on_x15hvs"])
tw1 = collider.twiss()

set_vars(collider, configs["on_0"])
# set_vars(collider, configs["on_x1hl"])
# set_vars(collider, configs["on_x5hl"])
set_vars(collider, configs["on_x15hvl"])
tw2 = collider.twiss()

print(tw1["lhcb1"][["x", "y", "px", "py"], "ip.*"])
print(tw2["lhcb1"][["x", "y", "px", "py"], "ip.*"])
# %%
fig, axs = plt.subplots(2, 1, figsize=(21, 13))
axs[0].plot(tw1["lhcb1"]['s', 'e.ds.l1.b1':'s.ds.r1.b1'], tw1["lhcb1"]['x', 'e.ds.l1.b1':'s.ds.r1.b1'], label="x, on_x15hs", color="black")
axs[0].plot(tw1["lhcb1"]['s', 'e.ds.l1.b1':'s.ds.r1.b1'], tw1["lhcb1"]['y', 'e.ds.l1.b1':'s.ds.r1.b1'], label="y, on_x15hs", color="red")
axs[0].plot(tw2["lhcb1"]['s', 'e.ds.l1.b1':'s.ds.r1.b1'], tw2["lhcb1"]['x', 'e.ds.l1.b1':'s.ds.r1.b1'], label="x, on_x15hl", ls="--", color="black")
axs[0].plot(tw2["lhcb1"]['s', 'e.ds.l1.b1':'s.ds.r1.b1'], tw2["lhcb1"]['y', 'e.ds.l1.b1':'s.ds.r1.b1'], label="y, on_x15hl", ls="--", color="red")
axs_t = axs[0].twiny()
axs_t.set_xticks(
    tw1["lhcb1"][["s"], "ip.*"], tw1["lhcb1"][["name"], "ip.*"], rotation="horizontal"
)
axs_t.set_xlim(axs[0].get_xlim()[0], axs[0].get_xlim()[1])
axs[0].set_ylabel(r"$co [m]$", fontsize=22)
axs[0].set_xlabel(r"$s [m]$", fontsize=22)
axs[0].legend(fontsize=20)
axs[0].grid()

axs[1].plot(tw1["lhcb1"]['s', 'e.ds.l5.b1':'s.ds.r5.b1'], tw1["lhcb1"]['x', 'e.ds.l5.b1':'s.ds.r5.b1'], label="x, on_x15hs", color="black")
axs[1].plot(tw1["lhcb1"]['s', 'e.ds.l5.b1':'s.ds.r5.b1'], tw1["lhcb1"]['y', 'e.ds.l5.b1':'s.ds.r5.b1'], label="y, on_x15hs", color="red")
axs[1].plot(tw2["lhcb1"]['s', 'e.ds.l5.b1':'s.ds.r5.b1'], tw2["lhcb1"]['x', 'e.ds.l5.b1':'s.ds.r5.b1'], label="x, on_x15hl", ls="--", color="black")
axs[1].plot(tw2["lhcb1"]['s', 'e.ds.l5.b1':'s.ds.r5.b1'], tw2["lhcb1"]['y', 'e.ds.l5.b1':'s.ds.r5.b1'], label="y, on_x15hl", ls="--", color="red")
axs_t = axs[1].twiny()
axs_t.set_xticks(
    tw1["lhcb1"][["s"], "ip.*"], tw1["lhcb1"][["name"], "ip.*"], rotation="horizontal"
)
axs_t.set_xlim(axs[1].get_xlim()[0], axs[1].get_xlim()[1])
axs[1].set_ylabel(r"$co [m]$", fontsize=22)
axs[1].set_xlabel(r"$s [m]$", fontsize=22)
axs[1].legend(fontsize=20)
axs[1].grid()
plt.show()
# %%
collider.lhcb1.cycle("lhcb1$start", inplace=True)
collider.lhcb2.cycle("lhcb2$start", inplace=True)
collider.lhcb1.twiss_default["only_markers"] = True
collider.lhcb2.twiss_default["only_markers"] = True

collider.lhcb1.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["reverse"] = True


# %%
for irn in ["1", "5"]:
    for hv in ["h", "v"]:
        for bim in ["b1", "b2"]:
            aux_name_l = (
                "acbrdhv4.lirnb1aux".replace("irn", irn)
                .replace("hv", hv)
                .replace("b1", bim)
            )

            aux_name_r = (
                "acbrdhv4.rirnb1aux".replace("irn", irn)
                .replace("hv", hv)
                .replace("b1", bim)
            )
            print(aux_name_r, collider.vars[aux_name_l]._get_value())
            print(aux_name_l, collider.vars[aux_name_r]._get_value())

            connect_target = (
                "acbrdhv4.lirnb1".replace("irn", irn)
                .replace("hv", hv)
                .replace("b1", bim)
            )
            print(connect_target, collider.vars[connect_target]._get_value())
            connect_target = connect_target.replace("l", "r")
            print(connect_target, collider.vars[connect_target]._get_value())

            connect_target = (
                "acbyhvs4.lirnb1".replace("irn", irn)
                .replace("hv", hv)
                .replace("b1", bim)
            )
            print(connect_target, collider.vars[connect_target]._get_value())
            connect_target = connect_target.replace("l", "r")
            print(connect_target, collider.vars[connect_target]._get_value())

            connect_target = (
                "acbyhv4.lirnb1".replace("irn", irn)
                .replace("hv", hv)
                .replace("b1", bim)
            )
            print(connect_target, collider.vars[connect_target]._get_value())
            connect_target = connect_target.replace("l", "r")
            print(connect_target, collider.vars[connect_target]._get_value())

# %%
