# %%
import os
from pathlib import Path
import numpy as np
import pandas as pd
from cpymad import libmadx
from cpymad.madx import Madx
import matplotlib.pyplot as plt


import xtrack as xt
import xmask as xm
import xpart as xp
import xmask.lhc as xlhc
import xobjects as xo
from misc import *
import xtrack._temp.lhc_match as lm

# %%
# try:
#     get_ipython().run_line_magic("matplotlib", "inline")
# except:
#     pass

# %%
prefix = Path("../").absolute()

f1 = (
    prefix
    # + "hl16/strengths/round/levelling/opt_levelling_580_1500_thin.madx"
    # + "hl16/strengths/flat/opt_flathv_500_2000_thin.madx"
    # + "summer_studies/collapse/opt_collapse_700_1500_thin.madx"
    # + "summer_studies/collapse/opt_flathv_450_1800_1500_thin.madx"
    # + "summer_studies/collapse/opt_collapse_flathv_700_2800_thin.madx"
    # + "summer_studies/collapse/opt_collapse_flathv_900_1800_1500.madx"
    # + "summer_studies/SoL/opt_flathv_300_1200_1500_thin.madx"
    # + "summer_studies/SoL/opt_flathv_300_1200_1500.madx"
    # + "summer_studies/opt_round_1100_1500.madx"
    # + "summer_studies/collapse/opt_collapse_1000_1500_triplet_match3.madx"
    # + "test_cycle/opt_1200_1500.madx"
    + "summer_studies/collapse/opt_collapse_1100_1500.madx"
)

simple_name = f1.split("/")[-1].split(".")[0] + ".json"
# simple_name = "collider_from_madx.json"
simple_name = "collider_hl16.json"

collider0 = xt.Multiline.from_json(simple_name)
collider = xt.Multiline.from_json(simple_name)
collider.build_trackers()
collider0.build_trackers()
collider.vars.load_madx_optics_file(f1)
collider0.vars.load_madx_optics_file(f1)

collider.lhcb1.twiss_default["only_markers"] = True
collider.lhcb2.twiss_default["only_markers"] = True

collider.lhcb1.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["method"] = "4d"
collider.lhcb2.twiss_default["reverse"] = True

# collider0.lhcb1.twiss_default["only_markers"] = True
# collider0.lhcb2.twiss_default["only_markers"] = True

collider0.lhcb1.twiss_default["method"] = "4d"
collider0.lhcb2.twiss_default["method"] = "4d"
collider0.lhcb2.twiss_default["reverse"] = True


# %%
tw0 = collider.lhcb1.twiss()
print(tw0[["betx", "bety"], "ip1"])
print(tw0[["betx", "bety"], "ip5"])
# %%

bn = "b1"
line_name = f"lhc{bn}"

betx_ip1 = 0.5
bety_ip1 = 0.5
betx_ip5 = 0.5
bety_ip5 = 0.5

muxip1_l = collider.varval[f"muxip1{bn}_l"]
muyip1_l = collider.varval[f"muyip1{bn}_l"]
muxip1_r = collider.varval[f"muxip1{bn}_r"]
muyip1_r = collider.varval[f"muyip1{bn}_r"]

muxip5_l = collider.varval[f"muxip5{bn}_l"]
muyip5_l = collider.varval[f"muyip5{bn}_l"]
muxip5_r = collider.varval[f"muxip5{bn}_r"]
muyip5_r = collider.varval[f"muyip5{bn}_r"]

muxip2 = collider.varval[f"muxip2{bn}"]
muyip2 = collider.varval[f"muyip2{bn}"]
muxip4 = collider.varval[f"muxip4{bn}"]
muyip4 = collider.varval[f"muyip4{bn}"]
muxip6 = collider.varval[f"muxip6{bn}"]
muyip6 = collider.varval[f"muyip6{bn}"]
muxip8 = collider.varval[f"muxip8{bn}"]
muyip8 = collider.varval[f"muyip8{bn}"]

mux12 = collider.varval[f"mux12{bn}"]
muy12 = collider.varval[f"muy12{bn}"]
mux45 = collider.varval[f"mux45{bn}"]
muy45 = collider.varval[f"muy45{bn}"]
mux56 = collider.varval[f"mux56{bn}"]
muy56 = collider.varval[f"muy56{bn}"]
mux81 = collider.varval[f"mux81{bn}"]
muy81 = collider.varval[f"muy81{bn}"]

muxs = {}
muys = {}

for bim in ["b1", "b2"]:
    for arc_name in lm.ARC_NAMES:
        muxs[f"mux{arc_name}{bim}"] = collider.varval[f"mux{arc_name}{bim}"]
        muys[f"muy{arc_name}{bim}"] = collider.varval[f"muy{arc_name}{bim}"]


# %%
def prop_opt(
    collider,
    ip_name,
    line_name,
    beta_star_x,
    beta_star_y,
    ele_start,
    ele_stop,
    NIR,
    SEC1,
    SEC2,
):
    assert collider.lhcb1.twiss_default.get("reverse", False) is False
    assert collider.lhcb2.twiss_default["reverse"] is True
    assert collider.lhcb1.element_names[1] == "ip1"
    assert collider.lhcb2.element_names[1] == "ip1.l1"
    assert collider.lhcb1.element_names[-2] == "ip1.l1"
    assert collider.lhcb2.element_names[-2] == "ip1"

    if ip_name == "ip1":
        ele_stop_left = "ip1.l1"
        ele_start_right = "ip1"
    else:
        ele_stop_left = ip_name
        ele_start_right = ip_name

    BIM = line_name[3:]
    ele_start1 = f"s.cell.{SEC1}.{BIM}"
    ele_stop1 = f"e.cell.{SEC1}.{BIM}"
    tw1 = collider[line_name].twiss(
        ele_start=ele_start1,
        ele_stop=ele_stop1,
        twiss_init=xt.TwissInit(
            line=collider[line_name],
            element_name=ele_stop1,
        ),
    )

    tw1_saved = tw1[:, ele_start1]

    ele_start2 = f"e.cell.{SEC1}.{BIM}"
    ele_stop2 = f"s.ds.l{NIR}.{BIM}"
    tw2 = collider[line_name].twiss(
        ele_start=ele_start2,
        ele_stop=ele_stop2,
        twiss_init=xt.TwissInit(
            line=collider[line_name],
            element_name=ele_start2,
            betx=tw1_saved.betx[0],
            bety=tw1_saved.bety[0],
            alfx=tw1_saved.alfx[0],
            alfy=tw1_saved.alfy[0],
            dx=tw1_saved.dx[0],
            dpx=tw1_saved.dpx[0],
            mux=tw1_saved.mux[0],
            muy=tw1_saved.muy[0],
        ),
    )

    tw2_saved = tw2[:, ele_stop2]
    tw2_saved.mux = tw2["mux", ele_stop2] - tw2["mux", ele_start2]
    tw2_saved.muy = tw2["muy", ele_stop2] - tw2["muy", ele_start2]

    ele_start3 = f"s.cell.{SEC2}.{BIM}"
    ele_stop3 = f"e.cell.{SEC2}.{BIM}"
    tw3 = collider[line_name].twiss(
        ele_start=ele_start3,
        ele_stop=ele_stop3,
        twiss_init=xt.TwissInit(
            line=collider[line_name],
            element_name=ele_stop3,
        ),
    )

    ele_start4 = f"e.ds.r{NIR}.{BIM}"
    ele_stop4 = f"s.cell.{SEC2}.{BIM}"
    tw4 = collider[line_name].twiss(
        ele_start=ele_start4,
        ele_stop=ele_stop4,
        twiss_init=xt.TwissInit(
            line=collider[line_name],
            element_name=ele_stop4,
        ),
    )

    tw_ip = xt.TwissTable.concatenate([tw1, tw2, tw3, tw4])

    return tw_ip


# %%
tw_sq_a81_ip1_a12_b1 = lm.propagate_optics_from_beta_star(
    collider,
    ip_name="ip1",
    line_name="lhcb1",
    # ele_start="s.ds.r8.b1",
    # ele_stop="e.ds.l2.b1",
    ele_start="s.cell.81.b1",
    ele_stop="e.cell.12.b1",
    beta_star_x=betx_ip1,
    beta_star_y=bety_ip1,
)

# tw_sq_a81_ip1_a12_b2 = lm.propagate_optics_from_beta_star(
#     collider,
#     ip_name="ip1",
#     line_name="lhcb2",
#     ele_start="s.ds.r8.b2",
#     ele_stop="e.ds.l2.b2",
#     # ele_start="s.cell.81.b2",
#     # ele_stop="e.cell.12.b2",
#     beta_star_x=betx_ip1,
#     beta_star_y=bety_ip1,
# )


print(tw_sq_a81_ip1_a12_b1[:, "ip1"])

tw_sq_a45_ip5_a56_b1 = lm.propagate_optics_from_beta_star(
    collider,
    ip_name="ip5",
    line_name="lhcb1",
    # ele_start="s.ds.r4.b1",
    # ele_stop="e.ds.l6.b1",
    ele_start="s.cell.45.b1",
    ele_stop="e.cell.56.b1",
    beta_star_x=betx_ip5,
    beta_star_y=bety_ip5,
)


# tw_sq_a45_ip5_a56_b2 = lm.propagate_optics_from_beta_star(
#     collider,
#     ip_name="ip5",
#     line_name="lhcb2",
#     ele_start="s.ds.r4.b2",
#     ele_stop="e.ds.l6.b2",
#     # ele_start="s.cell.45.b2",
#     # ele_stop="e.cell.56.b2",
#     beta_star_x=betx_ip5,
#     beta_star_y=bety_ip5,
# )

print(tw_sq_a45_ip5_a56_b1[:, "ip5"])

# %%

tw_sq_a81_ip1_a12 = prop_opt(
    collider,
    ip_name="ip1",
    line_name="lhcb1",
    ele_start="s.cell.81.b1",
    ele_stop="e.cell.12.b1",
    beta_star_x=betx_ip1,
    beta_star_y=bety_ip1,
    NIR=1,
    SEC1=81,
    SEC2=12,
)

tw_sq_a45_ip1_a56 = prop_opt(
    collider,
    ip_name="ip5",
    line_name="lhcb1",
    ele_start="s.cell.45.b1",
    ele_stop="e.cell.56.b1",
    beta_star_x=betx_ip1,
    beta_star_y=bety_ip1,
    NIR=5,
    SEC1=45,
    SEC2=56,
)
# %%

print(collider0.lhcb1.twiss()[:, "ip1"])
print(collider.lhcb1.twiss()[:, "ip1"])

# %%
tw45_56_xt = lm.get_arc_periodic_solution(collider, arc_name=["45", "56"])

# %%

match_on_triplet = 1
match_inj_tunes = 0
no_match_beta = 0
ir5q4sym = 0
ir5q5sym = 0
ir5q6sym = 0

staged_match = True

optimizers = {}


# %%
optimizers["ir15"] = rematch_ir15(
    collider,
    betx_ip5,
    bety_ip5,
    tw_sq_a45_ip5_a56=tw45_56_xt,
    restore=False,
    solve=True,
    match_on_triplet=0,
)
# %%
print(optimizers["ir15"].target_status())
print(optimizers["ir15"].log())
# %%
twb1 = collider["lhcb1"].cycle("ip3").twiss(method="4d")
twb2 = collider["lhcb2"].cycle("ip3").twiss(method="4d")

tw0b1 = collider0["lhcb1"].cycle("ip3").twiss(method="4d")
tw0b2 = collider0["lhcb2"].cycle("ip3").twiss(method="4d")

print(twb1[["betx", "bety"], "ip.*"])
print(twb2[["betx", "bety"], "ip.*"])
# %%
old_bets = tw0b1["betx", "ip1"]
new_bets = twb1["betx", "ip1"]
# %%
print(collider.varval["kq4.l5b1"])
print(collider0.varval["kq4.l5b1"])
# %%
rra = "s.ds.l5.b1"
rrb = "e.ds.r5.b1"
rra = None
rrb = None

fig, axs = plt.subplots(figsize=(11, 11))
axs.plot(
    twb1["s", rra:rrb],
    twb1["betx", rra:rrb],
    label=rf"$\beta_x$, $\beta_x^*$ {new_bets:2.2f} m",
)
axs.plot(
    twb1["s", rra:rrb],
    twb1["bety", rra:rrb],
    label=rf"$\beta_y$, $\beta_y^*$ {new_bets:2.2f} m",
)
axs.plot(
    tw0b1["s", rra:rrb],
    tw0b1["betx", rra:rrb],
    label=rf"$\beta_x$, $\beta_x^*$ {old_bets:2.2f} m",
    ls="--",
)
axs.plot(
    tw0b1["s", rra:rrb],
    tw0b1["bety", rra:rrb],
    label=rf"$\beta_y$, $\beta_y^*$ {old_bets:2.2f} m",
    ls="--",
)
axs_t = axs.twiny()
axs_t.set_xticks(
    tw0b1[["s"], "ip.*"],
    tw0b1[["name"], "ip.*"],
)
axs_t.set_xlim(axs.get_xlim())
[axs_t.axvline(xpos, linestyle="--") for xpos in tw0b1[["s"], "ip.*"]]
axs.set_xlabel("s [m]")
axs.set_ylabel(r"$\beta_{x,y}$ [m]")
axs.legend()
plt.show()
# %%
